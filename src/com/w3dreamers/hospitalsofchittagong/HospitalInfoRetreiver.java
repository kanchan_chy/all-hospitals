package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class HospitalInfoRetreiver {
	
	Activity activity;
	DbHelper dbOpenHelper;
	
	public HospitalInfoRetreiver(Activity activity)
	{
		this.activity=activity;
	}
	
	public void DetailActivityCaller(String name)
	{
		try
		{
			dbOpenHelper=new DbHelper(activity, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		try
		{
			String address="",direction="",website="",service24="",lat="",lon="",bloodBank="",ambulance="",email="",emergency="";
			ArrayList<String>columns=new ArrayList<String>();
			columns.add(Constants.INFO_ADDRESS);
			columns.add(Constants.INFO_DIRECTION);
			columns.add(Constants.INFO_WEBSITE);
			columns.add(Constants.INFO_SERVICE_24);
			columns.add(Constants.INFO_LATITUDE);
			columns.add(Constants.INFO_LONGITUDE);
			columns.add(Constants.INFO_BLOOD_BANK);
			columns.add(Constants.INFO_AMBULANCE);
			
			HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_INFO, columns, Constants.INFO_NAME, Constants.STRING_ONLY, name, Constants.INFO_NAME);
			if(data.get(Constants.INFO_ADDRESS).size()>0) address=data.get(Constants.INFO_ADDRESS).get(0);
			if(data.get(Constants.INFO_DIRECTION).size()>0) direction=data.get(Constants.INFO_DIRECTION).get(0);
			if(data.get(Constants.INFO_WEBSITE).size()>0) website=data.get(Constants.INFO_WEBSITE).get(0);
			if(data.get(Constants.INFO_SERVICE_24).size()>0) service24=data.get(Constants.INFO_SERVICE_24).get(0);
			if(data.get(Constants.INFO_LATITUDE).size()>0) lat=data.get(Constants.INFO_LATITUDE).get(0);
			if(data.get(Constants.INFO_LONGITUDE).size()>0) lon=data.get(Constants.INFO_LONGITUDE).get(0);
			if(data.get(Constants.INFO_BLOOD_BANK).size()>0) bloodBank=data.get(Constants.INFO_BLOOD_BANK).get(0);
			if(data.get(Constants.INFO_AMBULANCE).size()>0) ambulance=data.get(Constants.INFO_AMBULANCE).get(0);
			data.clear();
			
			ArrayList<String>columns1=new ArrayList<String>();
			columns1.add(Constants.EMAIL_MAIL);
			HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getSelectedRowString(Constants.TABLE_EMAIL, columns1, Constants.EMAIL_NAME, Constants.STRING_ONLY, name, Constants.EMAIL_NAME);
			if(data1.get(Constants.EMAIL_MAIL).size()>0) email=data1.get(Constants.EMAIL_MAIL).get(0);
			data1.clear();
			
			ArrayList<String>columns2=new ArrayList<String>();
			columns2.add(Constants.EMERGENCY_NUMBER);
			HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowString(Constants.TABLE_EMERGENCY, columns2, Constants.EMERGENCY_NAME, Constants.STRING_ONLY, name, Constants.EMERGENCY_NAME);
			if(data2.get(Constants.EMERGENCY_NUMBER).size()>0) emergency=data2.get(Constants.EMERGENCY_NUMBER).get(0);
			data2.clear();
			
			ArrayList<String>depts=new ArrayList<String>();
			ArrayList<String>columns3=new ArrayList<String>();
			columns3.add(Constants.DEPARTMENT_DEPT);
			HashMap<String,ArrayList<String>> data3	=dbOpenHelper.getSelectedRowString(Constants.TABLE_DEPARTMENT, columns3, Constants.DEPARTMENT_NAME, Constants.STRING_ONLY, name, Constants.DEPARTMENT_NAME);
			depts=data3.get(Constants.DEPARTMENT_DEPT);
			data3.clear();
			
			if(ambulance.equals("Available"))
			{
				ArrayList<String>columns4=new ArrayList<String>();
				columns4.add(Constants.AMBULANCE_NUMBER_NUMBER);
				HashMap<String,ArrayList<String>> data4	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER, columns4, Constants.AMBULANCE_NUMBER_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_NAME);
				if(data4.get(Constants.AMBULANCE_NUMBER_NUMBER).size()>0) ambulance=data4.get(Constants.AMBULANCE_NUMBER_NUMBER).get(0);
				data4.clear();
			}
			
			Intent in=new Intent(activity,DetailsActivity.class);
			//in.putExtra("id", id);
			in.putExtra("name", name);
			in.putExtra("address", address);
			in.putExtra("direction", direction);
			in.putExtra("website", website);
			in.putExtra("service24", service24);
			in.putExtra("lat", lat);
			in.putExtra("lon", lon);
			in.putExtra("ambulance", ambulance);
			in.putExtra("bloodBank", bloodBank);
			in.putExtra("email", email);
			in.putExtra("emergency", emergency);
			in.putExtra("depts", depts);
			activity.startActivity(in);
			activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		catch(Exception e) {}
		
	}
	

}
