package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;



public class OptionActivity extends Activity implements LocationListener,GooglePlayServicesClient.ConnectionCallbacks,GooglePlayServicesClient.OnConnectionFailedListener{
	
	LinearLayout linearEmergency,linearAmbulance,linear24_7,linearAll,linearCategory,linearBlood,linearLanguage,linearPharmecy,linearNearest,linearLocation;
	TextView txtEmergency,txtAmbulance,txt24_7,txtAll,txtCategory,txtBlood,txtLanguage,txtPharmecy,txtNearest,txtLocation;
	ImageView imgLanguage;
	
	LocationClient locationClient;
	int count;
	
	DbHelper dbOpenHelper;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		setContentView(R.layout.option_layout);
		
		linearEmergency=(LinearLayout)findViewById(R.id.linearEmergency);
		linearAmbulance=(LinearLayout)findViewById(R.id.linearAmbulance);
		linear24_7=(LinearLayout)findViewById(R.id.linear24_7);
		linearAll=(LinearLayout)findViewById(R.id.linearAll);
		linearCategory=(LinearLayout)findViewById(R.id.linearCategory);
		linearBlood=(LinearLayout)findViewById(R.id.linearBloodBank);
		linearLanguage=(LinearLayout)findViewById(R.id.linearLanguage);
		linearPharmecy=(LinearLayout)findViewById(R.id.linearPharmecy);
		linearNearest=(LinearLayout)findViewById(R.id.linearNearest);
		linearLocation=(LinearLayout)findViewById(R.id.linearLocation);
		
		txtEmergency=(TextView)findViewById(R.id.txtEmergency);
		txtAmbulance=(TextView)findViewById(R.id.txtAmbulance);
		txt24_7=(TextView)findViewById(R.id.txt24_7);
		txtAll=(TextView)findViewById(R.id.txtAll);
		txtCategory=(TextView)findViewById(R.id.txtCategory);
		txtBlood=(TextView)findViewById(R.id.txtBloodBank);
		txtLanguage=(TextView)findViewById(R.id.txtLanguage);
		txtPharmecy=(TextView)findViewById(R.id.txtPharmecy);
		txtNearest=(TextView)findViewById(R.id.txtNearest);
		txtLocation=(TextView)findViewById(R.id.txtLocation);
		
		imgLanguage=(ImageView)findViewById(R.id.imgLanguage);
		
		imgLanguage.setImageResource(R.drawable.ic_bangla_button);
		
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		linearEmergency.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String> numbers=new ArrayList<String>();
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRow(Constants.TABLE_EMERGENCY, Constants.EMERGENCY_NAME);
					names=data.get(Constants.EMERGENCY_NAME);
					numbers=data.get(Constants.EMERGENCY_NUMBER);
					data.clear();
					
					Intent in=new Intent(OptionActivity.this,NumberListActivity.class);
					in.putExtra("names", names);
					in.putExtra("title", "Emergency Numbers");
					in.putExtra("allContacts",numbers);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearAmbulance.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ArrayList<String> names=new ArrayList<String>();
				ArrayList<String>columns=new ArrayList<String>();
				columns.add(Constants.AMBULANCE_NUMBER_NAME);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_AMBULANCE_NUMBER, columns, Constants.AMBULANCE_NUMBER_NAME);
				names=data.get(Constants.AMBULANCE_NUMBER_NAME);
				data.clear();
				
				Intent in=new Intent(OptionActivity.this,HospitalListActivity.class);
				in.putExtra("title", "Ambulance");
				in.putExtra("names", names);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			}
		});
		
		
		linear24_7.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String>hospitals=new ArrayList<String>();
					ArrayList<String>ambulances=new ArrayList<String>();
					ArrayList<String>banks=new ArrayList<String>();
					ArrayList<String>pharmecies=new ArrayList<String>();
					
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_INFO, columns, Constants.INFO_SERVICE_24, Constants.STRING_ONLY, "Available", Constants.INFO_NAME);
					hospitals=data.get(Constants.INFO_NAME);
					
					ArrayList<String>columns1=new ArrayList<String>();
					columns1.add(Constants.AMBULANCE_NUMBER_NAME);
					HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER, columns1, Constants.AMBULANCE_NUMBER_SERVICE_24, Constants.STRING_ONLY, "Available", Constants.AMBULANCE_NUMBER_NAME);
					ambulances=data1.get(Constants.AMBULANCE_NUMBER_NAME);
					
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.BLOOD_BANK_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK, columns2, Constants.BLOOD_BANK_SERVICE_24, Constants.STRING_ONLY, "Available", Constants.BLOOD_BANK_NAME);
					banks=data2.get(Constants.BLOOD_BANK_NAME);
					
					ArrayList<String>columns3=new ArrayList<String>();
					columns3.add(Constants.PHARMECY_NAME);
					HashMap<String,ArrayList<String>> data3	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY, columns3, Constants.PHARMECY_SERVICE_24, Constants.STRING_ONLY, "Available", Constants.PHARMECY_NAME);
					pharmecies=data3.get(Constants.PHARMECY_NAME);
					
					Intent in=new Intent(OptionActivity.this,Service24Activity.class);
					in.putExtra("hospitals", hospitals);
					in.putExtra("ambulances", ambulances);
					in.putExtra("banks", banks);
					in.putExtra("pharmecies", pharmecies);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					
				}
				catch(Exception e) {}
			}
		});
		
		
		linearAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO, columns, Constants.INFO_NAME);
					names=data.get(Constants.INFO_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivity.this,HospitalListActivity.class);
					in.putExtra("title", "Hospitals of Chittagong");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearCategory.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> categories=new ArrayList<String>();
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRow(Constants.TABLE_ALL_CATEGORY, Constants.ALL_CATEGORY_CATEGORY_NAME);
					categories=data.get(Constants.ALL_CATEGORY_CATEGORY_NAME);	
					data.clear();
					
					Intent in=new Intent(OptionActivity.this,CategoriesActivity.class);
					in.putExtra("categories", categories);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearBlood.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.BLOOD_BANK_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_BLOOD_BANK, columns, Constants.BLOOD_BANK_NAME);
					names=data.get(Constants.BLOOD_BANK_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivity.this,HospitalListActivity.class);
					in.putExtra("title", "Blood Bank");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearLanguage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				LayoutInflater inflater=LayoutInflater.from(OptionActivity.this);
				View view=inflater.inflate(R.layout.warning_dialog, null);
				LinearLayout linearYes,linearNo;
				TextView txtYes,txtNo,txtWarning;
				linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
				linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
				txtYes=(TextView)view.findViewById(R.id.txtYes);
				txtNo=(TextView)view.findViewById(R.id.txtNo);
				txtWarning=(TextView)view.findViewById(R.id.txtWarning);
				Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
				txtWarning.setTypeface(banglaFont);
				
				String warning="আপনার মোবাইল ফোনে কি বাংলা ফন্ট ঠিকমত পড়া যাচ্ছে? যদি পড়া যাই তাহলে ইয়েস বাটনে প্রেস করুন অন্যথায় নো বাটনে প্রেস করুন।";
				SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
				txtWarning.setText(convertedWarning);
				
				AlertDialog.Builder builder=new AlertDialog.Builder(OptionActivity.this);
				builder.setView(view);
				builder.setCancelable(true);
				
				final AlertDialog dialog=builder.create();
				dialog.show();
				
				linearYes.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
						SharedPreferences.Editor editorSupport=prefSupport.edit();
						editorSupport.putBoolean("supported", true);
						editorSupport.commit();
						
						SharedPreferences prefLang=getSharedPreferences("Language", MODE_PRIVATE);
						SharedPreferences.Editor editorLang=prefLang.edit();
						editorLang.putString("language", "Bangla");
						editorLang.commit();
						
						Intent intent=new Intent(OptionActivity.this,OptionActivityBangla.class);
						startActivity(intent);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						finish();
					}
				});
				
				
				linearNo.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
						SharedPreferences.Editor editorSupport=prefSupport.edit();
						editorSupport.putBoolean("supported", false);
						editorSupport.commit();
						
						SharedPreferences prefLang=getSharedPreferences("Language", MODE_PRIVATE);
						SharedPreferences.Editor editorLang=prefLang.edit();
						editorLang.putString("language", "Bangla");
						editorLang.commit();
						
						Intent intent=new Intent(OptionActivity.this,OptionActivityBangla.class);
						startActivity(intent);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						finish();
					}
				});
				
				
				
			}
		});
		
		linearPharmecy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.PHARMECY_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_PHARMECY, columns, Constants.PHARMECY_NAME);
					names=data.get(Constants.PHARMECY_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivity.this,HospitalListActivity.class);
					in.putExtra("title", "Pharmecy");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		
		linearNearest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				callNearest();
			}
		});
		
		
		linearLocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> ids=new ArrayList<String>();
					ArrayList<String> hospitals=new ArrayList<String>();
					ArrayList<String> addresses=new ArrayList<String>();
					ArrayList<String> directions=new ArrayList<String>();
					ArrayList<String> lats=new ArrayList<String>();
					ArrayList<String> lons=new ArrayList<String>();
					
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_ID);
					columns.add(Constants.INFO_NAME);
					columns.add(Constants.INFO_ADDRESS);
					columns.add(Constants.INFO_DIRECTION);
					columns.add(Constants.INFO_LATITUDE);
					columns.add(Constants.INFO_LONGITUDE);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO, columns, Constants.INFO_NAME);
					ids=data.get(Constants.INFO_ID);
					hospitals=data.get(Constants.INFO_NAME);
					addresses=data.get(Constants.INFO_ADDRESS);
					directions=data.get(Constants.INFO_DIRECTION);
					lats=data.get(Constants.INFO_LATITUDE);
					lons=data.get(Constants.INFO_LONGITUDE);
					data.clear();
					
					Intent in=new Intent(OptionActivity.this,AddressActivity.class);
					in.putExtra("ids", ids);
					in.putExtra("hospitals", hospitals);
					in.putExtra("addresses", addresses);
					in.putExtra("directions", directions);
					in.putExtra("lats", lats);
					in.putExtra("lons", lons);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		
		
		
	}
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Google Play Service is not supported in your device", Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(OptionActivity.this,ExitActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		finish();
	}



	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		
		count++;
		try
		{
			Location location=locationClient.getLastLocation();
			double mLat=location.getLatitude();
			double mLon=location.getLongitude();
				
			ArrayList<String>hospitals=new ArrayList<String>();
			ArrayList<String>ambulances=new ArrayList<String>();
			ArrayList<String>bloods=new ArrayList<String>();
			ArrayList<String>pharmecies=new ArrayList<String>();
			ArrayList<String>hos_lats=new ArrayList<String>();
			ArrayList<String>hos_lons=new ArrayList<String>();
			ArrayList<String>amb_lats=new ArrayList<String>();
			ArrayList<String>amb_lons=new ArrayList<String>();
			ArrayList<String>blood_lats=new ArrayList<String>();
			ArrayList<String>blood_lons=new ArrayList<String>();
			ArrayList<String>phar_lats=new ArrayList<String>();
			ArrayList<String>phar_lons=new ArrayList<String>();
			
			ArrayList<String>columns=new ArrayList<String>();
			columns.add(Constants.INFO_NAME);
			columns.add(Constants.INFO_LATITUDE);
			columns.add(Constants.INFO_LONGITUDE);
			HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO, columns, Constants.INFO_NAME);
			hospitals=data.get(Constants.INFO_NAME);
			hos_lats=data.get(Constants.INFO_LATITUDE);
			hos_lons=data.get(Constants.INFO_LONGITUDE);
			
			ArrayList<String>columns1=new ArrayList<String>();
			columns1.add(Constants.AMBULANCE_NUMBER_NAME);
			columns1.add(Constants.AMBULANCE_NUMBER_LAT);
			columns1.add(Constants.AMBULANCE_NUMBER_LON);
			HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_AMBULANCE_NUMBER, columns1, Constants.AMBULANCE_NUMBER_NAME);
			ambulances=data1.get(Constants.AMBULANCE_NUMBER_NAME);
			amb_lats=data1.get(Constants.AMBULANCE_NUMBER_LAT);
			amb_lons=data1.get(Constants.AMBULANCE_NUMBER_LON);
			
			ArrayList<String>columns2=new ArrayList<String>();
			columns2.add(Constants.BLOOD_BANK_NAME);
			columns2.add(Constants.BLOOD_BANK_LAT);
			columns2.add(Constants.BLOOD_BANK_LON);
			HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_BLOOD_BANK, columns2, Constants.BLOOD_BANK_NAME);
			bloods=data2.get(Constants.BLOOD_BANK_NAME);
			blood_lats=data2.get(Constants.BLOOD_BANK_LAT);
			blood_lons=data2.get(Constants.BLOOD_BANK_LON);
			
			ArrayList<String>columns3=new ArrayList<String>();
			columns3.add(Constants.PHARMECY_NAME);
			columns3.add(Constants.PHARMECY_LAT);
			columns3.add(Constants.PHARMECY_LON);
			HashMap<String,ArrayList<String>> data3	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_PHARMECY, columns3, Constants.PHARMECY_NAME);
			pharmecies=data3.get(Constants.PHARMECY_NAME);
			pharmecies=data3.get(Constants.PHARMECY_LAT);
			pharmecies=data3.get(Constants.PHARMECY_LON);
			
			Intent in=new Intent(OptionActivity.this,NearestServiceActivity.class);
			in.putExtra("hospitals", hospitals);
			in.putExtra("ambulances", ambulances);
			in.putExtra("bloods", bloods);
			in.putExtra("pharmecies", pharmecies);
			in.putExtra("hos_lats", hos_lats);
			in.putExtra("hos_lons", hos_lons);
			in.putExtra("amb_lats", amb_lats);
			in.putExtra("amb_lons", amb_lons);
			in.putExtra("blood_lats", blood_lats);
			in.putExtra("blood_lons", blood_lons);
			in.putExtra("phar_lats", phar_lats);
			in.putExtra("phar_lons", phar_lons);
			in.putExtra("lat", mLat);
			in.putExtra("lon", mLon);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				
		}
		catch(Exception e)
		{
			if(count<10)
			{
				locationClient.disconnect();
				locationClient.connect();
			}
			else
			{
				Toast.makeText(getApplicationContext(), "Can't find your location", Toast.LENGTH_LONG).show();
			}
		}
		
	}



	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	public void callNearest()
	{
		WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
		boolean isWifiEnabled=wifi.isWifiEnabled();
		if(isConnectingInternet()||isWifiEnabled)
		{
			if(isMapAvailable())
			{
				LocationManager locManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
				boolean gps=locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				boolean network=locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
				if(gps||network)
				{
					Toast.makeText(getApplicationContext(), "Searching your location", Toast.LENGTH_SHORT).show();
					locationClient=new LocationClient(this, this, this);
					count=0;
					locationClient.connect();
				}
				else
				{
					
					LayoutInflater li = LayoutInflater.from(OptionActivity.this);
					View promptsView = li.inflate(R.layout.internet_dialog, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivity.this);
					builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.show();
					TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
					TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
					TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
					TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
					
					LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
					LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
					
					txtMain.setText("Location Service required");
					txtOptional.setText("Location Services: both GPS and Mobile Network are turned OFF in your mobile phone. Click Ok button to turn them ON.");
					
					linearCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
						}
					});	
					
					linearOk.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
							Intent in=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(in);
						}
					});
					
				}
			}
		}
		else
		{
			
			LayoutInflater li = LayoutInflater.from(OptionActivity.this);
			View promptsView = li.inflate(R.layout.internet_dialog, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivity.this);
			builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
			TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
			TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
			TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
			
			LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
			LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
			
			txtMain.setText("Internet Connection required");
			txtOptional.setText("Click Ok button to turn ON internet connection of your device");
			
			linearCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
				}
			});	
			
			linearOk.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
			
		}
	}
	
	
	
	

}
