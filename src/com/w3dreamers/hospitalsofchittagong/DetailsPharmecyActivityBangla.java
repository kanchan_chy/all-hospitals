package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsPharmecyActivityBangla extends Activity{
	
	TextView txtTitle,txtContactTitle,txtContact,txtAddressTitle,txtAddress,txtClick1,txtClick2;
	LinearLayout linearContact,linearAddress,linearFavourite;
	
	String type,name,number,address,lat,lon;
	boolean isExists;
	
	boolean supported;
	Typeface banglaFont;
	
	DbHelper dbOpenHelper;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.details_pharmecy);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtContactTitle=(TextView)findViewById(R.id.txtContactTitle);
		txtContact=(TextView)findViewById(R.id.txtContact);
		txtAddressTitle=(TextView)findViewById(R.id.txtAddressTitle);
		txtAddress=(TextView)findViewById(R.id.txtAddress);
		txtClick1=(TextView)findViewById(R.id.txtClick1);
		txtClick2=(TextView)findViewById(R.id.txtClick2);
		linearContact=(LinearLayout)findViewById(R.id.linearContact);
		linearAddress=(LinearLayout)findViewById(R.id.linearAddress);
		linearFavourite=(LinearLayout)findViewById(R.id.linearFavourite);
		
		type=getIntent().getExtras().getString("title");
		name=getIntent().getExtras().getString("name");
		number=getIntent().getExtras().getString("number");
		address=getIntent().getExtras().getString("address");
		lat=getIntent().getExtras().getString("lat");
		lon=getIntent().getExtras().getString("lon");
		
		String addressTitle="ঠিকানা";
		String contactTitle="যোগাযোগ";
		String click1="কল করতে ক্লিক করুন";
		String click2="ম্যাপ দেখতে ক্লিক করুন";
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		txtAddressTitle.setTypeface(banglaFont);
		txtAddress.setTypeface(banglaFont);
		txtContactTitle.setTypeface(banglaFont);
		txtContact.setTypeface(banglaFont);
		txtClick1.setTypeface(banglaFont);
		txtClick2.setTypeface(banglaFont);
		
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(name, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedAddressTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(addressTitle, banglaFont, (float)1);
			txtAddressTitle.setText(convertedAddressTitle);
			SpannableString convertedAddress=AndroidCustomFontSupport.getCorrectedBengaliFormat(address, banglaFont, (float)1);
			txtAddress.setText(convertedAddress);
			SpannableString convertedContactTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(contactTitle, banglaFont, (float)1);
			txtContactTitle.setText(convertedContactTitle);
			SpannableString convertedContact=AndroidCustomFontSupport.getCorrectedBengaliFormat(number, banglaFont, (float)1);
			txtContact.setText(convertedContact);
			SpannableString convertedClick1=AndroidCustomFontSupport.getCorrectedBengaliFormat(click1, banglaFont, (float)1);
			txtClick1.setText(convertedClick1);
			SpannableString convertedClick2=AndroidCustomFontSupport.getCorrectedBengaliFormat(click2, banglaFont, (float)1);
			txtClick2.setText(convertedClick2);
		}
		
		else
		{
			txtTitle.setText(name);
			txtAddressTitle.setText(addressTitle);
			txtAddress.setText(address);
			txtContactTitle.setText(contactTitle);
			txtContact.setText(number);
			txtClick1.setText(click1);
			txtClick2.setText(click2);
		}
		
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
        linearContact.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showCallDialog(number);
			}
		});
		
		
		
		linearAddress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
				boolean isWifiEnabled=wifi.isWifiEnabled();
				if(isConnectingInternet()||isWifiEnabled)
				{
					if(isMapAvailable())
					{
						Intent in=new Intent(DetailsPharmecyActivityBangla.this,LocationActivityBangla.class);
						in.putExtra("name", name);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
				}
				else
				{
					LayoutInflater li = LayoutInflater.from(DetailsPharmecyActivityBangla.this);
					View promptsView = li.inflate(R.layout.internet_dialog, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(DetailsPharmecyActivityBangla.this);
					builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.show();
					TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
					TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
					TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
					TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
					
					LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
					LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
					
					dialogTitle.setTypeface(banglaFont);
					txtMain.setTypeface(banglaFont);
					txtOptional.setTypeface(banglaFont);
					txtOk.setTypeface(banglaFont);
					txtCancel.setTypeface(banglaFont);
					
					String header="সতর্কবার্তা";
					String main="ইন্টারনেট কানেকশন দরকার";
					String optional="আপনার মোবাইল ফোনের ইন্টারনেট কানেকশন অন করতে ঠিক আছে বাটনে ক্লিক করুন";
					String ok="ঠিক আছে";
					String cancel="বাতিল";
					if(supported)
					{
						SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
						dialogTitle.setText(convertedHeader);
						SpannableString convertedMain=AndroidCustomFontSupport.getCorrectedBengaliFormat(main, banglaFont, (float)1);
						txtMain.setText(convertedMain);
						SpannableString convertedOptional=AndroidCustomFontSupport.getCorrectedBengaliFormat(optional, banglaFont, (float)1);
						txtOptional.setText(convertedOptional);
						SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
						txtOk.setText(convertedOk);
						SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
						txtCancel.setText(convertedCancel);
					}
					else
					{
						dialogTitle.setText(header);
						txtMain.setText(main);
						txtOptional.setText(optional);
						txtOk.setText(ok);
						txtCancel.setText(cancel);
					}
					
					linearCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
						}
					});	
					
					linearOk.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
							Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
							startActivity(in);
						}
					});
				}
				
			}
		});
		
		
		
		
		linearFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkIfExists();
				
				LayoutInflater inflater=LayoutInflater.from(DetailsPharmecyActivityBangla.this);
				View view=inflater.inflate(R.layout.warning_dialog, null);
				LinearLayout linearYes,linearNo;
				TextView txtYes,txtNo,txtWarning;
				linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
				linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
				txtYes=(TextView)view.findViewById(R.id.txtYes);
				txtNo=(TextView)view.findViewById(R.id.txtNo);
				txtWarning=(TextView)view.findViewById(R.id.txtWarning);
				Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
				txtWarning.setTypeface(banglaFont);
				txtYes.setTypeface(banglaFont);
				txtNo.setTypeface(banglaFont);
				
				String warning;
				if(isExists) warning="এই "+type+" টি আপনার প্রিয় তালিকাতে আছে। আপনি কি ইহাকে আপনার প্রিয় তালিকা থেকে বের করতে চান?";
				else warning="আপনি কি  এই "+type+"টিকে আপনার প্রিয় তালিকাতে যোগ করতে চান?";
				
				String yes="হ্যাঁ";
				String no="না";
				
				if(supported)
				{
					SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
					txtWarning.setText(convertedWarning);
					SpannableString convertedYes=AndroidCustomFontSupport.getCorrectedBengaliFormat(yes, banglaFont, (float)1);
					txtYes.setText(convertedYes);
					SpannableString convertedNo=AndroidCustomFontSupport.getCorrectedBengaliFormat(no, banglaFont, (float)1);
					txtNo.setText(convertedNo);
				}
				else
				{
					txtWarning.setText(warning);
					txtYes.setText(yes);
					txtNo.setText(no);
				}
				
				AlertDialog.Builder builder=new AlertDialog.Builder(DetailsPharmecyActivityBangla.this);
				builder.setView(view);
				builder.setCancelable(true);
				
				final AlertDialog dialog=builder.create();
				dialog.show();
				
		        linearYes.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
						if(isExists) makeUnFavourite();						
						else makeFavourite();
					}
				});
				
				
				linearNo.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				
			}
		});
		
		
		
		
	}
	
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস আপডেট করা নাই");
		}
		else
		{
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস সাপোর্ট করে না");
		}
		return false;
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	
	
	public void showCallDialog(final String number) 
	{
		
		/*	
		try
        {
            ContentResolver cr = this.getContentResolver();
            ContentValues cv = new ContentValues();
           // cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "New Name");
            cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER,number);
            cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);

            Toast.makeText(this, "Contact added", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            TextView tv = new TextView(this);
            tv.setText(e.toString());
            setContentView(tv);
        }   */
		
		LayoutInflater inflater=LayoutInflater.from(DetailsPharmecyActivityBangla.this);
		View view=inflater.inflate(R.layout.option_dialog, null);
		LinearLayout linearSendVia,linearSave,linearCall,linearClose;
		TextView txtDialogTitle,txtSendVia,txtSave,txtCall,txtClose;
		linearSendVia=(LinearLayout)view.findViewById(R.id.linearSendVia);
		linearSave=(LinearLayout)view.findViewById(R.id.linearSave);
		linearCall=(LinearLayout)view.findViewById(R.id.linearCall);
		linearClose=(LinearLayout)view.findViewById(R.id.linearClose);
		txtDialogTitle=(TextView)view.findViewById(R.id.txtTitle);
		txtSendVia=(TextView)view.findViewById(R.id.txtSendVia);
		txtSave=(TextView)view.findViewById(R.id.txtSave);
		txtCall=(TextView)view.findViewById(R.id.txtCall);
		txtClose=(TextView)view.findViewById(R.id.txtClose);
		
		String dialogTitle="বাছাই করুন";
		String send="ক্ষুদেবার্তার মাধ্যমে পাঠান";
		String save="মুঠোফোনে সংরক্ষণ করুন";
		String call="ফোন করুন";
		String close="বন্ধ করুন";
		
		txtDialogTitle.setTypeface(banglaFont);;
		txtSendVia.setTypeface(banglaFont);
		txtSave.setTypeface(banglaFont);
		txtCall.setTypeface(banglaFont);
		txtClose.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedDialogTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(dialogTitle, banglaFont, (float)1);
			SpannableString convertedSend=AndroidCustomFontSupport.getCorrectedBengaliFormat(send, banglaFont, (float)1);
			SpannableString convertedSave=AndroidCustomFontSupport.getCorrectedBengaliFormat(save, banglaFont, (float)1);
			SpannableString convertedCall=AndroidCustomFontSupport.getCorrectedBengaliFormat(call, banglaFont, (float)1);
			SpannableString convertedClose=AndroidCustomFontSupport.getCorrectedBengaliFormat(close, banglaFont, (float)1);
			txtDialogTitle.setText(convertedDialogTitle);
			txtSendVia.setText(convertedSend);
			txtSave.setText(convertedSave);
			txtCall.setText(convertedCall);
			txtClose.setText(convertedClose);
		}
		else
		{
			txtDialogTitle.setText(dialogTitle);
			txtSendVia.setText(send);
			txtSave.setText(save);
			txtCall.setText(call);
			txtClose.setText(close);
		}
		
		AlertDialog.Builder builder=new AlertDialog.Builder(DetailsPharmecyActivityBangla.this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearSendVia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				String uriStr = "sms:";
				Uri smsUri = Uri.parse(uriStr);
				Intent smsIntent = new Intent(Intent.ACTION_VIEW, smsUri);
				smsIntent.putExtra("sms_body", number);
				startActivity(smsIntent);
				
			}
		});
		
		linearSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				  ArrayList<ContentValues> data = new ArrayList<ContentValues>();

				  ContentValues row1 = new ContentValues();
				  row1.put(Phone.NUMBER, number);
				  data.add(row1);  

				  Intent intent = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
				  intent.putExtra(Insert.PHONE,number);
				  startActivity(intent); 
				
			}
		});
		
		linearCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// put the phone number in number variable
		        //String number="01.....";
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phone="tel:"+number;
				try {
		                callIntent.setData(Uri.parse(phone));
		                startActivity(callIntent);
		            }
		        catch (android.content.ActivityNotFoundException ex) 
		        {
		        	//Toast.makeText(getApplicationContext(),"Call faild, please try again later.", Toast.LENGTH_LONG).show();
		        	showToast("কল করা সম্ভব হই নি, দয়া করে আবার চেষ্টা করুন");
		        }   
				
			}
		});
		
		linearClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
		
		
	}
	
	
	
	
	public void checkIfExists()
	{
		try
		{
			String sName="";
			if(type.equals("অ্যাম্বুল্যান্স"))
			{
				ArrayList<String>columns=new ArrayList<String>();
			    columns.add(Constants.FAVOURITE_BANGLA_AMBULANCE);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE_BANGLA, columns, Constants.FAVOURITE_BANGLA_AMBULANCE, Constants.STRING_ONLY, name, Constants.FAVOURITE_BANGLA_AMBULANCE);
				if(data.get(Constants.FAVOURITE_BANGLA_AMBULANCE).size()>0) sName=data.get(Constants.FAVOURITE_BANGLA_AMBULANCE).get(0);
				data.clear();
			}
			else if(type.equals("ব্লাড ব্যাংক"))
			{
				ArrayList<String>columns=new ArrayList<String>();
			    columns.add(Constants.FAVOURITE_BANGLA_BLOOD_BANK);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE_BANGLA, columns, Constants.FAVOURITE_BANGLA_BLOOD_BANK, Constants.STRING_ONLY, name, Constants.FAVOURITE_BANGLA_BLOOD_BANK);
				if(data.get(Constants.FAVOURITE_BANGLA_BLOOD_BANK).size()>0) sName=data.get(Constants.FAVOURITE_BANGLA_BLOOD_BANK).get(0);
				data.clear();
			}
			else
			{
				ArrayList<String>columns=new ArrayList<String>();
			    columns.add(Constants.FAVOURITE_BANGLA_PHARMECY);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE_BANGLA, columns, Constants.FAVOURITE_BANGLA_PHARMECY, Constants.STRING_ONLY, name, Constants.FAVOURITE_BANGLA_PHARMECY);
				if(data.get(Constants.FAVOURITE_BANGLA_PHARMECY).size()>0) sName=data.get(Constants.FAVOURITE_BANGLA_PHARMECY).get(0);
				data.clear();
			}
			if(sName.equals(name)) isExists=true;
			else isExists=false;
		}
		catch(Exception e) {}
	}
	
	
	
	
	public void makeFavourite()
	{
		if(type.equals("অ্যাম্বুল্যান্স"))
		{
			
			try
			{
					boolean success=false;
					LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
					column.put(Constants.FAVOURITE_BANGLA_AMBULANCE, Constants.IS_STRING);
					
					HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
					ArrayList<String>data1= new ArrayList<String>();
					data1.add(name);
					value.put(0,data1);
					boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column, value);
					if(response==true)
					{
						ArrayList<String>wheres=new ArrayList<String>();
						ArrayList<String>values=new ArrayList<String>();
						int[] matchPositions=new int[6];
						int index=0;
						
						wheres.add(Constants.AMBULANCE_NUMBER_LAT);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lat);
						index++;
						
						wheres.add(Constants.AMBULANCE_NUMBER_LON);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lon);
						index++;
						
						String bName="";
						ArrayList<String>columns2=new ArrayList<String>();
						columns2.add(Constants.AMBULANCE_NUMBER_NAME);
						HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_AMBULANCE_NUMBER, columns2, wheres, matchPositions, values, Constants.AMBULANCE_NUMBER_NAME);
						if(data2.get(Constants.AMBULANCE_NUMBER_NAME).size()>0) bName=data2.get(Constants.AMBULANCE_NUMBER_NAME).get(0);	
						data2.clear();
						
						if(!bName.equals(""))
						{
							LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
							column3.put(Constants.FAVOURITE_AMBULANCE, Constants.IS_STRING);
							
							HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
							ArrayList<String>data3= new ArrayList<String>();
							data3.add(bName);
							value3.put(0,data3);
							boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column3, value3);
							if(response3==true) success=true;
						}
					}
					if(success==false)
					{
						showToast("দুঃখিত, এই অ্যাম্বুল্যান্সটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
					}
					else
					{
						showToast("এই অ্যাম্বুল্যান্সটিকে আপনার প্রিয় তালিকাতে যোগ করা হয়েছে");
					}
				
			}
			catch(Exception e) 
			{
				showToast("দুঃখিত, এই অ্যাম্বুল্যান্সটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
			}
			
		}
		else if(type.equals("ব্লাড ব্যাংক"))
		{
			
			try
			{
					boolean success=false;
					LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
					column.put(Constants.FAVOURITE_BANGLA_BLOOD_BANK, Constants.IS_STRING);
					
					HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
					ArrayList<String>data1= new ArrayList<String>();
					data1.add(name);
					value.put(0,data1);
					boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column, value);
					if(response==true)
					{
						ArrayList<String>wheres=new ArrayList<String>();
						ArrayList<String>values=new ArrayList<String>();
						int[] matchPositions=new int[6];
						int index=0;
						
						wheres.add(Constants.BLOOD_BANK_LAT);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lat);
						index++;
						
						wheres.add(Constants.BLOOD_BANK_LON);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lon);
						index++;
						
						String bName="";
						ArrayList<String>columns2=new ArrayList<String>();
						columns2.add(Constants.BLOOD_BANK_NAME);
						HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_BLOOD_BANK, columns2, wheres, matchPositions, values, Constants.BLOOD_BANK_NAME);
						if(data2.get(Constants.BLOOD_BANK_NAME).size()>0) bName=data2.get(Constants.BLOOD_BANK_NAME).get(0);	
						data2.clear();
						
						if(!bName.equals(""))
						{
							LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
							column3.put(Constants.FAVOURITE_BLOOD_BANK, Constants.IS_STRING);
							
							HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
							ArrayList<String>data3= new ArrayList<String>();
							data3.add(bName);
							value3.put(0,data3);
							boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column3, value3);
							if(response3==true) success=true;
						}
					}
					if(success==false)
					{
						showToast("দুঃখিত, এই  ব্লাড ব্যাংকটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
					}
					else
					{
						showToast("এই  ব্লাড ব্যাংকটিকে আপনার প্রিয় তালিকাতে যোগ করা হয়েছে");
					}
				
			}
			catch(Exception e) 
			{
				showToast("দুঃখিত, এই  ব্লাড ব্যাংকটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
			}
			
		}
		else
		{
			
			try
			{
					boolean success=false;
					LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
					column.put(Constants.FAVOURITE_BANGLA_PHARMECY, Constants.IS_STRING);
					
					HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
					ArrayList<String>data1= new ArrayList<String>();
					data1.add(name);
					value.put(0,data1);
					boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column, value);
					if(response==true)
					{
						ArrayList<String>wheres=new ArrayList<String>();
						ArrayList<String>values=new ArrayList<String>();
						int[] matchPositions=new int[6];
						int index=0;
						
						wheres.add(Constants.PHARMECY_LAT);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lat);
						index++;
						
						wheres.add(Constants.PHARMECY_LON);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lon);
						index++;
						
						String bName="";
						ArrayList<String>columns2=new ArrayList<String>();
						columns2.add(Constants.PHARMECY_NAME);
						HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_PHARMECY, columns2, wheres, matchPositions, values, Constants.PHARMECY_NAME);
						if(data2.get(Constants.PHARMECY_NAME).size()>0) bName=data2.get(Constants.PHARMECY_NAME).get(0);	
						data2.clear();
						
						if(!bName.equals(""))
						{
							LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
							column3.put(Constants.FAVOURITE_PHARMECY, Constants.IS_STRING);
							
							HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
							ArrayList<String>data3= new ArrayList<String>();
							data3.add(bName);
							value3.put(0,data3);
							boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column3, value3);
							if(response3==true) success=true;
						}
					}
					if(success==false)
					{
						showToast("দুঃখিত, এই  ফার্মেসিটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
					}
					else
					{
						showToast("এই  ফার্মেসিটিকে আপনার প্রিয় তালিকাতে যোগ করা হয়েছে");
					}
				
			}
			catch(Exception e) 
			{
				showToast("দুঃখিত, এই  ফার্মেসিটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
			}
			
		}
		
	}
	
	
	
	
	
	public void makeUnFavourite()
	{
		
		try
		{
			if(type.equals("অ্যাম্বুল্যান্স"))
			{
				boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_AMBULANCE, Constants.STRING_ONLY, name);
				if(isDeleted)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.AMBULANCE_NUMBER_LAT);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.AMBULANCE_NUMBER_LON);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.AMBULANCE_NUMBER_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_AMBULANCE_NUMBER, columns2, wheres, matchPositions, values, Constants.AMBULANCE_NUMBER_NAME);
					if(data2.get(Constants.AMBULANCE_NUMBER_NAME).size()>0) bName=data2.get(Constants.AMBULANCE_NUMBER_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_AMBULANCE, Constants.STRING_ONLY, bName);
						if(isDeleted) showToast("আপনার সিদ্ধান্তটি ঠিকমত কার্যকর হয়েছে");
						else showToast("দুঃখিত, আপনার সিদ্ধান্তটি ঠিকমত কর্যকর করা যায় নি");
					}
				}
			}
			else if(type.equals("ব্লাড ব্যাংক"))
			{
				boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_BLOOD_BANK, Constants.STRING_ONLY, name);
				if(isDeleted)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.BLOOD_BANK_LAT);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.BLOOD_BANK_LON);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.BLOOD_BANK_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_BLOOD_BANK, columns2, wheres, matchPositions, values, Constants.BLOOD_BANK_NAME);
					if(data2.get(Constants.BLOOD_BANK_NAME).size()>0) bName=data2.get(Constants.BLOOD_BANK_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_BLOOD_BANK, Constants.STRING_ONLY, bName);
						if(isDeleted) showToast("আপনার সিদ্ধান্তটি ঠিকমত কার্যকর হয়েছে");
						else showToast("দুঃখিত, আপনার সিদ্ধান্তটি ঠিকমত কর্যকর করা যায় নি");
					}
				}
			}
			else
			{
				boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_PHARMECY, Constants.STRING_ONLY, name);
				if(isDeleted)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.PHARMECY_LAT);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.PHARMECY_LON);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.PHARMECY_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_PHARMECY, columns2, wheres, matchPositions, values, Constants.PHARMECY_NAME);
					if(data2.get(Constants.PHARMECY_NAME).size()>0) bName=data2.get(Constants.PHARMECY_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_PHARMECY, Constants.STRING_ONLY, bName);
						if(isDeleted) showToast("আপনার সিদ্ধান্তটি ঠিকমত কার্যকর হয়েছে");
						else showToast("দুঃখিত, আপনার সিদ্ধান্তটি ঠিকমত কর্যকর করা যায় নি");
					}
				}
			}
		}
		catch(Exception e)
		{
			showToast("দুঃখিত, আপনার সিদ্ধান্তটি ঠিকমত কর্যকর করা যায় নি");
		}
		
	}
	
	
	
	

}
