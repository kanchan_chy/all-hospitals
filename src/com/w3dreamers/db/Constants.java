package com.w3dreamers.db;

import java.util.ArrayList;

public class Constants {
//-------------------------------------------------------------	
    public static final int DATABASE_VERSION = 1;
    
    public static final int STRING_ONLY=1;
    public static final int STRING_ANY=2;
    public static final int STRING_FIRST=3;
    public static final int STRING_LAST=4;
    
    public static final String IS_EQUAL="=";
    public static final String IS_NOT_EQUAL="!=";
    public static final String IS_GREATER=">";
    public static final String IS_LESS="<";
    public static final String IS_GREATER_EQUAL=">=";
    public static final String IS_LESS_EQUAL="<=";
    
    public static final int IS_NUMBER=1;
    public static final int IS_STRING=2;
    
    //-----------------------------------------------------------
    
    
    // Database Name
    public static final String DATABASE_NAME = "hospital2.db";
 
    // Contacts table name
    public static final String TABLE_INFO = "info"; 
    public static final String INFO_ID="id";
    public static final String INFO_NAME="name";
    public static final String INFO_ADDRESS="address";
    public static final String INFO_DIRECTION="direction";
    public static final String INFO_WEBSITE="website";
    public static final String INFO_SERVICE_24="service_24";
    public static final String INFO_LATITUDE="latitude";
    public static final String INFO_LONGITUDE="longitude";
    public static final String INFO_BLOOD_BANK="blood_bank";
    public static final String INFO_AMBULANCE="ambulance";
    public static final String INFO_CATEGORY="category";
    public static final String INFO_PLACE="place";
    
    
    public static final String TABLE_EMAIL = "email"; 
    public static final String EMAIL_ID="id";
    public static final String EMAIL_NAME="name";
    public static final String EMAIL_MAIL="mail";
    
    
    public static final String TABLE_EMERGENCY = "emergency"; 
    public static final String EMERGENCY_ID="id";
    public static final String EMERGENCY_NAME="name";
    public static final String EMERGENCY_NUMBER="number";
    
    
    public static final String TABLE_DEPARTMENT = "department"; 
    public static final String DEPARTMENT_ID="id";
    public static final String DEPARTMENT_NAME="name";
    public static final String DEPARTMENT_DEPT="dept";
    
    
    public static final String TABLE_AMBULANCE_NUMBER = "ambulance_number"; 
    public static final String AMBULANCE_NUMBER_ID="id";
    public static final String AMBULANCE_NUMBER_NAME="name";
    public static final String AMBULANCE_NUMBER_NUMBER="number";
    public static final String AMBULANCE_NUMBER_ADDRESS="address";
    public static final String AMBULANCE_NUMBER_SERVICE_24="service_24";
    public static final String AMBULANCE_NUMBER_LAT="lat";
    public static final String AMBULANCE_NUMBER_LON="lon";
    
    public static final String TABLE_PHARMECY = "pharmecy"; 
    public static final String PHARMECY_ID="id";
    public static final String PHARMECY_NAME="name";
    public static final String PHARMECY_NUMBER="number";
    public static final String PHARMECY_SERVICE_24="service_24";
    public static final String PHARMECY_ADDRESS="address";
    public static final String PHARMECY_LAT="lat";
    public static final String PHARMECY_LON="lon";
    
    
    public static final String TABLE_BLOOD_BANK = "blood_bank"; 
    public static final String BLOOD_BANK_ID="id";
    public static final String BLOOD_BANK_NAME="name";
    public static final String BLOOD_BANK_NUMBER="number";
    public static final String BLOOD_BANK_SERVICE_24="service_24";
    public static final String BLOOD_BANK_ADDRESS="address";
    public static final String BLOOD_BANK_LAT="lat";
    public static final String BLOOD_BANK_LON="lon";
    
    
    public static final String TABLE_FAVOURITE = "favourite"; 
    public static final String FAVOURITE_ID="id";
    public static final String FAVOURITE_HOSPITAL="hospital";
    public static final String FAVOURITE_AMBULANCE="ambulance";
    public static final String FAVOURITE_BLOOD_BANK="blood_bank";
    public static final String FAVOURITE_PHARMECY="pharmecy";
    
    
    public static final String TABLE_ALL_CATEGORY = "all_category"; 
    public static final String ALL_CATEGORY_ID="id";
    public static final String ALL_CATEGORY_CATEGORY_NAME="category_name";
    
    
    public static final String TABLE_ALL_PLACE = "all_place"; 
    public static final String ALL_PLACE_ID="id";
    public static final String ALL_PLACE_PLACE_NAME="place_name";
    
    
    
    
    
    public static final String TABLE_INFO_BANGLA = "info_bangla"; 
    public static final String INFO_BANGLA_ID="id";
    public static final String INFO_BANGLA_NAME="name";
    public static final String INFO_BANGLA_ADDRESS="address";
    public static final String INFO_BANGLA_DIRECTION="direction";
    public static final String INFO_BANGLA_WEBSITE="website";
    public static final String INFO_BANGLA_SERVICE_24="service_24";
    public static final String INFO_BANGLA_LATITUDE="latitude";
    public static final String INFO_BANGLA_LONGITUDE="longitude";
    public static final String INFO_BANGLA_BLOOD_BANK="blood_bank";
    public static final String INFO_BANGLA_AMBULANCE="ambulance";
    public static final String INFO_BANGLA_CATEGORY="category";
    public static final String INFO_BANGLA_PLACE="place";
    
    
    public static final String TABLE_EMAIL_BANGLA = "email_bangla"; 
    public static final String EMAIL_BANGLA_ID="id";
    public static final String EMAIL_BANGLA_NAME="name";
    public static final String EMAIL_BANGLA_MAIL="mail";
    
    
    public static final String TABLE_EMERGENCY_BANGLA = "emergency_bangla"; 
    public static final String EMERGENCY_BANGLA_ID="id";
    public static final String EMERGENCY_BANGLA_NAME="name";
    public static final String EMERGENCY_BANGLA_NUMBER="number";
    
    
    public static final String TABLE_DEPARTMENT_BANGLA = "department_bangla"; 
    public static final String DEPARTMENT_BANGLA_ID="id";
    public static final String DEPARTMENT_BANGLA_NAME="name";
    public static final String DEPARTMENT_BANGLA_DEPT="dept";
    
    
    public static final String TABLE_AMBULANCE_NUMBER_BANGLA = "ambulance_number_bangla"; 
    public static final String AMBULANCE_NUMBER_BANGLA_ID="id";
    public static final String AMBULANCE_NUMBER_BANGLA_NAME="name";
    public static final String AMBULANCE_NUMBER_BANGLA_NUMBER="number";
    public static final String AMBULANCE_NUMBER_BANGLA_ADDRESS="address";
    public static final String AMBULANCE_NUMBER_BANGLA_SERVICE_24="service_24";
    public static final String AMBULANCE_NUMBER_BANGLA_LAT="lat";
    public static final String AMBULANCE_NUMBER_BANGLA_LON="lon";
    
    
    public static final String TABLE_PHARMECY_BANGLA = "pharmecy_bangla"; 
    public static final String PHARMECY_BANGLA_ID="id";
    public static final String PHARMECY_BANGLA_NAME="name";
    public static final String PHARMECY_BANGLA_NUMBER="number";
    public static final String PHARMECY_BANGLA_SERVICE_24="service_24";
    public static final String PHARMECY_BANGLA_ADDRESS="address";
    public static final String PHARMECY_BANGLA_LAT="lat";
    public static final String PHARMECY_BANGLA_LON="lon";
    
    
    public static final String TABLE_BLOOD_BANK_BANGLA = "blood_bank_bangla"; 
    public static final String BLOOD_BANK_BANGLA_ID="id";
    public static final String BLOOD_BANK_BANGLA_NAME="name";
    public static final String BLOOD_BANK_BANGLA_NUMBER="number";
    public static final String BLOOD_BANK_BANGLA_SERVICE_24="service_24";
    public static final String BLOOD_BANK_BANGLA_ADDRESS="address";
    public static final String BLOOD_BANK_BANGLA_LAT="lat";
    public static final String BLOOD_BANK_BANGLA_LON="lon";
    
    
    public static final String TABLE_FAVOURITE_BANGLA = "favourite_bangla"; 
    public static final String FAVOURITE_BANGLA_ID="id";
    public static final String FAVOURITE_BANGLA_HOSPITAL="hospital";
    public static final String FAVOURITE_BANGLA_AMBULANCE="ambulance";
    public static final String FAVOURITE_BANGLA_BLOOD_BANK="blood_bank";
    public static final String FAVOURITE_BANGLA_PHARMECY="pharmecy";
    
    
    public static final String TABLE_ALL_CATEGORY_BANGLA = "all_category_bangla"; 
    public static final String ALL_CATEGORY_BANGLA_ID="id";
    public static final String ALL_CATEGORY_BANGLA_CATEGORY_NAME="category_name";
    
    
    public static final String TABLE_ALL_PLACE_BANGLA = "all_place_bangla"; 
    public static final String ALL_PLACE_BANGLA_ID="id";
    public static final String ALL_PLACE_BANGLA_PLACE_NAME="place_name";
    
    
    
    public static final String TABLE_VERSION = "version"; 
    public static final String VERSION_ID="id";
    public static final String VERSION_VERSION_CODE="version_code";
    
    
    
    
    public ArrayList<String> getColumnName(String tableName)
    {
    	ArrayList<String> allColumn=new ArrayList<String>();
    	allColumn.clear();
    	if(tableName.equals(this.TABLE_INFO))
    	{
    		allColumn.add(INFO_ID);
    		allColumn.add(INFO_NAME);
    		allColumn.add(INFO_ADDRESS);
    		allColumn.add(INFO_DIRECTION);
    		allColumn.add(INFO_WEBSITE);
    		allColumn.add(INFO_SERVICE_24);
    		allColumn.add(INFO_LATITUDE);
    		allColumn.add(INFO_LONGITUDE);
    		allColumn.add(INFO_BLOOD_BANK);
    		allColumn.add(INFO_AMBULANCE);
    		allColumn.add(INFO_CATEGORY);
    		allColumn.add(INFO_PLACE);
    	}
    	else if(tableName.equals(this.TABLE_EMAIL))
    	{
    		allColumn.add(EMAIL_ID);
    		allColumn.add(EMAIL_NAME);
    		allColumn.add(EMAIL_MAIL);
    	}
    	else if(tableName.equals(this.TABLE_EMERGENCY))
    	{
    		allColumn.add(EMERGENCY_ID);
    		allColumn.add(EMERGENCY_NAME);
    		allColumn.add(EMERGENCY_NUMBER);
    	}
    	else if(tableName.equals(this.TABLE_DEPARTMENT))
    	{
    		allColumn.add(DEPARTMENT_ID);
    		allColumn.add(DEPARTMENT_NAME);
    		allColumn.add(DEPARTMENT_DEPT);
    	}
    	else if(tableName.equals(this.TABLE_AMBULANCE_NUMBER))
    	{
    		allColumn.add(AMBULANCE_NUMBER_ID);
    		allColumn.add(AMBULANCE_NUMBER_NAME);
    		allColumn.add(AMBULANCE_NUMBER_NUMBER);
    		allColumn.add(AMBULANCE_NUMBER_SERVICE_24);
    		allColumn.add(AMBULANCE_NUMBER_LAT);
    		allColumn.add(AMBULANCE_NUMBER_LON);
    	}
    	else if(tableName.equals(this.TABLE_PHARMECY))
    	{
    		allColumn.add(PHARMECY_ID);
    		allColumn.add(PHARMECY_NAME);
    		allColumn.add(PHARMECY_NUMBER);
    		allColumn.add(PHARMECY_SERVICE_24);
    		allColumn.add(PHARMECY_ADDRESS);
    		allColumn.add(PHARMECY_LAT);
    		allColumn.add(PHARMECY_LON);
    	}
    	else if(tableName.equals(this.TABLE_BLOOD_BANK))
    	{
    		allColumn.add(BLOOD_BANK_ID);
    		allColumn.add(BLOOD_BANK_NAME);
    		allColumn.add(BLOOD_BANK_NUMBER);
    		allColumn.add(BLOOD_BANK_SERVICE_24);
    		allColumn.add(BLOOD_BANK_ADDRESS);
    		allColumn.add(BLOOD_BANK_LAT);
    		allColumn.add(BLOOD_BANK_LON);
    	}
    	else if(tableName.equals(this.TABLE_FAVOURITE))
    	{
    		allColumn.add(FAVOURITE_ID);
    		allColumn.add(FAVOURITE_HOSPITAL);
    		allColumn.add(FAVOURITE_AMBULANCE);
    		allColumn.add(FAVOURITE_BLOOD_BANK);
    		allColumn.add(FAVOURITE_PHARMECY);
    	}
    	else if(tableName.equals(this.TABLE_ALL_CATEGORY))
    	{
    		allColumn.add(ALL_CATEGORY_ID);
    		allColumn.add(ALL_CATEGORY_CATEGORY_NAME);
    	}
    	else if(tableName.equals(this.TABLE_ALL_PLACE))
    	{
    		allColumn.add(ALL_PLACE_ID);
    		allColumn.add(ALL_PLACE_PLACE_NAME);
    	}
    	
    	
    	
    	if(tableName.equals(this.TABLE_INFO_BANGLA))
    	{
    		allColumn.add(INFO_BANGLA_ID);
    		allColumn.add(INFO_BANGLA_NAME);
    		allColumn.add(INFO_BANGLA_ADDRESS);
    		allColumn.add(INFO_BANGLA_DIRECTION);
    		allColumn.add(INFO_BANGLA_WEBSITE);
    		allColumn.add(INFO_BANGLA_SERVICE_24);
    		allColumn.add(INFO_BANGLA_LATITUDE);
    		allColumn.add(INFO_BANGLA_LONGITUDE);
    		allColumn.add(INFO_BANGLA_BLOOD_BANK);
    		allColumn.add(INFO_BANGLA_AMBULANCE);
    		allColumn.add(INFO_BANGLA_CATEGORY);
    		allColumn.add(INFO_BANGLA_PLACE);
    	}
    	else if(tableName.equals(this.TABLE_EMAIL_BANGLA))
    	{
    		allColumn.add(EMAIL_BANGLA_ID);
    		allColumn.add(EMAIL_BANGLA_NAME);
    		allColumn.add(EMAIL_BANGLA_MAIL);
    	}
    	else if(tableName.equals(this.TABLE_EMERGENCY_BANGLA))
    	{
    		allColumn.add(EMERGENCY_BANGLA_ID);
    		allColumn.add(EMERGENCY_BANGLA_NAME);
    		allColumn.add(EMERGENCY_BANGLA_NUMBER);
    	}
    	else if(tableName.equals(this.TABLE_DEPARTMENT_BANGLA))
    	{
    		allColumn.add(DEPARTMENT_BANGLA_ID);
    		allColumn.add(DEPARTMENT_BANGLA_NAME);
    		allColumn.add(DEPARTMENT_BANGLA_DEPT);
    	}
    	else if(tableName.equals(this.TABLE_AMBULANCE_NUMBER_BANGLA))
    	{
    		allColumn.add(AMBULANCE_NUMBER_BANGLA_ID);
    		allColumn.add(AMBULANCE_NUMBER_BANGLA_NAME);
    		allColumn.add(AMBULANCE_NUMBER_BANGLA_NUMBER);
    		allColumn.add(AMBULANCE_NUMBER_BANGLA_SERVICE_24);
    		allColumn.add(AMBULANCE_NUMBER_BANGLA_LAT);
    		allColumn.add(AMBULANCE_NUMBER_BANGLA_LON);
    	}
    	else if(tableName.equals(this.TABLE_PHARMECY_BANGLA))
    	{
    		allColumn.add(PHARMECY_BANGLA_ID);
    		allColumn.add(PHARMECY_BANGLA_NAME);
    		allColumn.add(PHARMECY_BANGLA_NUMBER);
    		allColumn.add(PHARMECY_BANGLA_SERVICE_24);
    		allColumn.add(PHARMECY_BANGLA_ADDRESS);
    		allColumn.add(PHARMECY_BANGLA_LAT);
    		allColumn.add(PHARMECY_BANGLA_LON);
    	}
    	else if(tableName.equals(this.TABLE_BLOOD_BANK_BANGLA))
    	{
    		allColumn.add(BLOOD_BANK_BANGLA_ID);
    		allColumn.add(BLOOD_BANK_BANGLA_NAME);
    		allColumn.add(BLOOD_BANK_BANGLA_NUMBER);
    		allColumn.add(BLOOD_BANK_BANGLA_SERVICE_24);
    		allColumn.add(BLOOD_BANK_BANGLA_ADDRESS);
    		allColumn.add(BLOOD_BANK_BANGLA_LAT);
    		allColumn.add(BLOOD_BANK_BANGLA_LON);
    	}
    	else if(tableName.equals(this.TABLE_FAVOURITE_BANGLA))
    	{
    		allColumn.add(FAVOURITE_BANGLA_ID);
    		allColumn.add(FAVOURITE_BANGLA_HOSPITAL);
    		allColumn.add(FAVOURITE_BANGLA_AMBULANCE);
    		allColumn.add(FAVOURITE_BANGLA_BLOOD_BANK);
    		allColumn.add(FAVOURITE_BANGLA_PHARMECY);
    	}
    	else if(tableName.equals(this.TABLE_ALL_CATEGORY_BANGLA))
    	{
    		allColumn.add(ALL_CATEGORY_BANGLA_ID);
    		allColumn.add(ALL_CATEGORY_BANGLA_CATEGORY_NAME);
    	}
    	else if(tableName.equals(this.TABLE_ALL_PLACE_BANGLA))
    	{
    		allColumn.add(ALL_PLACE_BANGLA_ID);
    		allColumn.add(ALL_PLACE_BANGLA_PLACE_NAME);
    	}
    	else if(tableName.equals(this.TABLE_VERSION))
    	{
    		allColumn.add(VERSION_ID);
    		allColumn.add(VERSION_VERSION_CODE);
    	}
   	
    	return allColumn;
    }

}
