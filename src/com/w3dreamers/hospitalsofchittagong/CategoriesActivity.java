package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CategoriesActivity extends Activity{
	
	ListView list;
	TextView txtAll;
	LinearLayout linearAll,linearSearchOption,linearTitle,linearSearch;
	ImageView imgSearchOption,imgIcon2;
	EditText edtSearch;
	
	CustomAdapter adapter;
	
	ArrayList<String>categories=new ArrayList<String>();
	
	DbHelper dbOpenHelper;
	
	boolean titleVisibility;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.category_layout);
		list=(ListView)findViewById(R.id.listView1);
		txtAll=(TextView)findViewById(R.id.txtAll);
		imgSearchOption=(ImageView)findViewById(R.id.imgSearchOption);
		imgIcon2=(ImageView)findViewById(R.id.imgIcon2);
		linearAll=(LinearLayout)findViewById(R.id.linearAll);
		linearSearchOption=(LinearLayout)findViewById(R.id.linearSearchOption);
		linearTitle=(LinearLayout)findViewById(R.id.linearTitle);
		linearSearch=(LinearLayout)findViewById(R.id.linearSearch);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		
		linearSearch.setVisibility(View.INVISIBLE);
		titleVisibility=true;
		
		categories=(ArrayList<String>)getIntent().getSerializableExtra("categories");
	    
	    adapter=new CustomAdapter(this,categories,"Category");
	    list.setAdapter(adapter);
	    
	    list.setTextFilterEnabled(true);
	    
	    try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
	    
	    
	    list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				//String category=categories.get(pos);
				
				String category=(String)parent.getItemAtPosition(pos);
				
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_INFO, columns, Constants.INFO_CATEGORY, Constants.STRING_ONLY, category, Constants.INFO_NAME);
					names=data.get(Constants.INFO_NAME);
					data.clear();
					
					Intent in=new Intent(CategoriesActivity.this,HospitalListActivity.class);
					in.putExtra("title", category);
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
				
			}
		});
	    
	    linearAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO, columns, Constants.INFO_NAME);
					names=data.get(Constants.INFO_NAME);
					data.clear();
					
					Intent in=new Intent(CategoriesActivity.this,HospitalListActivity.class);
					in.putExtra("title", "Hospitals of Chittagong");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
	    
	    
       linearSearchOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				linearTitle.setVisibility(View.INVISIBLE);
				linearSearch.setVisibility(View.VISIBLE);
				titleVisibility=false;
			}
		});
		
		
         imgIcon2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearSearch.setVisibility(View.INVISIBLE);
				linearTitle.setVisibility(View.VISIBLE);
				titleVisibility=true;
			}
		});
         
         
         
         
         edtSearch.addTextChangedListener(new TextWatcher() {
 			
 			@Override
 			public void onTextChanged(CharSequence cs, int start, int before, int count) {
 				// TODO Auto-generated method stub
 				adapter.getFilter().filter(cs);
 			}
 			
 			@Override
 			public void beforeTextChanged(CharSequence s, int start, int count,
 					int after) {
 				// TODO Auto-generated method stub
 				
 			}
 			
 			@Override
 			public void afterTextChanged(Editable s) {
 				// TODO Auto-generated method stub
 				
 			}
 		});
	    
		
	}
	
	
	

}
