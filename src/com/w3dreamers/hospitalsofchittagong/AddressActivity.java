package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class AddressActivity extends Activity{
	
	ImageView imgSearchOption,imgIcon2;
	LinearLayout linearSearchOption,linearTitle,linearSearch;
	ListView list;
	EditText edtSearch;
	
	ArrayList<String> hospitals=new ArrayList<String>();
	ArrayList<String> ids=new ArrayList<String>();
	ArrayList<String> addresses=new ArrayList<String>();
	ArrayList<String> directions=new ArrayList<String>();
	ArrayList<String> lats=new ArrayList<String>();
	ArrayList<String> lons=new ArrayList<String>();
	String title;
	
	CustomAdapterAddress adapter;
	
	boolean titleVisibility;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.address_layout);
		imgSearchOption=(ImageView)findViewById(R.id.imgSearchOption);
		linearSearchOption=(LinearLayout)findViewById(R.id.linearSearchOption);
		list=(ListView)findViewById(R.id.listView1);
		imgIcon2=(ImageView)findViewById(R.id.imgIcon2);
		linearTitle=(LinearLayout)findViewById(R.id.linearTitle);
		linearSearch=(LinearLayout)findViewById(R.id.linearSearch);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		
		linearSearch.setVisibility(View.INVISIBLE);
		titleVisibility=true;
		
		hospitals=(ArrayList<String>)getIntent().getSerializableExtra("hospitals");
		ids=(ArrayList<String>)getIntent().getSerializableExtra("ids");
		addresses=(ArrayList<String>)getIntent().getSerializableExtra("addresses");
		directions=(ArrayList<String>)getIntent().getSerializableExtra("directions");
		lats=(ArrayList<String>)getIntent().getSerializableExtra("lats");
		lons=(ArrayList<String>)getIntent().getSerializableExtra("lons");
		
		
		adapter=new CustomAdapterAddress(this, hospitals, addresses, directions);
		
		list.setAdapter(adapter);
		
		list.setTextFilterEnabled(true);
		
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String name=(String)parent.getItemAtPosition(pos);
				
				//String name=hospitals.get(pos);
				//String id=ids.get(pos);
				String lat=lats.get(pos);
				String lon=lons.get(pos);
				if(isConnectingInternet())
				{
					if(isMapAvailable())
					{
						Intent in=new Intent(AddressActivity.this,LocationActivity.class);
						in.putExtra("name", name);
						//in.putExtra("id", id);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
				}
				else
				{
					
					LayoutInflater li = LayoutInflater.from(AddressActivity.this);
					View promptsView = li.inflate(R.layout.internet_dialog, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(AddressActivity.this);
					builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.show();
					TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
					TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
					TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
					TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
					
					LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
					LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
					
					txtMain.setText("Internet Connection required");
					txtOptional.setText("Click Ok button to turn ON internet connection of your device");
					
					linearCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
						}
					});	
					
					linearOk.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
							Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
							startActivity(in);
						}
					});
					
				}
				
			}
		});
		
		
		
		
		
		
        linearSearchOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				linearTitle.setVisibility(View.INVISIBLE);
				linearSearch.setVisibility(View.VISIBLE);
				titleVisibility=false;
			}
		});
		
		
         imgIcon2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearSearch.setVisibility(View.INVISIBLE);
				linearTitle.setVisibility(View.VISIBLE);
				titleVisibility=true;
			}
		});
         
         
         edtSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int start, int before, int count) {
				// TODO Auto-generated method stub
				adapter.getFilter().filter(cs);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
		
	}
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Google Play Service is not supported in your device", Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	

}
