package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DetailsActivity extends FragmentActivity{
	
	TextView txtTitle,txtAddr,txtDir,txtDept,txtEmerg,txtWeb,txtMail,txtClickWeb,txtClickMail,txtClickEmerg;
	ImageView img24_7,imgAmbulance,imgBlood;
	LinearLayout linearFavourite;
	
	//String id;
	String name,address,direction,website,service24,lat,lon,bloodBank,ambulance,email,emergency;
	
	ArrayList<String>depts=new ArrayList<String>();
	
	String title;
	boolean isExists;
	
	DbHelper dbOpenHelper;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.details_layout);
		linearFavourite=(LinearLayout)findViewById(R.id.linearFavourite);
		img24_7=(ImageView)findViewById(R.id.img24_7);
		imgAmbulance=(ImageView)findViewById(R.id.imgAmbulance);
		imgBlood=(ImageView)findViewById(R.id.imgBlood);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtAddr=(TextView)findViewById(R.id.txtAddress);
		txtDir=(TextView)findViewById(R.id.txtDirection);
		txtDept=(TextView)findViewById(R.id.txtDept);
		txtEmerg=(TextView)findViewById(R.id.txtEmergency);
		txtWeb=(TextView)findViewById(R.id.txtWebsite);
		txtMail=(TextView)findViewById(R.id.txtEmail);
		txtClickWeb=(TextView)findViewById(R.id.txtClickWebsite);
		txtClickMail=(TextView)findViewById(R.id.txtClickEmail);
		txtClickEmerg=(TextView)findViewById(R.id.txtClickEmergency);
		
		//id=getIntent().getExtras().getString("id");
		name=getIntent().getExtras().getString("name");
		address=getIntent().getExtras().getString("address");
		direction=getIntent().getExtras().getString("direction");
		website=getIntent().getExtras().getString("website");
		service24=getIntent().getExtras().getString("service24");
		lat=getIntent().getExtras().getString("lat");
		lon=getIntent().getExtras().getString("lon");
		bloodBank=getIntent().getExtras().getString("bloodBank");
		ambulance=getIntent().getExtras().getString("ambulance");
		email=getIntent().getExtras().getString("email");
		emergency=getIntent().getExtras().getString("emergency");
		depts=(ArrayList<String>)getIntent().getSerializableExtra("depts");;
		
		
		title=name;
		txtTitle.setText(title);
		
		
        setTextData();
        
        try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
        
        
        linearFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					String sName="";
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.FAVOURITE_HOSPITAL);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE, columns, Constants.FAVOURITE_HOSPITAL, Constants.STRING_ONLY, name, Constants.FAVOURITE_HOSPITAL);
					if(data.get(Constants.FAVOURITE_HOSPITAL).size()>0) sName=data.get(Constants.FAVOURITE_HOSPITAL).get(0);
					data.clear();
					
					if(sName.equals(name)) isExists=true;
					else isExists=false;
				}
				catch(Exception e) {}
				
				
				LayoutInflater inflater=LayoutInflater.from(DetailsActivity.this);
				View view=inflater.inflate(R.layout.warning_dialog, null);
				LinearLayout linearYes,linearNo;
				TextView txtWarning;
				linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
				linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
				txtWarning=(TextView)view.findViewById(R.id.txtWarning);
				
				String text;
				if(isExists) text="This Hospital exists in your favourite list. Do you want to remove it?";
				else text="Do you want to add this Hospital in your favourite list?";
				
				txtWarning.setText(text);
				
				AlertDialog.Builder builder=new AlertDialog.Builder(DetailsActivity.this);
				builder.setView(view);
				builder.setCancelable(true);
				
				final AlertDialog dialog=builder.create();
				dialog.show();
				
				linearYes.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
						if(isExists) makeUnFavourite();
						else makeFavourite();
					}
				});
				
				
				linearNo.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				
			}
		});
        
			
		
	}
	
	
	
	
	public void makeFavourite()
	{
		try
		{
				boolean success=false;
				LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
				column.put(Constants.FAVOURITE_HOSPITAL, Constants.IS_STRING);
				
				HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
				ArrayList<String>data1= new ArrayList<String>();
				data1.add(name);
				value.put(0,data1);
				boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column, value);
				if(response==true)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.INFO_BANGLA_LATITUDE);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.INFO_BANGLA_LONGITUDE);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.INFO_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_INFO_BANGLA, columns2, wheres, matchPositions, values, Constants.INFO_BANGLA_NAME);
					if(data2.get(Constants.INFO_BANGLA_NAME).size()>0) bName=data2.get(Constants.INFO_BANGLA_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
						column3.put(Constants.FAVOURITE_BANGLA_HOSPITAL, Constants.IS_STRING);
						
						HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
						ArrayList<String>data3= new ArrayList<String>();
						data3.add(bName);
						value3.put(0,data3);
						boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column3, value3);
						if(response3==true) success=true;
					}
				}
				if(success==false)
				{
					Toast.makeText(getApplicationContext(), "Sorry, failed to make this hospital favourite", Toast.LENGTH_LONG).show();
				}
				else
				{
					Toast.makeText(getApplicationContext(), "This hospital is added in your favourite list", Toast.LENGTH_LONG).show();
				}
			
		}
		catch(Exception e) 
		{
			Toast.makeText(getApplicationContext(), "Sorry, failed to make this hospital favourite", Toast.LENGTH_LONG).show();
		}
		
	}
	
	
	
	public void makeUnFavourite()
	{
		try
		{
			boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_HOSPITAL,Constants.STRING_ONLY, name);
			if(isDeleted)
			{
				ArrayList<String>wheres=new ArrayList<String>();
				ArrayList<String>values=new ArrayList<String>();
				int[] matchPositions=new int[6];
				int index=0;
				
				wheres.add(Constants.INFO_BANGLA_LATITUDE);
				matchPositions[index]=Constants.STRING_ONLY;
				values.add(lat);
				index++;
				
				wheres.add(Constants.INFO_BANGLA_LONGITUDE);
				matchPositions[index]=Constants.STRING_ONLY;
				values.add(lon);
				index++;
				
				String bName="";
				ArrayList<String>columns2=new ArrayList<String>();
				columns2.add(Constants.INFO_BANGLA_NAME);
				HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_INFO_BANGLA, columns2, wheres, matchPositions, values, Constants.INFO_BANGLA_NAME);
				if(data2.get(Constants.INFO_BANGLA_NAME).size()>0) bName=data2.get(Constants.INFO_BANGLA_NAME).get(0);	
				data2.clear();
				
				if(!bName.equals(""))
				{
					isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_HOSPITAL,Constants.STRING_ONLY, bName);
					if(isDeleted) Toast.makeText(getApplicationContext(), "Your action was performed successfully", Toast.LENGTH_LONG).show();
					else Toast.makeText(getApplicationContext(), "Sorry, failed to perform your action", Toast.LENGTH_LONG).show();
				}
			}
		}
		catch(Exception e) 
		{
			Toast.makeText(getApplicationContext(), "Sorry, failed to perform your action", Toast.LENGTH_LONG).show();
		}
	}
	
	
	
	
	public void optionClicked(View view)
	{
		if(view.getId()==R.id.linearAddress)
		{
			showMap();
		}
		else if(view.getId()==R.id.linearDirection)
		{
			showMap();
		}
		else if(view.getId()==R.id.linearDept)
		{
			if(depts.size()>1)
			{
				Intent intent=new Intent(DetailsActivity.this,EachOptionActivity.class);
				intent.putExtra("option", "Departments");
				intent.putExtra("title", name);
				intent.putExtra("items",depts);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			}
		}
		else if(view.getId()==R.id.linearEmergency)
		{
			if(!emergency.equals(""))
			{
				showCallDialog(emergency);
			}
		}
		else if(view.getId()==R.id.linearWebsite)
		{
			if(!website.equals("Not available"))
			{
				Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(website));
				startActivity(intent);
			}
		}
		else if(view.getId()==R.id.linearEmail)
		{
			if(!email.equals(""))
			{
				String[] sendTo={email};
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.setData(Uri.parse("mailto:"));
				emailIntent.setType("text/html");
				emailIntent.putExtra(Intent.EXTRA_EMAIL,sendTo);
				//emailIntent.putExtra(Intent.EXTRA_CC, CC);
			    //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
			    //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
				try {
			         startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			      } catch (android.content.ActivityNotFoundException ex) {
			         Toast.makeText(getApplicationContext(),"There is no email client installed.", Toast.LENGTH_SHORT).show();
			      }
			}
		}
	}
	
	
	
	
	public void setTextData()
	{
		txtAddr.setText(address);
		txtDir.setText(direction);
		
		if(website.equals("Not available"))
		{
			txtWeb.setText(website);
			txtClickWeb.setVisibility(View.GONE);
		}
		else
		{
			String url="";
			boolean flag=false;
			for(int i=0;i<website.length();i++)
			{
				if(website.charAt(i)=='w') flag=true;
				if(flag==true&&website.charAt(i)!='/') url=url+website.charAt(i);
			}
			txtWeb.setText(url);
		}
		
		if(!email.equals(""))
		{
			txtMail.setText(email);
		}
		else
		{
			txtMail.setText("Not Available");
			txtClickMail.setVisibility(View.GONE);
		}
		
		if(!emergency.equals(""))
		{
			txtEmerg.setText(emergency);
		}
		else
		{
			txtEmerg.setText("Not Available");
			txtClickEmerg.setVisibility(View.GONE);
		}
		
		if(service24.equals("Available"))
		{
			img24_7.setImageResource(R.drawable.checkbox_checked);
		}
		else
		{
			img24_7.setImageResource(R.drawable.checkbox_unchecked);
		}
		
		if(bloodBank.equals("Available"))
		{
			imgBlood.setImageResource(R.drawable.checkbox_checked);
		}
		else
		{
			imgBlood.setImageResource(R.drawable.checkbox_unchecked);
		}
		
		if(!ambulance.equals("Not Available")) 
		{
			imgAmbulance.setImageResource(R.drawable.checkbox_checked);
		}
		else
		{
			imgAmbulance.setImageResource(R.drawable.checkbox_unchecked);
		}
		
		if(depts.size()<=1)
		{
			txtDept.setText(depts.get(0));
		}
		else
		{
			txtDept.setText("Click to see the departments");
		}
		
	}
	
	
	
	
	
	public void showCallDialog(final String number) 
	{	
		/*	
		try
        {
            ContentResolver cr = this.getContentResolver();
            ContentValues cv = new ContentValues();
           // cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "New Name");
            cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER,number);
            cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);

            Toast.makeText(this, "Contact added", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            TextView tv = new TextView(this);
            tv.setText(e.toString());
            setContentView(tv);
        }   */
		
		LayoutInflater inflater=LayoutInflater.from(this);
		View view=inflater.inflate(R.layout.option_dialog, null);
		LinearLayout linearSendVia,linearSave,linearCall,linearClose;
		TextView txtSendVia,txtSave,txtCall,txtClose;
		linearSendVia=(LinearLayout)view.findViewById(R.id.linearSendVia);
		linearSave=(LinearLayout)view.findViewById(R.id.linearSave);
		linearCall=(LinearLayout)view.findViewById(R.id.linearCall);
		linearClose=(LinearLayout)view.findViewById(R.id.linearClose);
		txtSendVia=(TextView)view.findViewById(R.id.txtSendVia);
		txtSave=(TextView)view.findViewById(R.id.txtSave);
		txtCall=(TextView)view.findViewById(R.id.txtCall);
		txtClose=(TextView)view.findViewById(R.id.txtClose);
		
		AlertDialog.Builder builder=new AlertDialog.Builder(this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearSendVia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				String uriStr = "sms:";
				Uri smsUri = Uri.parse(uriStr);
				Intent smsIntent = new Intent(Intent.ACTION_VIEW, smsUri);
				smsIntent.putExtra("sms_body", number);
				startActivity(smsIntent);
				
			}
		});
		
		linearSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				  ArrayList<ContentValues> data = new ArrayList<ContentValues>();

				  ContentValues row1 = new ContentValues();
				  row1.put(Phone.NUMBER, number);
				  data.add(row1);  

				  Intent intent = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
				  intent.putExtra(Insert.PHONE,number);
				  startActivity(intent); 
				
			}
		});
		
		linearCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// put the phone number in number variable
		        //String number="01.....";
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phone="tel:"+number;
				try {
		                callIntent.setData(Uri.parse(phone));
		                startActivity(callIntent);
		            }
		        catch (android.content.ActivityNotFoundException ex) 
		        {
		        	Toast.makeText(getApplicationContext(),"Call faild, please try again later.", Toast.LENGTH_LONG).show();
		        }   
				
			}
		});
		
		linearClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
	}
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Google Play Service is not supported in your device", Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	
	
	public void showMap()
	{
		if(isConnectingInternet())
		{
			if(isMapAvailable())
			{
				Intent in=new Intent(DetailsActivity.this,LocationActivity.class);
				in.putExtra("name", name);
				//in.putExtra("id", id);
				in.putExtra("lat", lat);
				in.putExtra("lon", lon);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			}
		}
		else
		{
			
			LayoutInflater li = LayoutInflater.from(DetailsActivity.this);
			View promptsView = li.inflate(R.layout.internet_dialog, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(DetailsActivity.this);
			builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
			TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
			TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
			TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
			
			LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
			LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
			
			txtMain.setText("Internet Connection required");
			txtOptional.setText("Click Ok button to turn ON internet connection of your device");
			
			linearCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
				}
			});	
			
			linearOk.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
			
		}
	}
	
	
	
	

}
