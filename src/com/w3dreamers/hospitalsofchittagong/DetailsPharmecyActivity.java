package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsPharmecyActivity extends Activity{
	
	TextView txtTitle,txtContact,txtAddress;
	LinearLayout linearContact,linearAddress,linearFavourite;
	String type,name,number,address,lat,lon;
	boolean isExists;
	
	DbHelper dbOpenHelper;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.details_pharmecy);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtContact=(TextView)findViewById(R.id.txtContact);
		txtAddress=(TextView)findViewById(R.id.txtAddress);
		linearContact=(LinearLayout)findViewById(R.id.linearContact);
		linearAddress=(LinearLayout)findViewById(R.id.linearAddress);
		linearFavourite=(LinearLayout)findViewById(R.id.linearFavourite);
		
		type=getIntent().getExtras().getString("title");
		name=getIntent().getExtras().getString("name");
		number=getIntent().getExtras().getString("number");
		address=getIntent().getExtras().getString("address");
		lat=getIntent().getExtras().getString("lat");
		lon=getIntent().getExtras().getString("lon");
		
		txtTitle.setText(name);
		txtContact.setText(number);
		txtAddress.setText(address);
		
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		linearContact.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showCallDialog(number);
			}
		});
		
		
		
		linearAddress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
				boolean isWifiEnabled=wifi.isWifiEnabled();
				if(isConnectingInternet()||isWifiEnabled)
				{
					if(isMapAvailable())
					{
						Intent in=new Intent(DetailsPharmecyActivity.this,LocationActivity.class);
						in.putExtra("name", name);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
				}
				else
				{
					
					LayoutInflater li = LayoutInflater.from(DetailsPharmecyActivity.this);
					View promptsView = li.inflate(R.layout.internet_dialog, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(DetailsPharmecyActivity.this);
					builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.show();
					TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
					TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
					TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
					TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
					
					LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
					LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
					
					txtMain.setText("Internet Connection required");
					txtOptional.setText("Click Ok button to turn ON internet connection of your device");
					
					linearCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
						}
					});	
					
					linearOk.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
							Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
							startActivity(in);
						}
					});
					
				}
				
			}
		});
		
		
		
		linearFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkIfExistst();
				
				LayoutInflater inflater=LayoutInflater.from(DetailsPharmecyActivity.this);
				View view=inflater.inflate(R.layout.warning_dialog, null);
				LinearLayout linearYes,linearNo;
				TextView txtWarning;
				linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
				linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
				txtWarning=(TextView)view.findViewById(R.id.txtWarning);
				
				String text;
				if(isExists) text="This "+type+" exists in your favourite list. Do you want to remove it?";
				else text="Do you want to add this "+type+" in your favourite list?";
				txtWarning.setText(text);
				
				AlertDialog.Builder builder=new AlertDialog.Builder(DetailsPharmecyActivity.this);
				builder.setView(view);
				builder.setCancelable(true);
				
				final AlertDialog dialog=builder.create();
				dialog.show();
				
				linearYes.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
						if(isExists) makeUnFavourite();
						else makeFavourite();
					}
				});
				
				
				linearNo.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				
				
			}
				
		});
		
		
		
	}
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			Toast.makeText(getApplicationContext(), "Google Play Service is not updated in your device", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Google Play Service is not supported in your device", Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	
	
	public void showCallDialog(final String number) 
	{
		
		/*	
		try
        {
            ContentResolver cr = this.getContentResolver();
            ContentValues cv = new ContentValues();
           // cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "New Name");
            cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER,number);
            cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);

            Toast.makeText(this, "Contact added", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            TextView tv = new TextView(this);
            tv.setText(e.toString());
            setContentView(tv);
        }   */
		
		LayoutInflater inflater=LayoutInflater.from(DetailsPharmecyActivity.this);
		View view=inflater.inflate(R.layout.option_dialog, null);
		LinearLayout linearSendVia,linearSave,linearCall,linearClose;
		TextView txtSendVia,txtSave,txtCall,txtClose;
		linearSendVia=(LinearLayout)view.findViewById(R.id.linearSendVia);
		linearSave=(LinearLayout)view.findViewById(R.id.linearSave);
		linearCall=(LinearLayout)view.findViewById(R.id.linearCall);
		linearClose=(LinearLayout)view.findViewById(R.id.linearClose);
		txtSendVia=(TextView)view.findViewById(R.id.txtSendVia);
		txtSave=(TextView)view.findViewById(R.id.txtSave);
		txtCall=(TextView)view.findViewById(R.id.txtCall);
		txtClose=(TextView)view.findViewById(R.id.txtClose);
		
		AlertDialog.Builder builder=new AlertDialog.Builder(DetailsPharmecyActivity.this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearSendVia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				String uriStr = "sms:";
				Uri smsUri = Uri.parse(uriStr);
				Intent smsIntent = new Intent(Intent.ACTION_VIEW, smsUri);
				smsIntent.putExtra("sms_body", number);
				startActivity(smsIntent);
				
			}
		});
		
		linearSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				  ArrayList<ContentValues> data = new ArrayList<ContentValues>();

				  ContentValues row1 = new ContentValues();
				  row1.put(Phone.NUMBER, number);
				  data.add(row1);  

				  Intent intent = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
				  intent.putExtra(Insert.PHONE,number);
				  startActivity(intent); 
				
			}
		});
		
		linearCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// put the phone number in number variable
		        //String number="01.....";
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phone="tel:"+number;
				try {
		                callIntent.setData(Uri.parse(phone));
		                startActivity(callIntent);
		            }
		        catch (android.content.ActivityNotFoundException ex) 
		        {
		        	Toast.makeText(getApplicationContext(),"Call faild, please try again later.", Toast.LENGTH_LONG).show();
		        }   
				
			}
		});
		
		linearClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
		
		
	}
	
	
	
	public void checkIfExistst()
	{
		try
		{
			String sName="";
			if(type.equals("Ambulance"))
			{
				ArrayList<String>columns=new ArrayList<String>();
			    columns.add(Constants.FAVOURITE_AMBULANCE);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE, columns, Constants.FAVOURITE_AMBULANCE, Constants.STRING_ONLY, name, Constants.FAVOURITE_AMBULANCE);
				if(data.get(Constants.FAVOURITE_AMBULANCE).size()>0) sName=data.get(Constants.FAVOURITE_AMBULANCE).get(0);
				data.clear();
			}
			else if(type.equals("Blood Bank"))
			{
				ArrayList<String>columns=new ArrayList<String>();
			    columns.add(Constants.FAVOURITE_BLOOD_BANK);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE, columns, Constants.FAVOURITE_BLOOD_BANK, Constants.STRING_ONLY, name, Constants.FAVOURITE_BLOOD_BANK);
				if(data.get(Constants.FAVOURITE_BLOOD_BANK).size()>0) sName=data.get(Constants.FAVOURITE_BLOOD_BANK).get(0);
				data.clear();
			}
			else
			{
				ArrayList<String>columns=new ArrayList<String>();
			    columns.add(Constants.FAVOURITE_PHARMECY);
				HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE, columns, Constants.FAVOURITE_PHARMECY, Constants.STRING_ONLY, name, Constants.FAVOURITE_PHARMECY);
				if(data.get(Constants.FAVOURITE_PHARMECY).size()>0) sName=data.get(Constants.FAVOURITE_PHARMECY).get(0);
				data.clear();
			}
			if(sName.equals(name)) isExists=true;
			else isExists=false;
		}
		catch(Exception e) {}
	}
	
	
	
	
	public void makeFavourite()
	{
		if(type.equals("Ambulance"))
		{
			
			try
			{
					boolean success=false;
					LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
					column.put(Constants.FAVOURITE_AMBULANCE, Constants.IS_STRING);
					
					HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
					ArrayList<String>data1= new ArrayList<String>();
					data1.add(name);
					value.put(0,data1);
					boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column, value);
					if(response==true)
					{
						ArrayList<String>wheres=new ArrayList<String>();
						ArrayList<String>values=new ArrayList<String>();
						int[] matchPositions=new int[6];
						int index=0;
						
						wheres.add(Constants.AMBULANCE_NUMBER_BANGLA_LAT);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lat);
						index++;
						
						wheres.add(Constants.AMBULANCE_NUMBER_BANGLA_LON);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lon);
						index++;
						
						String bName="";
						ArrayList<String>columns2=new ArrayList<String>();
						columns2.add(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
						HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columns2, wheres, matchPositions, values, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
						if(data2.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME).size()>0) bName=data2.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME).get(0);	
						data2.clear();
						
						if(!bName.equals(""))
						{
							LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
							column3.put(Constants.FAVOURITE_BANGLA_AMBULANCE, Constants.IS_STRING);
							
							HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
							ArrayList<String>data3= new ArrayList<String>();
							data3.add(bName);
							value3.put(0,data3);
							boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column3, value3);
							if(response3==true) success=true;
						}
					}
					if(success==false)
					{
						Toast.makeText(getApplicationContext(), "Sorry, failed to add this Ambulance in your favourite list", Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(getApplicationContext(), "This Ambulance is added in your favourite list", Toast.LENGTH_LONG).show();
					}
				
			}
			catch(Exception e) 
			{
				Toast.makeText(getApplicationContext(), "Sorry, failed to add this Ambulance in your favourite list", Toast.LENGTH_LONG).show();
			}
			
		}
		else if(type.equals("Blood Bank"))
		{
			
			try
			{
					boolean success=false;
					LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
					column.put(Constants.FAVOURITE_BLOOD_BANK, Constants.IS_STRING);
					
					HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
					ArrayList<String>data1= new ArrayList<String>();
					data1.add(name);
					value.put(0,data1);
					boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column, value);
					if(response==true)
					{
						ArrayList<String>wheres=new ArrayList<String>();
						ArrayList<String>values=new ArrayList<String>();
						int[] matchPositions=new int[6];
						int index=0;
						
						wheres.add(Constants.BLOOD_BANK_BANGLA_LAT);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lat);
						index++;
						
						wheres.add(Constants.BLOOD_BANK_BANGLA_LON);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lon);
						index++;
						
						String bName="";
						ArrayList<String>columns2=new ArrayList<String>();
						columns2.add(Constants.BLOOD_BANK_BANGLA_NAME);
						HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_BLOOD_BANK_BANGLA, columns2, wheres, matchPositions, values, Constants.BLOOD_BANK_BANGLA_NAME);
						if(data2.get(Constants.BLOOD_BANK_BANGLA_NAME).size()>0) bName=data2.get(Constants.BLOOD_BANK_BANGLA_NAME).get(0);	
						data2.clear();
						
						if(!bName.equals(""))
						{
							LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
							column3.put(Constants.FAVOURITE_BANGLA_BLOOD_BANK, Constants.IS_STRING);
							
							HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
							ArrayList<String>data3= new ArrayList<String>();
							data3.add(bName);
							value3.put(0,data3);
							boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column3, value3);
							if(response3==true) success=true;
						}
					}
					if(success==false)
					{
						Toast.makeText(getApplicationContext(), "Sorry, failed to add this Blood Bank in your favourite list", Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(getApplicationContext(), "This Blood Bank is added in your favourite list", Toast.LENGTH_LONG).show();
					}
				
			}
			catch(Exception e) 
			{
				Toast.makeText(getApplicationContext(), "Sorry, failed to add this Blood Bank in your favourite list", Toast.LENGTH_LONG).show();
			}
			
		}
		else
		{
			
			try
			{
					boolean success=false;
					LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
					column.put(Constants.FAVOURITE_PHARMECY, Constants.IS_STRING);
					
					HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
					ArrayList<String>data1= new ArrayList<String>();
					data1.add(name);
					value.put(0,data1);
					boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column, value);
					if(response==true)
					{
						ArrayList<String>wheres=new ArrayList<String>();
						ArrayList<String>values=new ArrayList<String>();
						int[] matchPositions=new int[6];
						int index=0;
						
						wheres.add(Constants.PHARMECY_BANGLA_LAT);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lat);
						index++;
						
						wheres.add(Constants.PHARMECY_BANGLA_LON);
						matchPositions[index]=Constants.STRING_ONLY;
						values.add(lon);
						index++;
						
						String bName="";
						ArrayList<String>columns2=new ArrayList<String>();
						columns2.add(Constants.PHARMECY_BANGLA_NAME);
						HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_PHARMECY_BANGLA, columns2, wheres, matchPositions, values, Constants.PHARMECY_BANGLA_NAME);
						if(data2.get(Constants.PHARMECY_BANGLA_NAME).size()>0) bName=data2.get(Constants.PHARMECY_BANGLA_NAME).get(0);	
						data2.clear();
						
						if(!bName.equals(""))
						{
							LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
							column3.put(Constants.FAVOURITE_BANGLA_PHARMECY, Constants.IS_STRING);
							
							HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
							ArrayList<String>data3= new ArrayList<String>();
							data3.add(bName);
							value3.put(0,data3);
							boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column3, value3);
							if(response3==true) success=true;
						}
					}
					if(success==false)
					{
						Toast.makeText(getApplicationContext(), "Sorry, failed to add this Pharmecy in your favourite list", Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(getApplicationContext(), "This Pharmecy is added in your favourite list", Toast.LENGTH_LONG).show();
					}
				
			}
			catch(Exception e) 
			{
				Toast.makeText(getApplicationContext(), "Sorry, failed to add this Pharmecy in your favourite list", Toast.LENGTH_LONG).show();
			}
			
		}
		
	}
	
	
	
	
	
	public void makeUnFavourite()
	{
		try
		{
			if(type.equals("Ambulance"))
			{
				boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_AMBULANCE, Constants.STRING_ONLY, name);
				if(isDeleted)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.AMBULANCE_NUMBER_BANGLA_LAT);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.AMBULANCE_NUMBER_BANGLA_LON);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columns2, wheres, matchPositions, values, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					if(data2.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME).size()>0) bName=data2.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_AMBULANCE, Constants.STRING_ONLY, bName);
						if(isDeleted) Toast.makeText(getApplicationContext(), "Your action was performed successfully", Toast.LENGTH_LONG).show();
						else Toast.makeText(getApplicationContext(), "Sorry, failed to perform your action", Toast.LENGTH_LONG).show();
					}
				}
			}
			else if(type.equals("Blood Bank"))
			{
				boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_BLOOD_BANK, Constants.STRING_ONLY, name);
				if(isDeleted)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.BLOOD_BANK_BANGLA_LAT);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.BLOOD_BANK_BANGLA_LON);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.BLOOD_BANK_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_BLOOD_BANK_BANGLA, columns2, wheres, matchPositions, values, Constants.BLOOD_BANK_BANGLA_NAME);
					if(data2.get(Constants.BLOOD_BANK_BANGLA_NAME).size()>0) bName=data2.get(Constants.BLOOD_BANK_BANGLA_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_BLOOD_BANK, Constants.STRING_ONLY, bName);
						if(isDeleted) Toast.makeText(getApplicationContext(), "Your action was performed successfully", Toast.LENGTH_LONG).show();
						else Toast.makeText(getApplicationContext(), "Sorry, failed to perform your action", Toast.LENGTH_LONG).show();
					}
				}
			}
			else
			{
				boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_PHARMECY, Constants.STRING_ONLY, name);
				if(isDeleted)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.PHARMECY_BANGLA_LAT);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.PHARMECY_BANGLA_LON);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.PHARMECY_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_PHARMECY_BANGLA, columns2, wheres, matchPositions, values, Constants.PHARMECY_BANGLA_NAME);
					if(data2.get(Constants.PHARMECY_BANGLA_NAME).size()>0) bName=data2.get(Constants.PHARMECY_BANGLA_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_PHARMECY, Constants.STRING_ONLY, bName);
						if(isDeleted) Toast.makeText(getApplicationContext(), "Your action was performed successfully", Toast.LENGTH_LONG).show();
						else Toast.makeText(getApplicationContext(), "Sorry, failed to perform your action", Toast.LENGTH_LONG).show();
					}
				}
			}
		}
		catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), "Sorry, failed to perform your action", Toast.LENGTH_LONG).show();
		}
	}
	
	
	
	

}
