package com.w3dreamers.hospitalsofchittagong;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

public class GuideActivityBangla extends Activity{
	
	TextView txtTitle;
	WebView web;
	
	boolean supported;
	Typeface banglaFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_development);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		web=(WebView)findViewById(R.id.webView1);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		
		String title="চট্টগ্রামের হাসপাতালসমূহ";
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
		}
		else txtTitle.setText(title);
		
		WebSettings webSettings=web.getSettings();
		webSettings.setAppCacheEnabled(false);
		webSettings.setBlockNetworkImage(true);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setGeolocationEnabled(false);
		webSettings.setNeedInitialFocus(false);
		webSettings.setSaveFormData(false);
		
		web.getSettings().setJavaScriptEnabled(true);
		//web.getSettings().setBuiltInZoomControls(true);
	    web.loadUrl("file:///android_asset/web_pages/userguidebangla.html");
		
		
	}

}
