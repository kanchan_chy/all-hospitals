package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.impl.entity.StrictContentLengthStrategy;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Service24ActivityBangla extends Activity{
	
	TextView txtTitle;
	TextView[] txtTabs=new TextView[6];
	LinearLayout[] linearTabs=new LinearLayout[6];
	ListView list;
	int[] linearIds={R.id.linearHospital,R.id.linearAmbulance,R.id.linearBloodBank,R.id.linearPharmecy};
	int[] txtIds={R.id.txtHospital,R.id.txtAmbulance,R.id.txtBloodBank,R.id.txtPharmecy};
	String[] tabs=new String[6];
	
	int selected;
	CustomAdapterBangla adapter0,adapter1,adapter2,adapter3;
	
	ArrayList<String>hospitals=new ArrayList<String>();
	ArrayList<String>ambulances=new ArrayList<String>();
	ArrayList<String>bloods=new ArrayList<String>();
	ArrayList<String>pharmecies=new ArrayList<String>();
	ArrayList<String>amb_numbers=new ArrayList<String>();
	
	boolean supported;
	Typeface banglaFont;
	
	DbHelper dbOpenHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.service_24_layout);
		list=(ListView)findViewById(R.id.listView1);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		for(int i=0;i<linearIds.length;i++)
		{
			txtTabs[i]=(TextView)findViewById(txtIds[i]);
			linearTabs[i]=(LinearLayout)findViewById(linearIds[i]);
		}
		
		hospitals=(ArrayList<String>)getIntent().getSerializableExtra("hospitals");
		ambulances=(ArrayList<String>)getIntent().getSerializableExtra("ambulances");
		bloods=(ArrayList<String>)getIntent().getSerializableExtra("banks");
		pharmecies=(ArrayList<String>)getIntent().getSerializableExtra("pharmecies");
		amb_numbers=(ArrayList<String>)getIntent().getSerializableExtra("amb_numbers");
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		for(int i=0;i<txtIds.length;i++)
		{
			txtTabs[i].setTypeface(banglaFont);
		}
		
		String title="২৪ ঘন্টা সেবাসমূহ";
		tabs[0]="হাসপাতাল";
		tabs[1]="অ্যাম্বুল্যান্স";
		tabs[2]="ব্লাড ব্যাংক";
		tabs[3]="ফার্মেসি";
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			for(int i=0;i<txtIds.length;i++)
			{
				SpannableString convertedTab=AndroidCustomFontSupport.getCorrectedBengaliFormat(tabs[i], banglaFont, (float)1);
				txtTabs[i].setText(convertedTab);
			}
		}
		else
		{
			txtTitle.setText(title);
			for(int i=0;i<txtIds.length;i++)
			{
				txtTabs[i].setText(tabs[i]);
			}
		}
		
		
		adapter0=new CustomAdapterBangla(this, hospitals,"হাসপাতাল");
		adapter1=new CustomAdapterBangla(this, ambulances,"অ্যাম্বুল্যান্স");
		adapter2=new CustomAdapterBangla(this, bloods,"ব্লাড ব্যাংক");
		adapter3=new CustomAdapterBangla(this, pharmecies,"ফার্মেসি");
		list.setAdapter(adapter0);
		selected=0;
		
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		linearTabs[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[0].setBackgroundResource(R.drawable.selected);	
				//adapter=new CustomAdapterBangla(Service24ActivityBangla.this,hospitals);
				list.setAdapter(adapter0);
				selected=0;
			}
		});
		
		
		linearTabs[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[1].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapterBangla(Service24ActivityBangla.this,ambulances);
				list.setAdapter(adapter1);
				selected=1;
			}
		});
		
		
		linearTabs[2].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[2].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapterBangla(Service24ActivityBangla.this,bloods);
				list.setAdapter(adapter2);
				selected=2;
			}
		});
		
		
		linearTabs[3].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[3].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapterBangla(Service24ActivityBangla.this,pharmecies);
				list.setAdapter(adapter3);
				selected=3;
			}
		});
		
		
		
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
					try
					{
						String number="",address="",lat="",lon="";
						if(selected==0)
						{
							//String name=(String)parent.getItemAtPosition(position);					
							String name=hospitals.get(position);
							HospitalInfoRetreiverBangla retreiver=new HospitalInfoRetreiverBangla(Service24ActivityBangla.this);
							retreiver.DetailActivityCaller(name);
						}
						else if(selected==1)
						{
							//String name=(String)parent.getItemAtPosition(position);
							String name=ambulances.get(position);
							ArrayList<String>columnName=new ArrayList<String>();
							columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER);
							columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_ADDRESS);
							columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_LAT);
							columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_LON);
							HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columnName, Constants.AMBULANCE_NUMBER_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
							if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER).size()>0) number=data.get(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER).get(0);
							if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_ADDRESS).size()>0) address=data.get(Constants.AMBULANCE_NUMBER_BANGLA_ADDRESS).get(0);
							if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_LAT).size()>0) lat=data.get(Constants.AMBULANCE_NUMBER_BANGLA_LAT).get(0);
							if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_LON).size()>0) lon=data.get(Constants.AMBULANCE_NUMBER_BANGLA_LON).get(0);
							
							//showCallDialog(number);
							Intent in=new Intent(Service24ActivityBangla.this,DetailsPharmecyActivityBangla.class);
							in.putExtra("title",tabs[1]);
							in.putExtra("name", name);
							in.putExtra("number", number);
							in.putExtra("address", address);
							in.putExtra("lat", lat);
							in.putExtra("lon", lon);
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						}
						else if(selected==2)
						{
							//String name=(String)parent.getItemAtPosition(position);
							String name=bloods.get(position);
							ArrayList<String>columnName=new ArrayList<String>();
							columnName.add(Constants.BLOOD_BANK_BANGLA_NUMBER);
							columnName.add(Constants.BLOOD_BANK_BANGLA_ADDRESS);
							columnName.add(Constants.BLOOD_BANK_BANGLA_LAT);
							columnName.add(Constants.BLOOD_BANK_BANGLA_LON);
							HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK_BANGLA, columnName, Constants.BLOOD_BANK_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.BLOOD_BANK_BANGLA_NAME);
							if(data.get(Constants.BLOOD_BANK_BANGLA_NUMBER).size()>0) number=data.get(Constants.BLOOD_BANK_BANGLA_NUMBER).get(0);
							if(data.get(Constants.BLOOD_BANK_BANGLA_ADDRESS).size()>0) address=data.get(Constants.BLOOD_BANK_BANGLA_ADDRESS).get(0);
							if(data.get(Constants.BLOOD_BANK_BANGLA_LAT).size()>0) lat=data.get(Constants.BLOOD_BANK_BANGLA_LAT).get(0);
							if(data.get(Constants.BLOOD_BANK_BANGLA_LON).size()>0) lon=data.get(Constants.BLOOD_BANK_BANGLA_LON).get(0);
							Intent in=new Intent(Service24ActivityBangla.this,DetailsPharmecyActivityBangla.class);
							in.putExtra("title",tabs[2]);
							in.putExtra("name", name);
							in.putExtra("number", number);
							in.putExtra("address", address);
							in.putExtra("lat", lat);
							in.putExtra("lon", lon);
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						}
						else if(selected==3)
						{
							//String name=(String)parent.getItemAtPosition(position);
							String name=pharmecies.get(position);
							ArrayList<String>columnName=new ArrayList<String>();
							columnName.add(Constants.PHARMECY_BANGLA_NUMBER);
							columnName.add(Constants.PHARMECY_BANGLA_ADDRESS);
							columnName.add(Constants.PHARMECY_BANGLA_LAT);
							columnName.add(Constants.PHARMECY_BANGLA_LON);
							HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY_BANGLA, columnName, Constants.PHARMECY_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.PHARMECY_BANGLA_NAME);
							if(data.get(Constants.PHARMECY_BANGLA_NUMBER).size()>0) number=data.get(Constants.PHARMECY_BANGLA_NUMBER).get(0);
							if(data.get(Constants.PHARMECY_BANGLA_ADDRESS).size()>0) address=data.get(Constants.PHARMECY_BANGLA_ADDRESS).get(0);
							if(data.get(Constants.PHARMECY_BANGLA_LAT).size()>0) lat=data.get(Constants.PHARMECY_BANGLA_LAT).get(0);
							if(data.get(Constants.PHARMECY_BANGLA_LON).size()>0) lon=data.get(Constants.PHARMECY_BANGLA_LON).get(0);
							Intent in=new Intent(Service24ActivityBangla.this,DetailsPharmecyActivityBangla.class);
							in.putExtra("title",tabs[3]);
							in.putExtra("name", name);
							in.putExtra("number", number);
							in.putExtra("address", address);
							in.putExtra("lat", lat);
							in.putExtra("lon", lon);
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						}
					}
					catch(Exception e)
					{
						
					}
			}
		});
		
		
		
		
	}
	
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	

	
	
	

}
