package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HospitalListActivity extends Activity{
	
	TextView txtTitle,txtOption,txtFavourite;
	ImageView imgSearchOption,imgIcon2;
	LinearLayout linearSearchOption,linearTitle,linearSearch,linearOption,linearFavourite;
	ListView list;
	EditText edtSearch;
	
	boolean titleVisibility;
	
	ArrayList<String> names=new ArrayList<String>();
	String title,option;
	
	int selected;
	CustomAdapter adapter,adapter2;
	//ArrayAdapter<String> adapter2;
	
	DbHelper dbOpenHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.hospital_list_layout2);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtOption=(TextView)findViewById(R.id.txtOption);
		txtFavourite=(TextView)findViewById(R.id.txtFavourite);
		imgSearchOption=(ImageView)findViewById(R.id.imgSearchOption);
		imgIcon2=(ImageView)findViewById(R.id.imgIcon2);
		linearSearchOption=(LinearLayout)findViewById(R.id.linearSearchOption);
		linearTitle=(LinearLayout)findViewById(R.id.linearTitle);
		linearSearch=(LinearLayout)findViewById(R.id.linearSearch);
		linearOption=(LinearLayout)findViewById(R.id.linearOption);
		linearFavourite=(LinearLayout)findViewById(R.id.linearFavourite);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		list=(ListView)findViewById(R.id.listView1);
		
		linearSearch.setVisibility(View.INVISIBLE);
		titleVisibility=true;
		
		title=getIntent().getExtras().getString("title");
		names=(ArrayList<String>)getIntent().getSerializableExtra("names");
		
		if(title.equals("Hospitals of Chittagong")) option="Hospital";
		else option=title;
		
		txtTitle.setText(title);
		txtOption.setText(option);
		txtFavourite.setText("Favourite List");
		
		adapter=new CustomAdapter(this,names,option);	
		list.setAdapter(adapter);
		selected=0;
		
		//adapter2=new ArrayAdapter<String>(this, R.layout.item, R.id.txtItemName, hospitals);
		//list.setAdapter(adapter2);
		
		list.setTextFilterEnabled(true);
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		
		linearOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearOption.setBackgroundResource(R.drawable.selected);
				linearFavourite.setBackgroundResource(R.drawable.unselected);
				list.setAdapter(adapter);
				selected=0;
				list.setTextFilterEnabled(true);
			}
		});
		
		
		linearFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setFavouriteList();
			}
		});
		
		
		
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String number="",address="",lat="",lon="";
				try
				{
					String name=(String)parent.getItemAtPosition(pos);
					if(title.equals("Ambulance"))
					{
						ArrayList<String>columnName=new ArrayList<String>();
						columnName.add(Constants.AMBULANCE_NUMBER_NUMBER);
						columnName.add(Constants.AMBULANCE_NUMBER_ADDRESS);
						columnName.add(Constants.AMBULANCE_NUMBER_LAT);
						columnName.add(Constants.AMBULANCE_NUMBER_LON);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER, columnName, Constants.AMBULANCE_NUMBER_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_NAME);
						if(data.get(Constants.AMBULANCE_NUMBER_NUMBER).size()>0) number=data.get(Constants.AMBULANCE_NUMBER_NUMBER).get(0);
						if(data.get(Constants.AMBULANCE_NUMBER_ADDRESS).size()>0) address=data.get(Constants.AMBULANCE_NUMBER_ADDRESS).get(0);
						if(data.get(Constants.AMBULANCE_NUMBER_LAT).size()>0) lat=data.get(Constants.AMBULANCE_NUMBER_LAT).get(0);
						if(data.get(Constants.AMBULANCE_NUMBER_LON).size()>0) lon=data.get(Constants.AMBULANCE_NUMBER_LON).get(0);
   	   					
   						//showCallDialog(number);
						Intent in=new Intent(HospitalListActivity.this,DetailsPharmecyActivity.class);
						in.putExtra("title","Ambulance");
						in.putExtra("name", name);
						in.putExtra("number", number);
						in.putExtra("address", address);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
					else if(title.equals("Blood Bank"))
					{
						ArrayList<String>columnName=new ArrayList<String>();
						columnName.add(Constants.BLOOD_BANK_NUMBER);
						columnName.add(Constants.BLOOD_BANK_ADDRESS);
						columnName.add(Constants.BLOOD_BANK_LAT);
						columnName.add(Constants.BLOOD_BANK_LON);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK, columnName, Constants.BLOOD_BANK_NAME, Constants.STRING_ONLY, name, Constants.BLOOD_BANK_NAME);
						if(data.get(Constants.BLOOD_BANK_NUMBER).size()>0) number=data.get(Constants.BLOOD_BANK_NUMBER).get(0);
						if(data.get(Constants.BLOOD_BANK_ADDRESS).size()>0) address=data.get(Constants.BLOOD_BANK_ADDRESS).get(0);
						if(data.get(Constants.BLOOD_BANK_LAT).size()>0) lat=data.get(Constants.BLOOD_BANK_LAT).get(0);
						if(data.get(Constants.BLOOD_BANK_LON).size()>0) lon=data.get(Constants.BLOOD_BANK_LON).get(0);
						Intent in=new Intent(HospitalListActivity.this,DetailsPharmecyActivity.class);
						in.putExtra("title","Blood Bank");
						in.putExtra("name", name);
						in.putExtra("number", number);
						in.putExtra("address", address);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
					else if(title.equals("Pharmecy"))
					{
						ArrayList<String>columnName=new ArrayList<String>();
						columnName.add(Constants.PHARMECY_NUMBER);
						columnName.add(Constants.PHARMECY_ADDRESS);
						columnName.add(Constants.PHARMECY_LAT);
						columnName.add(Constants.PHARMECY_LON);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY, columnName, Constants.PHARMECY_NAME, Constants.STRING_ONLY, name, Constants.PHARMECY_NAME);
						if(data.get(Constants.PHARMECY_NUMBER).size()>0) number=data.get(Constants.PHARMECY_NUMBER).get(0);
						if(data.get(Constants.PHARMECY_ADDRESS).size()>0) address=data.get(Constants.PHARMECY_ADDRESS).get(0);
						if(data.get(Constants.PHARMECY_LAT).size()>0) lat=data.get(Constants.PHARMECY_LAT).get(0);
						if(data.get(Constants.PHARMECY_LON).size()>0) lon=data.get(Constants.PHARMECY_LON).get(0);
						Intent in=new Intent(HospitalListActivity.this,DetailsPharmecyActivity.class);
						in.putExtra("title","Pharmecy");
						in.putExtra("name", name);
						in.putExtra("number", number);
						in.putExtra("address", address);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
					else
					{
						HospitalInfoRetreiver retreiver=new HospitalInfoRetreiver(HospitalListActivity.this);
						retreiver.DetailActivityCaller(name);
					}
					
				}
				catch(Exception e) {}
				
			}
		});
		
		
		linearSearchOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				linearTitle.setVisibility(View.INVISIBLE);
				linearSearch.setVisibility(View.VISIBLE);
				titleVisibility=false;
			}
		});
		
		
         imgIcon2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearSearch.setVisibility(View.INVISIBLE);
				linearTitle.setVisibility(View.VISIBLE);
				titleVisibility=true;
			}
		});
         
         
         edtSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int start, int before, int count) {
				// TODO Auto-generated method stub
				adapter.getFilter().filter(cs);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
	}
	
	
	
	
	
	public void setFavouriteList()
	{
		try
		{
			ArrayList<String>favourites=new ArrayList<String>();
			ArrayList<String>columnName=new ArrayList<String>();
			String mColumn;
			if(title.equals("Ambulance")) mColumn=Constants.FAVOURITE_AMBULANCE;
			else if(title.equals("Blood Bank")) mColumn=Constants.FAVOURITE_BLOOD_BANK;
			else if(title.equals("Pharmecy")) mColumn=Constants.FAVOURITE_PHARMECY;
			else mColumn=Constants.FAVOURITE_HOSPITAL;
			columnName.add(mColumn);
			HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_FAVOURITE, columnName,mColumn);
			favourites=data.get(mColumn);
			
			ArrayList<String>rFavourites=new ArrayList<String>();
			for(int i=0;i<favourites.size();i++)
			{
				if(favourites.get(i)!=null) rFavourites.add(favourites.get(i));
			}
			
			if(rFavourites.size()<=0)
			{
				String text="No ";
				if(title.equals("Ambulance")||title.equals("Blood Bank")||title.equals("Pharmecy")) text+=title+" ";
				else text+="Hospital ";
				text+="was added in your favourite list";
				Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
			}
			else
			{
				linearOption.setBackgroundResource(R.drawable.unselected);
				linearFavourite.setBackgroundResource(R.drawable.selected);
				adapter2=new CustomAdapter(HospitalListActivity.this,rFavourites,option);
				list.setAdapter(adapter2);
				selected=1;
				list.setTextFilterEnabled(true);
			}
		}
		catch(Exception e)
		{
			
		}
	}
	
	
	
	

}
