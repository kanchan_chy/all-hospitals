package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter {
	
	//Typeface banglaFont;
	//SharedPreferences prefBangla;
	//boolean supported;

	
	LayoutInflater inFlater;
	
	Context context;
	ArrayList<String>itemList;
	String type;
	
	ArrayList<String>dataList;
	private DataFilter filter;
	
	public CustomAdapter(Context context,ArrayList<String>itemList, String type)
	{
		this.context=context;
		this.itemList=itemList;
		this.type=type;
		
		this.dataList=itemList;
		
		//banglaFont = Typeface.createFromAsset(context.getAssets(),"font/solaimanlipinormal.ttf");
		//prefBangla=context.getSharedPreferences("BanglaFont",0);
		//supported=prefBangla.getBoolean("supported", false);
		
		inFlater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return itemList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		  
		
		ViewHolder holder=null;
		if(convertView==null)
		{
			convertView=inFlater.inflate(R.layout.item, null);
		  
		    holder= new ViewHolder();
		   holder.itemName=(TextView)convertView.findViewById(R.id.txtItemName);
		   holder.imgItem=(ImageView)convertView.findViewById(R.id.imgItem);
		//   holder.linear1=(LinearLayout)convertView.findViewById(R.id.linearList1);
		//   holder.linear2=(LinearLayout)convertView.findViewById(R.id.linearList2);
		    convertView.setTag(holder);
		}
		else
		{   
			    holder=(ViewHolder)convertView.getTag();
		}
		
	/*	int pos=position%4;
		if(pos==0)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#5BB05F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_green);
		}
		else if(pos==1)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#DF483F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_red);
		}
		else if(pos==2)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#FAA741"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_yellow);
		}
		else
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#1E87C8"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_blue);
		}   */
		
		//holder.itemName.setTypeface(banglaFont);
		String text=itemList.get(position);
	/*	if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			holder.menuName.setText(convertedText);
		}
		else holder.menuName.setText(text);  */
		holder.itemName.setText(text);
		
		if(type.equals("Hospital")) holder.imgItem.setImageResource(R.drawable.hospital2);
		else if(type.equals("Ambulance")) holder.imgItem.setImageResource(R.drawable.ambulance2);
		else if(type.equals("Blood Bank")) holder.imgItem.setImageResource(R.drawable.blood2);
		else if(type.equals("Pharmecy")) holder.imgItem.setImageResource(R.drawable.pharmecy2);
		else if(type.equals("Category")) holder.imgItem.setImageResource(R.drawable.category2);
		
		return convertView;
	}

	
	public static class ViewHolder {
	    public TextView itemName;
	    ImageView imgItem;
	  //  public LinearLayout linear1,linear2;
	    
	}
	
	
	
	
	  public Filter getFilter() {
	   if (filter == null){
	    filter  = new DataFilter();
	   }
	   return filter;
	  }
	
	
	
	
	private class DataFilter extends Filter
	  {

	   @Override
	   protected FilterResults performFiltering(CharSequence constraint) {

	    constraint = constraint.toString().toLowerCase();
	    FilterResults result = new FilterResults();
	    if(constraint != null && constraint.toString().length() > 0)
	    {
	    ArrayList<String> filteredItems = new ArrayList<String>();

	    for(int i = 0, l = dataList.size(); i < l; i++)
	    {
	     String data = dataList.get(i);
	     if(data.toLowerCase().contains(constraint))
	      filteredItems.add(data);
	    }
	    result.count = filteredItems.size();
	    result.values = filteredItems;
	    }
	    else
	    {
	     synchronized(this)
	     {
	      result.values =dataList;
	      result.count =dataList.size();
	     }
	    }
	    return result;
	   }

	   @Override
	   protected void publishResults(CharSequence constraint, FilterResults results) {

		   if(results.count== 0){
			   notifyDataSetInvalidated();
			   }
			   else{
			   itemList=(ArrayList) results.values;
			   notifyDataSetChanged();
			   }

	   }

	 }
	

	
	

}
