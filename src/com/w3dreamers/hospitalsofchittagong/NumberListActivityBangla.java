package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NumberListActivityBangla extends Activity{
	
	TextView txtTitle;
	ListView list;
	
	CustomAdapter2Bangla adapter;
	
	String title;
	ArrayList<String>names=new ArrayList<String>();
	ArrayList<String>allEmergencies=new ArrayList<String>();
	
	ArrayList<String>itemBodies=new ArrayList<String>();
	ArrayList<String>itemClicks=new ArrayList<String>();
	
	boolean supported;
	Typeface banglaFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.number_list_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		list=(ListView)findViewById(R.id.listView1);
		
		title=getIntent().getExtras().getString("title");
		names=(ArrayList<String>)getIntent().getSerializableExtra("names");
		allEmergencies=(ArrayList<String>)getIntent().getSerializableExtra("allContacts");
		
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		txtTitle.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
		}	
		else txtTitle.setText(title);
		
		prepareListData();
		
		adapter=new CustomAdapter2Bangla(this, names, allEmergencies, itemClicks);
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				showCallDialog(allEmergencies.get(0));
			}
		});
		
		
	}
	
	
	public void prepareListData()
	{
		for(int i=0;i<names.size();i++)
		{
			itemClicks.add("কল করতে ক্লিক করুন");
		}
	}
	
	
	
	public void showCallDialog(final String number) 
	{
		
		/*	
		try
        {
            ContentResolver cr = this.getContentResolver();
            ContentValues cv = new ContentValues();
           // cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "New Name");
            cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER,number);
            cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);

            Toast.makeText(this, "Contact added", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            TextView tv = new TextView(this);
            tv.setText(e.toString());
            setContentView(tv);
        }   */
		
		LayoutInflater inflater=LayoutInflater.from(NumberListActivityBangla.this);
		View view=inflater.inflate(R.layout.option_dialog, null);
		LinearLayout linearSendVia,linearSave,linearCall,linearClose;
		TextView txtSendVia,txtSave,txtCall,txtClose,txtDialogTitle;
		linearSendVia=(LinearLayout)view.findViewById(R.id.linearSendVia);
		linearSave=(LinearLayout)view.findViewById(R.id.linearSave);
		linearCall=(LinearLayout)view.findViewById(R.id.linearCall);
		linearClose=(LinearLayout)view.findViewById(R.id.linearClose);
		txtDialogTitle=(TextView)view.findViewById(R.id.txtTitle);
		txtSendVia=(TextView)view.findViewById(R.id.txtSendVia);
		txtSave=(TextView)view.findViewById(R.id.txtSave);
		txtCall=(TextView)view.findViewById(R.id.txtCall);
		txtClose=(TextView)view.findViewById(R.id.txtClose);
		
		String dialogTitle="বাছাই করুন";
		String send="ক্ষুদেবার্তার মাধ্যমে পাঠান";
		String save="মুঠোফোনে সংরক্ষণ করুন";
		String call="ফোন করুন";
		String close="বন্ধ করুন";
		
		txtDialogTitle.setTypeface(banglaFont);;
		txtSendVia.setTypeface(banglaFont);
		txtSave.setTypeface(banglaFont);
		txtCall.setTypeface(banglaFont);
		txtClose.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedDialogTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(dialogTitle, banglaFont, (float)1);
			SpannableString convertedSend=AndroidCustomFontSupport.getCorrectedBengaliFormat(send, banglaFont, (float)1);
			SpannableString convertedSave=AndroidCustomFontSupport.getCorrectedBengaliFormat(save, banglaFont, (float)1);
			SpannableString convertedCall=AndroidCustomFontSupport.getCorrectedBengaliFormat(call, banglaFont, (float)1);
			SpannableString convertedClose=AndroidCustomFontSupport.getCorrectedBengaliFormat(close, banglaFont, (float)1);
			txtDialogTitle.setText(convertedDialogTitle);
			txtSendVia.setText(convertedSend);
			txtSave.setText(convertedSave);
			txtCall.setText(convertedCall);
			txtClose.setText(convertedClose);
		}
		else
		{
			txtDialogTitle.setText(dialogTitle);
			txtSendVia.setText(send);
			txtSave.setText(save);
			txtCall.setText(call);
			txtClose.setText(close);
		}
		
		AlertDialog.Builder builder=new AlertDialog.Builder(NumberListActivityBangla.this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearSendVia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				String uriStr = "sms:";
				Uri smsUri = Uri.parse(uriStr);
				Intent smsIntent = new Intent(Intent.ACTION_VIEW, smsUri);
				smsIntent.putExtra("sms_body", number);
				startActivity(smsIntent);
				
			}
		});
		
		linearSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				  ArrayList<ContentValues> data = new ArrayList<ContentValues>();

				  ContentValues row1 = new ContentValues();
				  row1.put(Phone.NUMBER, number);
				  data.add(row1);  

				  Intent intent = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
				  intent.putExtra(Insert.PHONE,number);
				  startActivity(intent); 
				
			}
		});
		
		linearCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// put the phone number in number variable
		        //String number="01.....";
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phone="tel:"+number;
				try {
		                callIntent.setData(Uri.parse(phone));
		                startActivity(callIntent);
		            }
		        catch (android.content.ActivityNotFoundException ex) 
		        {
		        	Toast.makeText(getApplicationContext(),"Call faild, please try again later.", Toast.LENGTH_LONG).show();
		        }   
				
			}
		});
		
		linearClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
		
		
	}
	
	

}
