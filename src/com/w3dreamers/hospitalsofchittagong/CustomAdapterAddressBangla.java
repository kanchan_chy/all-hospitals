package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.List;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapterAddressBangla extends BaseAdapter {
	
	//Typeface banglaFont;
	//SharedPreferences prefBangla;
	//boolean supported;

	
	LayoutInflater inFlater;
	
	Context context;
	
	ArrayList<String>itemList;
	ArrayList<String>addresses;
	ArrayList<String>directions;
	
	ArrayList<String>dataList;
	private DataFilter filter;
	
	boolean supported;
	Typeface banglaFont;
	
	public CustomAdapterAddressBangla(Context context,ArrayList<String>itemList,ArrayList<String>addresses,ArrayList<String>directions)
	{
		this.context=context;
		this.itemList=itemList;
		this.addresses=addresses;
		this.directions=directions;
		
		this.dataList=itemList;
		
		//banglaFont = Typeface.createFromAsset(context.getAssets(),"font/solaimanlipinormal.ttf");
		//prefBangla=context.getSharedPreferences("BanglaFont",0);
		//supported=prefBangla.getBoolean("supported", false);
		
		SharedPreferences prefSupport=context.getSharedPreferences("BanglaLibrary", 0);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(context.getAssets(), "font/solaimanlipinormal.ttf");
		
		inFlater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return itemList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		  
		
		ViewHolder holder=null;
		if(convertView==null)
		{
			convertView=inFlater.inflate(R.layout.item_address, null);
		  
		   holder= new ViewHolder();
		//   holder.linear1=(LinearLayout)convertView.findViewById(R.id.linearList1);
		//   holder.linear2=(LinearLayout)convertView.findViewById(R.id.linearList2);
		   holder.txtItemTitle=(TextView)convertView.findViewById(R.id.txtItemTitle);
		   holder.txtAddress=(TextView)convertView.findViewById(R.id.txtAddress);
		   holder.txtAddressBody=(TextView)convertView.findViewById(R.id.txtAddressBody);
		   holder.txtDirection=(TextView)convertView.findViewById(R.id.txtDirection);
		   holder.txtDirectionBody=(TextView)convertView.findViewById(R.id.txtDirectionBody);
		   holder.txtClick=(TextView)convertView.findViewById(R.id.txtClick);
		   
		    convertView.setTag(holder);
		}
		else
		{   
			    holder=(ViewHolder)convertView.getTag();
		}
		
	/*	int pos=position%4;
		if(pos==0)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#5BB05F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_green);
		}
		else if(pos==1)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#DF483F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_red);
		}
		else if(pos==2)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#FAA741"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_yellow);
		}
		else
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#1E87C8"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_blue);
		}   */
		
		String title=itemList.get(position);
		String addressBody=addresses.get(position);
		String directionBody=directions.get(position);
		
		String address="ঠিকানা";
		String direction="যাওয়ার পথ";
		String click="ম্যাপ দেখতে ক্লিক করুন";
		
		holder.txtItemTitle.setTypeface(banglaFont);
		holder.txtAddress.setTypeface(banglaFont);
		holder.txtAddressBody.setTypeface(banglaFont);
		holder.txtDirection.setTypeface(banglaFont);
		holder.txtDirectionBody.setTypeface(banglaFont);
		holder.txtClick.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title,banglaFont, (float) 1);
			SpannableString convertedAddress=AndroidCustomFontSupport.getCorrectedBengaliFormat(address,banglaFont, (float) 1);
			SpannableString convertedDirection=AndroidCustomFontSupport.getCorrectedBengaliFormat(direction,banglaFont, (float) 1);
			SpannableString convertedAddressBody=AndroidCustomFontSupport.getCorrectedBengaliFormat(addressBody,banglaFont, (float) 1);
			SpannableString convertedDirectionBody=AndroidCustomFontSupport.getCorrectedBengaliFormat(directionBody,banglaFont, (float) 1);
			SpannableString convertedClick=AndroidCustomFontSupport.getCorrectedBengaliFormat(click,banglaFont, (float) 1);
			holder.txtItemTitle.setText(convertedTitle);
			holder.txtAddress.setText(convertedAddress);
			holder.txtDirection.setText(convertedDirection);
			holder.txtAddressBody.setText(convertedAddressBody);
			holder.txtDirectionBody.setText(convertedDirectionBody);
			holder.txtClick.setText(convertedClick);
		}
		else
		{
			holder.txtItemTitle.setText(title);  
			holder.txtAddress.setText(address);
			holder.txtDirection.setText(direction);
			holder.txtAddressBody.setText(addressBody);
			holder.txtDirectionBody.setText(directionBody);
			holder.txtClick.setText(click);
		}
		
		return convertView;
	}

	
	public static class ViewHolder {
	    public TextView txtItemTitle,txtAddress,txtAddressBody,txtDirection,txtDirectionBody,txtClick;
	 //   public LinearLayout linear1,linear2;
	    
	}
	
	
	
	
	
	public Filter getFilter() {
		   if (filter == null){
		    filter  = new DataFilter();
		   }
		   return filter;
		  }
		
		
		
		
		private class DataFilter extends Filter
		  {

		   @Override
		   protected FilterResults performFiltering(CharSequence constraint) {

		    constraint = constraint.toString().toLowerCase();
		    FilterResults result = new FilterResults();
		    if(constraint != null && constraint.toString().length() > 0)
		    {
		    ArrayList<String> filteredItems = new ArrayList<String>();

		    for(int i = 0, l = dataList.size(); i < l; i++)
		    {
		     String data = dataList.get(i);
		     if(data.toLowerCase().contains(constraint))
		      filteredItems.add(data);
		    }
		    result.count = filteredItems.size();
		    result.values = filteredItems;
		    }
		    else
		    {
		     synchronized(this)
		     {
		      result.values =dataList;
		      result.count =dataList.size();
		     }
		    }
		    return result;
		   }

		   @Override
		   protected void publishResults(CharSequence constraint, FilterResults results) {

			   if(results.count== 0){
				   notifyDataSetInvalidated();
				   }
				   else{
				   itemList=(ArrayList) results.values;
				   notifyDataSetChanged();
				   }

		   }

		 }

	
	

}
