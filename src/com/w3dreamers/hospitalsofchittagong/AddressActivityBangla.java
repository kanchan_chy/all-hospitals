package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class AddressActivityBangla extends Activity{
	
	TextView txtTitle;
	ImageView imgSearchOption,imgIcon2;
	LinearLayout linearSearchOption,linearTitle,linearSearch;
	ListView list;
	EditText edtSearch;
	
	ArrayList<String> hospitals=new ArrayList<String>();
	ArrayList<String> ids=new ArrayList<String>();
	ArrayList<String> addresses=new ArrayList<String>();
	ArrayList<String> directions=new ArrayList<String>();
	ArrayList<String> lats=new ArrayList<String>();
	ArrayList<String> lons=new ArrayList<String>();
	String title;
	
	CustomAdapterAddressBangla adapter;
	boolean supported;
	Typeface banglaFont;
	
	boolean titleVisibility;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.address_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		imgSearchOption=(ImageView)findViewById(R.id.imgSearchOption);
		linearSearchOption=(LinearLayout)findViewById(R.id.linearSearchOption);
		list=(ListView)findViewById(R.id.listView1);
		imgIcon2=(ImageView)findViewById(R.id.imgIcon2);
		linearTitle=(LinearLayout)findViewById(R.id.linearTitle);
		linearSearch=(LinearLayout)findViewById(R.id.linearSearch);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		
		linearSearch.setVisibility(View.INVISIBLE);
		titleVisibility=true;
		
		hospitals=(ArrayList<String>)getIntent().getSerializableExtra("hospitals");
		ids=(ArrayList<String>)getIntent().getSerializableExtra("ids");
		addresses=(ArrayList<String>)getIntent().getSerializableExtra("addresses");
		directions=(ArrayList<String>)getIntent().getSerializableExtra("directions");
		lats=(ArrayList<String>)getIntent().getSerializableExtra("lats");
		lons=(ArrayList<String>)getIntent().getSerializableExtra("lons");
		
		title="চট্টগ্রামের হাসপাতালসমূহ";
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		edtSearch.setTypeface(banglaFont);
		
		String hint="হাসপাতালের নাম টাইপ করুন";
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedHint=AndroidCustomFontSupport.getCorrectedBengaliFormat(hint, banglaFont, (float)1);
			edtSearch.setHint(convertedHint);
		}	
		else
		{
			txtTitle.setText(title);
			edtSearch.setHint(hint);
		}
		
		adapter=new CustomAdapterAddressBangla(this, hospitals, addresses, directions);
		
		list.setAdapter(adapter);
		
		list.setTextFilterEnabled(true);
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String name=(String)parent.getItemAtPosition(pos);
				
				//String name=hospitals.get(pos);
				//String id=ids.get(pos);
				String lat=lats.get(pos);
				String lon=lons.get(pos);
				if(isConnectingInternet())
				{
					if(isMapAvailable())
					{
						Intent in=new Intent(AddressActivityBangla.this,LocationActivityBangla.class);
						in.putExtra("name", name);
						//in.putExtra("id", id);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
				}
				else
				{
					
					LayoutInflater li = LayoutInflater.from(AddressActivityBangla.this);
					View promptsView = li.inflate(R.layout.internet_dialog, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(AddressActivityBangla.this);
					builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.show();
					TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
					TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
					TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
					TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
					
					LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
					LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
					
					dialogTitle.setTypeface(banglaFont);
					txtMain.setTypeface(banglaFont);
					txtOptional.setTypeface(banglaFont);
					txtOk.setTypeface(banglaFont);
					txtCancel.setTypeface(banglaFont);
					
					String header="সতর্কবার্তা";
					String main="ইন্টারনেট কানেকশন দরকার";
					String optional="আপনার মোবাইল ফোনের ইন্টারনেট কানেকশন অন করতে ঠিক আছে বাটনে ক্লিক করুন";
					String ok="ঠিক আছে";
					String cancel="বাতিল";
					if(supported)
					{
						SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
						dialogTitle.setText(convertedHeader);
						SpannableString convertedMain=AndroidCustomFontSupport.getCorrectedBengaliFormat(main, banglaFont, (float)1);
						txtMain.setText(convertedMain);
						SpannableString convertedOptional=AndroidCustomFontSupport.getCorrectedBengaliFormat(optional, banglaFont, (float)1);
						txtOptional.setText(convertedOptional);
						SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
						txtOk.setText(convertedOk);
						SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
						txtCancel.setText(convertedCancel);
					}
					else
					{
						dialogTitle.setText(header);
						txtMain.setText(main);
						txtOptional.setText(optional);
						txtOk.setText(ok);
						txtCancel.setText(cancel);
					}
					
					linearCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
						}
					});	
					
					linearOk.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
							Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
							startActivity(in);
						}
					});
					
					
				}
				
			}
		});
		
		
		
		
		
       linearSearchOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				linearTitle.setVisibility(View.INVISIBLE);
				linearSearch.setVisibility(View.VISIBLE);
				titleVisibility=false;
			}
		});
		
		
         imgIcon2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearSearch.setVisibility(View.INVISIBLE);
				linearTitle.setVisibility(View.VISIBLE);
				titleVisibility=true;
			}
		});
         
         
         edtSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int start, int before, int count) {
				// TODO Auto-generated method stub
				adapter.getFilter().filter(cs);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
	}
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস আপডেট করা নাই");
		}
		else
		{
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস সাপোর্ট করে না");
		}
		return false;
	}
	
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	

}
