package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Service24Activity extends Activity{
	
	//TextView[] txtTabs=new TextView[6];
	LinearLayout[] linearTabs=new LinearLayout[6];
	ListView list;
	int[] linearIds={R.id.linearHospital,R.id.linearAmbulance,R.id.linearBloodBank,R.id.linearPharmecy};
	//int[] txtIds={R.id.txtHospital,R.id.txtAmbulance,R.id.txtBloodBank,R.id.txtPharmecy};
	
	int selected;
	CustomAdapter adapter0,adapter1,adapter2,adapter3;
	
	ArrayList<String>hospitals=new ArrayList<String>();
	ArrayList<String>ambulances=new ArrayList<String>();
	ArrayList<String>bloods=new ArrayList<String>();
	ArrayList<String>pharmecies=new ArrayList<String>();
	ArrayList<String>amb_numbers=new ArrayList<String>();
	
	DbHelper dbOpenHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.service_24_layout);
		list=(ListView)findViewById(R.id.listView1);
		for(int i=0;i<linearIds.length;i++)
		{
			//txtTabs[i]=(TextView)findViewById(txtIds[i]);
			linearTabs[i]=(LinearLayout)findViewById(linearIds[i]);
		}
		
		hospitals=(ArrayList<String>)getIntent().getSerializableExtra("hospitals");
		ambulances=(ArrayList<String>)getIntent().getSerializableExtra("ambulances");
		bloods=(ArrayList<String>)getIntent().getSerializableExtra("banks");
		pharmecies=(ArrayList<String>)getIntent().getSerializableExtra("pharmecies");
		amb_numbers=(ArrayList<String>)getIntent().getSerializableExtra("amb_numbers");		
		
		adapter0=new CustomAdapter(this, hospitals,"Hospital");
		adapter1=new CustomAdapter(this, ambulances,"Ambulance");
		adapter2=new CustomAdapter(this, bloods,"Blood Bank");
		adapter3=new CustomAdapter(this, pharmecies,"Pharmecy");
		list.setAdapter(adapter0);
		selected=0;
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		
		linearTabs[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[0].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapter(Service24Activity.this,hospitals);
				list.setAdapter(adapter0);
				selected=0;
			}
		});
		
		
		linearTabs[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[1].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapter(Service24Activity.this,ambulances);
				list.setAdapter(adapter1);
				selected=1;
			}
		});
		
		
		linearTabs[2].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[2].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapter(Service24Activity.this,bloods);
				list.setAdapter(adapter2);
				selected=2;
			}
		});
		
		
		linearTabs[3].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[3].setBackgroundResource(R.drawable.selected);				
				//adapter=new CustomAdapter(Service24Activity.this,pharmecies);
				list.setAdapter(adapter3);
				selected=3;
			}
		});
		
		
		
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
					try
					{
						String number="",address="",lat="",lon="";
						if(selected==0)
						{
							//String name=(String)parent.getItemAtPosition(position);					
							String name=hospitals.get(position);
							HospitalInfoRetreiver retreiver=new HospitalInfoRetreiver(Service24Activity.this);
							retreiver.DetailActivityCaller(name);
						}
						else if(selected==1)
						{
							//String name=(String)parent.getItemAtPosition(position);
							String name=ambulances.get(position);
							ArrayList<String>columnName=new ArrayList<String>();
							columnName.add(Constants.AMBULANCE_NUMBER_NUMBER);
							columnName.add(Constants.AMBULANCE_NUMBER_ADDRESS);
							columnName.add(Constants.AMBULANCE_NUMBER_LAT);
							columnName.add(Constants.AMBULANCE_NUMBER_LON);
							HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER, columnName, Constants.AMBULANCE_NUMBER_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_NAME);
							if(data.get(Constants.AMBULANCE_NUMBER_NUMBER).size()>0) number=data.get(Constants.AMBULANCE_NUMBER_NUMBER).get(0);
							if(data.get(Constants.AMBULANCE_NUMBER_ADDRESS).size()>0) address=data.get(Constants.AMBULANCE_NUMBER_ADDRESS).get(0);
							if(data.get(Constants.AMBULANCE_NUMBER_LAT).size()>0) lat=data.get(Constants.AMBULANCE_NUMBER_LAT).get(0);
							if(data.get(Constants.AMBULANCE_NUMBER_LON).size()>0) lon=data.get(Constants.AMBULANCE_NUMBER_LON).get(0);
							
							//showCallDialog(number);
							Intent in=new Intent(Service24Activity.this,DetailsPharmecyActivity.class);
							in.putExtra("title","Ambulance");
							in.putExtra("name", name);
							in.putExtra("number", number);
							in.putExtra("address", address);
							in.putExtra("lat", lat);
							in.putExtra("lon", lon);
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						}
						else if(selected==2)
						{
							//String name=(String)parent.getItemAtPosition(position);
							String name=bloods.get(position);
							ArrayList<String>columnName=new ArrayList<String>();
							columnName.add(Constants.BLOOD_BANK_NUMBER);
							columnName.add(Constants.BLOOD_BANK_ADDRESS);
							columnName.add(Constants.BLOOD_BANK_LAT);
							columnName.add(Constants.BLOOD_BANK_LON);
							HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK, columnName, Constants.BLOOD_BANK_NAME, Constants.STRING_ONLY, name, Constants.BLOOD_BANK_NAME);
							if(data.get(Constants.BLOOD_BANK_NUMBER).size()>0) number=data.get(Constants.BLOOD_BANK_NUMBER).get(0);
							if(data.get(Constants.BLOOD_BANK_ADDRESS).size()>0) address=data.get(Constants.BLOOD_BANK_ADDRESS).get(0);
							if(data.get(Constants.BLOOD_BANK_LAT).size()>0) lat=data.get(Constants.BLOOD_BANK_LAT).get(0);
							if(data.get(Constants.BLOOD_BANK_LON).size()>0) lon=data.get(Constants.BLOOD_BANK_LON).get(0);
							Intent in=new Intent(Service24Activity.this,DetailsPharmecyActivity.class);
							in.putExtra("title","Blood Bank");
							in.putExtra("name", name);
							in.putExtra("number", number);
							in.putExtra("address", address);
							in.putExtra("lat", lat);
							in.putExtra("lon", lon);
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						}
						else if(selected==3)
						{
							//String name=(String)parent.getItemAtPosition(position);
							String name=pharmecies.get(position);
							ArrayList<String>columnName=new ArrayList<String>();
							columnName.add(Constants.PHARMECY_NUMBER);
							columnName.add(Constants.PHARMECY_ADDRESS);
							columnName.add(Constants.PHARMECY_LAT);
							columnName.add(Constants.PHARMECY_LON);
							HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY, columnName, Constants.PHARMECY_NAME, Constants.STRING_ONLY, name, Constants.PHARMECY_NAME);
							if(data.get(Constants.PHARMECY_NUMBER).size()>0) number=data.get(Constants.PHARMECY_NUMBER).get(0);
							if(data.get(Constants.PHARMECY_ADDRESS).size()>0) address=data.get(Constants.PHARMECY_ADDRESS).get(0);
							if(data.get(Constants.PHARMECY_LAT).size()>0) lat=data.get(Constants.PHARMECY_LAT).get(0);
							if(data.get(Constants.PHARMECY_LON).size()>0) lon=data.get(Constants.PHARMECY_LON).get(0);
							Intent in=new Intent(Service24Activity.this,DetailsPharmecyActivity.class);
							in.putExtra("title","Pharmecy");
							in.putExtra("name", name);
							in.putExtra("number", number);
							in.putExtra("address", address);
							in.putExtra("lat", lat);
							in.putExtra("lon", lon);
							startActivity(in);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
						}
					}
					catch(Exception e)
					{
						
					}
			}
		});
		
		
		
		
	}
	
	
	
	

}
