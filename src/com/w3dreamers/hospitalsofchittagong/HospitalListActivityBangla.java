package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HospitalListActivityBangla extends Activity{
	
	TextView txtTitle,txtOption,txtFavourite;
	ImageView imgSearchOption,imgIcon2;
	LinearLayout linearSearchOption,linearTitle,linearSearch,linearOption,linearFavourite;
	ListView list;
	EditText edtSearch;
	
	ArrayList<String> names=new ArrayList<String>();
	String title,option,favourite;
	
	int selected;
	CustomAdapterBangla adapter,adapter2;
	DbHelper dbOpenHelper;
	boolean supported;
	Typeface banglaFont;
	
	boolean titleVisibility;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.hospital_list_layout2);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtOption=(TextView)findViewById(R.id.txtOption);
		txtFavourite=(TextView)findViewById(R.id.txtFavourite);
		imgSearchOption=(ImageView)findViewById(R.id.imgSearchOption);
		imgIcon2=(ImageView)findViewById(R.id.imgIcon2);
		linearSearchOption=(LinearLayout)findViewById(R.id.linearSearchOption);
		linearTitle=(LinearLayout)findViewById(R.id.linearTitle);
		linearSearch=(LinearLayout)findViewById(R.id.linearSearch);
		linearOption=(LinearLayout)findViewById(R.id.linearOption);
		linearFavourite=(LinearLayout)findViewById(R.id.linearFavourite);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		list=(ListView)findViewById(R.id.listView1);
		
		linearSearch.setVisibility(View.INVISIBLE);
		titleVisibility=true;
		
		title=getIntent().getExtras().getString("title");
		names=(ArrayList<String>)getIntent().getSerializableExtra("names");
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		edtSearch.setTypeface(banglaFont);
		
		if(title.equals("চট্টগ্রামের হাসপাতালসমূহ")) option="হাসপাতাল";
		else option=title;
		String hint="হাসপাতালের নাম টাইপ করুন";
		favourite="প্রিয় তালিকা";
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedOption=AndroidCustomFontSupport.getCorrectedBengaliFormat(option, banglaFont, (float)1);
			txtOption.setText(convertedOption);
			SpannableString convertedFavourite=AndroidCustomFontSupport.getCorrectedBengaliFormat(favourite, banglaFont, (float)1);
			txtFavourite.setText(convertedFavourite);
			SpannableString convertedHint=AndroidCustomFontSupport.getCorrectedBengaliFormat(hint, banglaFont, (float)1);
			edtSearch.setHint(convertedHint);
		}
		else
		{
			txtTitle.setText(title);
			txtOption.setText(option);
			txtFavourite.setText(favourite);
			edtSearch.setHint(hint);
		}
		
		adapter=new CustomAdapterBangla(this,names,option);		
		list.setAdapter(adapter);
		selected=0;
		list.setTextFilterEnabled(true);
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		
       linearOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearOption.setBackgroundResource(R.drawable.selected);
				linearFavourite.setBackgroundResource(R.drawable.unselected);
				list.setAdapter(adapter);
				selected=0;
				list.setTextFilterEnabled(true);
			}
		});
		
		
		linearFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setFavouriteList();
			}
		});
		
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				String number="",address="",lat="",lon="";
				try
				{
					String name=(String)parent.getItemAtPosition(pos);					
					if(title.equals("অ্যাম্বুল্যান্স"))
					{
						ArrayList<String>columnName=new ArrayList<String>();
						columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER);
						columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_ADDRESS);
						columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_LAT);
						columnName.add(Constants.AMBULANCE_NUMBER_BANGLA_LON);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columnName, Constants.AMBULANCE_NUMBER_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
						if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER).size()>0) number=data.get(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER).get(0);
						if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_ADDRESS).size()>0) address=data.get(Constants.AMBULANCE_NUMBER_BANGLA_ADDRESS).get(0);
						if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_LAT).size()>0) lat=data.get(Constants.AMBULANCE_NUMBER_BANGLA_LAT).get(0);
						if(data.get(Constants.AMBULANCE_NUMBER_BANGLA_LON).size()>0) lon=data.get(Constants.AMBULANCE_NUMBER_BANGLA_LON).get(0);
   	   					
   						//showCallDialog(number);
						Intent in=new Intent(HospitalListActivityBangla.this,DetailsPharmecyActivityBangla.class);
						in.putExtra("title","অ্যাম্বুল্যান্স");
						in.putExtra("name", name);
						in.putExtra("number", number);
						in.putExtra("address", address);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);					
					}
					else if(title.equals("ব্লাড ব্যাংক"))
					{
						ArrayList<String>columnName=new ArrayList<String>();
						columnName.add(Constants.BLOOD_BANK_BANGLA_NUMBER);
						columnName.add(Constants.BLOOD_BANK_BANGLA_ADDRESS);
						columnName.add(Constants.BLOOD_BANK_BANGLA_LAT);
						columnName.add(Constants.BLOOD_BANK_BANGLA_LON);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK_BANGLA, columnName, Constants.BLOOD_BANK_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.BLOOD_BANK_BANGLA_NAME);
						if(data.get(Constants.BLOOD_BANK_BANGLA_NUMBER).size()>0) number=data.get(Constants.BLOOD_BANK_BANGLA_NUMBER).get(0);
						if(data.get(Constants.BLOOD_BANK_BANGLA_ADDRESS).size()>0) address=data.get(Constants.BLOOD_BANK_BANGLA_ADDRESS).get(0);
						if(data.get(Constants.BLOOD_BANK_BANGLA_LAT).size()>0) lat=data.get(Constants.BLOOD_BANK_BANGLA_LAT).get(0);
						if(data.get(Constants.BLOOD_BANK_BANGLA_LON).size()>0) lon=data.get(Constants.BLOOD_BANK_BANGLA_LON).get(0);
						Intent in=new Intent(HospitalListActivityBangla.this,DetailsPharmecyActivityBangla.class);
						in.putExtra("title","ব্লাড ব্যাংক");
						in.putExtra("name", name);
						in.putExtra("number", number);
						in.putExtra("address", address);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
					else if(title.equals("ফার্মেসি"))
					{
						ArrayList<String>columnName=new ArrayList<String>();
						columnName.add(Constants.PHARMECY_BANGLA_NUMBER);
						columnName.add(Constants.PHARMECY_BANGLA_ADDRESS);
						columnName.add(Constants.PHARMECY_BANGLA_LAT);
						columnName.add(Constants.PHARMECY_BANGLA_LON);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY_BANGLA, columnName, Constants.PHARMECY_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.PHARMECY_BANGLA_NAME);
						if(data.get(Constants.PHARMECY_BANGLA_NUMBER).size()>0) number=data.get(Constants.PHARMECY_BANGLA_NUMBER).get(0);
						if(data.get(Constants.PHARMECY_BANGLA_ADDRESS).size()>0) address=data.get(Constants.PHARMECY_BANGLA_ADDRESS).get(0);
						if(data.get(Constants.PHARMECY_BANGLA_LAT).size()>0) lat=data.get(Constants.PHARMECY_BANGLA_LAT).get(0);
						if(data.get(Constants.PHARMECY_BANGLA_LON).size()>0) lon=data.get(Constants.PHARMECY_BANGLA_LON).get(0);
						Intent in=new Intent(HospitalListActivityBangla.this,DetailsPharmecyActivityBangla.class);
						in.putExtra("title","ফার্মেসি");
						in.putExtra("name", name);
						in.putExtra("number", number);
						in.putExtra("address", address);
						in.putExtra("lat", lat);
						in.putExtra("lon", lon);
						startActivity(in);
						overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					}
					else
					{
						HospitalInfoRetreiverBangla retreiver=new HospitalInfoRetreiverBangla(HospitalListActivityBangla.this);
						retreiver.DetailActivityCaller(name);
					}

				}
				catch(Exception e) {}
				
			}
		});
		
		
            linearSearchOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				linearTitle.setVisibility(View.INVISIBLE);
				linearSearch.setVisibility(View.VISIBLE);
				titleVisibility=false;
			}
		});
		
		
         imgIcon2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearSearch.setVisibility(View.INVISIBLE);
				linearTitle.setVisibility(View.VISIBLE);
				titleVisibility=true;
			}
		});
         
         
         edtSearch.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int start, int before, int count) {
				// TODO Auto-generated method stub
				adapter.getFilter().filter(cs);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
	}
	
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	
	
	public void setFavouriteList()
	{
		
		try
		{
			ArrayList<String>favourites=new ArrayList<String>();
			ArrayList<String>columnName=new ArrayList<String>();
			String mColumn;
			if(title.equals("অ্যাম্বুল্যান্স")) mColumn=Constants.FAVOURITE_BANGLA_AMBULANCE;
			else if(title.equals("ব্লাড ব্যাংক")) mColumn=Constants.FAVOURITE_BANGLA_BLOOD_BANK;
			else if(title.equals("ফার্মেসি")) mColumn=Constants.FAVOURITE_BANGLA_PHARMECY;
			else mColumn=Constants.FAVOURITE_BANGLA_HOSPITAL;
			columnName.add(mColumn);
			HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, columnName,mColumn);
			favourites=data.get(mColumn);
			data.clear();
			
			ArrayList<String>rFavourites=new ArrayList<String>();
			for(int i=0;i<favourites.size();i++)
			{
				if(favourites.get(i)!=null) rFavourites.add(favourites.get(i));
			}
			
			if(rFavourites.size()<=0)
			{
				String text="প্রিয় তালিকাতে কোন ";
				if(title.equals("অ্যাম্বুল্যান্স")||title.equals("ব্লাড ব্যাংক")||title.equals("ফার্মেসি")) text+=title+" ";
				else text+="হাস্পাতাল ";
				text+="যোগ করা হয় নি";
				showToast(text);
			}
			else
			{
				linearOption.setBackgroundResource(R.drawable.unselected);
				linearFavourite.setBackgroundResource(R.drawable.selected);
				adapter2=new CustomAdapterBangla(HospitalListActivityBangla.this,rFavourites,option);
				list.setAdapter(adapter2);
				selected=1;
				list.setTextFilterEnabled(true);
			}
		}
		catch(Exception e)
		{
			
		}
		
	}
	
	
	
	

}
