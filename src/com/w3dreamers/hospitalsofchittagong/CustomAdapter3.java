package com.w3dreamers.hospitalsofchittagong;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter3 extends BaseAdapter {
	
	//Typeface banglaFont;
	//SharedPreferences prefBangla;
	//boolean supported;

	
	LayoutInflater inFlater;
	
	Context context;
	List<String>itemList;
	String option;
	
	public CustomAdapter3(Context context,List<String>itemList,String option)
	{
		this.context=context;
		this.itemList=itemList;
		this.option=option;
		
		//banglaFont = Typeface.createFromAsset(context.getAssets(),"font/solaimanlipinormal.ttf");
		//prefBangla=context.getSharedPreferences("BanglaFont",0);
		//supported=prefBangla.getBoolean("supported", false);
		
		inFlater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return itemList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		  
		
		ViewHolder holder=null;
		if(convertView==null)
		{
			convertView=inFlater.inflate(R.layout.item3, null);
		  
		    holder= new ViewHolder();
		    holder.itemName=(TextView)convertView.findViewById(R.id.txtItemName);
		    holder.txtClick=(TextView)convertView.findViewById(R.id.txtClick);
		    holder.imgItem=(ImageView)convertView.findViewById(R.id.imgItem);
	//	    holder.linear1=(LinearLayout)convertView.findViewById(R.id.linearList1);
	//		holder.linear2=(LinearLayout)convertView.findViewById(R.id.linearList2);
		    convertView.setTag(holder);
		}
		else
		{   
			    holder=(ViewHolder)convertView.getTag();
		}
		
/*		int pos=position%4;
		if(pos==0)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#5BB05F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_green);
		}
		else if(pos==1)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#DF483F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_red);
		}
		else if(pos==2)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#FAA741"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_yellow);
		}
		else
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#1E87C8"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_blue);
		}  */
		
		String text=itemList.get(position);
		String click="";
		
		if(option.equals("Emails"))
		{
			holder.imgItem.setImageResource(R.drawable.ic_action_mail);
			click="Click to send mail";
		}
		else if(option.equals("Emergency Numbers")||option.equals("Ambulance Contacts"))
		{
			holder.imgItem.setImageResource(R.drawable.mobile);
			click="Click to call";
		}
		
		//holder.itemName.setTypeface(banglaFont);
	/*	if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			holder.menuName.setText(convertedText);
		}
		else holder.menuName.setText(text);  */
		holder.itemName.setText(text);
		holder.txtClick.setText(click);
		
		return convertView;
	}

	
	public static class ViewHolder {
	    public TextView itemName,txtClick;
	    ImageView imgItem;
	 //   public LinearLayout linear1,linear2;
	}

	
	

}
