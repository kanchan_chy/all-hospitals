package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.SpannableString;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OptionActivityBangla extends Activity implements LocationListener,GooglePlayServicesClient.ConnectionCallbacks,GooglePlayServicesClient.OnConnectionFailedListener{
	
	LinearLayout linearEmergency,linearAmbulance,linear24_7,linearAll,linearCategory,linearBlood,linearLanguage,linearPharmecy,linearNearest,linearLocation;
	TextView txtEmergency,txtAmbulance,txt24_7,txtAll,txtCategory,txtBlood,txtLanguage,txtPharmecy,txtNearest,txtLocation;
	ImageView imgLanguage;
	
	boolean supported;
	Typeface banglaFont;
	
	LocationClient locationClient;
	int count;
	
	DbHelper dbOpenHelper;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);	
		setContentView(R.layout.option_layout);
		
		linearEmergency=(LinearLayout)findViewById(R.id.linearEmergency);
		linearAmbulance=(LinearLayout)findViewById(R.id.linearAmbulance);
		linear24_7=(LinearLayout)findViewById(R.id.linear24_7);
		linearAll=(LinearLayout)findViewById(R.id.linearAll);
		linearCategory=(LinearLayout)findViewById(R.id.linearCategory);
		linearBlood=(LinearLayout)findViewById(R.id.linearBloodBank);
		linearLanguage=(LinearLayout)findViewById(R.id.linearLanguage);
		linearPharmecy=(LinearLayout)findViewById(R.id.linearPharmecy);
		linearNearest=(LinearLayout)findViewById(R.id.linearNearest);
		linearLocation=(LinearLayout)findViewById(R.id.linearLocation);
		
		txtEmergency=(TextView)findViewById(R.id.txtEmergency);
		txtAmbulance=(TextView)findViewById(R.id.txtAmbulance);
		txt24_7=(TextView)findViewById(R.id.txt24_7);
		txtAll=(TextView)findViewById(R.id.txtAll);
		txtCategory=(TextView)findViewById(R.id.txtCategory);
		txtBlood=(TextView)findViewById(R.id.txtBloodBank);
		txtLanguage=(TextView)findViewById(R.id.txtLanguage);
		txtPharmecy=(TextView)findViewById(R.id.txtPharmecy);
		txtNearest=(TextView)findViewById(R.id.txtNearest);
		txtLocation=(TextView)findViewById(R.id.txtLocation);
		
		imgLanguage=(ImageView)findViewById(R.id.imgLanguage);
		
		imgLanguage.setImageResource(R.drawable.ic_english_button);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtEmergency.setTypeface(banglaFont);
		txtAmbulance.setTypeface(banglaFont);
		txt24_7.setTypeface(banglaFont);
		txtAll.setTypeface(banglaFont);
		txtCategory.setTypeface(banglaFont);
		txtBlood.setTypeface(banglaFont);
		txtLanguage.setTypeface(banglaFont);
		txtPharmecy.setTypeface(banglaFont);
		txtNearest.setTypeface(banglaFont);
		txtLocation.setTypeface(banglaFont);
		
		String emergency="জরুরী যোগাযোগ";
		String ambulance="অ্যাম্বুল্যান্স";
		String service24="২৪/৭";
		String all="সব হাসপাতাল";
		String category="হাসপাতাল ক্যাটেগরি";
		String blood="ব্লাড ব্যাংক";
		String language="ইংরেজি";
		String pharmecy="ফার্মেসি";
		String nearest="নিকটবর্তী সেবাসমূহ";
		String location="হাসপাতালসমূহের অবস্থান";
		
		if(supported)
		{
			SpannableString convertedEmergency=AndroidCustomFontSupport.getCorrectedBengaliFormat(emergency, banglaFont, (float)1);
			SpannableString convertedAmbulance=AndroidCustomFontSupport.getCorrectedBengaliFormat(ambulance, banglaFont, (float)1);
			SpannableString convertedService24=AndroidCustomFontSupport.getCorrectedBengaliFormat(service24, banglaFont, (float)1);
			SpannableString convertedAll=AndroidCustomFontSupport.getCorrectedBengaliFormat(all, banglaFont, (float)1);
			SpannableString convertedCategory=AndroidCustomFontSupport.getCorrectedBengaliFormat(category, banglaFont, (float)1);
			SpannableString convertedBlood=AndroidCustomFontSupport.getCorrectedBengaliFormat(blood, banglaFont, (float)1);
			SpannableString convertedLanguage=AndroidCustomFontSupport.getCorrectedBengaliFormat(language, banglaFont, (float)1);
			SpannableString convertedPharmecy=AndroidCustomFontSupport.getCorrectedBengaliFormat(pharmecy, banglaFont, (float)1);
			SpannableString convertedNearest=AndroidCustomFontSupport.getCorrectedBengaliFormat(nearest, banglaFont, (float)1);
			SpannableString convertedLocation=AndroidCustomFontSupport.getCorrectedBengaliFormat(location, banglaFont, (float)1);
			
			txtEmergency.setText(convertedEmergency);
			txtAmbulance.setText(convertedAmbulance);
			txt24_7.setText(convertedService24);
			txtAll.setText(convertedAll);
			txtCategory.setText(convertedCategory);
			txtBlood.setText(convertedBlood);
			txtLanguage.setText(convertedLanguage);
			txtPharmecy.setText(convertedPharmecy);
			txtNearest.setText(convertedNearest);
			txtLocation.setText(convertedLocation);
		}
		else
		{
			txtEmergency.setText(emergency);
			txtAmbulance.setText(ambulance);
			txt24_7.setText(service24);
			txtAll.setText(all);
			txtCategory.setText(category);
			txtBlood.setText(blood);
			txtLanguage.setText(language);
			txtPharmecy.setText(pharmecy);
			txtNearest.setText(nearest);
			txtLocation.setText(location);
		}
		
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		
		
		linearEmergency.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String> numbers=new ArrayList<String>();
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRow(Constants.TABLE_EMERGENCY_BANGLA, Constants.EMERGENCY_BANGLA_NAME);
					names=data.get(Constants.EMERGENCY_BANGLA_NAME);
					numbers=data.get(Constants.EMERGENCY_BANGLA_NUMBER);
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,NumberListActivityBangla.class);
					in.putExtra("names", names);
					in.putExtra("title", "জরুরী যোগাযোগ");
					in.putExtra("allContacts",numbers);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearAmbulance.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columns, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					names=data.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,HospitalListActivityBangla.class);
					in.putExtra("title", "অ্যাম্বুল্যান্স");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linear24_7.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String>hospitals=new ArrayList<String>();
					ArrayList<String>ambulances=new ArrayList<String>();
					ArrayList<String>banks=new ArrayList<String>();
					ArrayList<String>pharmecies=new ArrayList<String>();
					
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_SERVICE_24, Constants.STRING_ONLY, "অ্যাভেইলেবল", Constants.INFO_BANGLA_NAME);
					hospitals=data.get(Constants.INFO_BANGLA_NAME);
					
					ArrayList<String>columns1=new ArrayList<String>();
					columns1.add(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columns1, Constants.AMBULANCE_NUMBER_BANGLA_SERVICE_24, Constants.STRING_ONLY, "অ্যাভেইলেবল", Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					ambulances=data1.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
					
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.BLOOD_BANK_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK_BANGLA, columns2, Constants.BLOOD_BANK_BANGLA_SERVICE_24, Constants.STRING_ONLY, "অ্যাভেইলেবল", Constants.BLOOD_BANK_BANGLA_NAME);
					banks=data2.get(Constants.BLOOD_BANK_BANGLA_NAME);
					
					ArrayList<String>columns3=new ArrayList<String>();
					columns3.add(Constants.PHARMECY_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data3	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY_BANGLA, columns3, Constants.PHARMECY_BANGLA_SERVICE_24, Constants.STRING_ONLY, "অ্যাভেইলেবল", Constants.PHARMECY_BANGLA_NAME);
					pharmecies=data3.get(Constants.PHARMECY_BANGLA_NAME);
					
					Intent in=new Intent(OptionActivityBangla.this,Service24ActivityBangla.class);
					in.putExtra("hospitals", hospitals);
					in.putExtra("ambulances", ambulances);
					in.putExtra("banks", banks);
					in.putExtra("pharmecies", pharmecies);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_NAME);
					names=data.get(Constants.INFO_BANGLA_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,HospitalListActivityBangla.class);
					in.putExtra("title", "চট্টগ্রামের হাসপাতালসমূহ");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearCategory.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> categories=new ArrayList<String>();
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRow(Constants.TABLE_ALL_CATEGORY_BANGLA, Constants.ALL_CATEGORY_BANGLA_CATEGORY_NAME);
					categories=data.get(Constants.ALL_CATEGORY_BANGLA_CATEGORY_NAME);	
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,CategoriesActivityBangla.class);
					in.putExtra("categories", categories);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearBlood.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.BLOOD_BANK_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_BLOOD_BANK_BANGLA, columns, Constants.BLOOD_BANK_BANGLA_NAME);
					names=data.get(Constants.BLOOD_BANK_BANGLA_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,HospitalListActivityBangla.class);
					in.putExtra("title", "ব্লাড ব্যাংক");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		linearLanguage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				SharedPreferences prefLang=getSharedPreferences("Language", MODE_PRIVATE);
				SharedPreferences.Editor editorLang=prefLang.edit();
				editorLang.putString("language", "English");
				editorLang.commit();
				
				Intent intent=new Intent(OptionActivityBangla.this,OptionActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				finish();
				
			}
		});
		
		linearPharmecy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.PHARMECY_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_PHARMECY_BANGLA, columns, Constants.PHARMECY_BANGLA_NAME);
					names=data.get(Constants.PHARMECY_BANGLA_NAME);
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,HospitalListActivityBangla.class);
					in.putExtra("title", "ফার্মেসি");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		
		linearNearest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				callNearest();
			}
		});
		
		linearLocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> ids=new ArrayList<String>();
					ArrayList<String> hospitals=new ArrayList<String>();
					ArrayList<String> addresses=new ArrayList<String>();
					ArrayList<String> directions=new ArrayList<String>();
					ArrayList<String> lats=new ArrayList<String>();
					ArrayList<String> lons=new ArrayList<String>();
					
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_BANGLA_ID);
					columns.add(Constants.INFO_BANGLA_NAME);
					columns.add(Constants.INFO_BANGLA_ADDRESS);
					columns.add(Constants.INFO_BANGLA_DIRECTION);
					columns.add(Constants.INFO_BANGLA_LATITUDE);
					columns.add(Constants.INFO_BANGLA_LONGITUDE);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_NAME);
					ids=data.get(Constants.INFO_BANGLA_ID);
					hospitals=data.get(Constants.INFO_BANGLA_NAME);
					addresses=data.get(Constants.INFO_BANGLA_ADDRESS);
					directions=data.get(Constants.INFO_BANGLA_DIRECTION);
					lats=data.get(Constants.INFO_BANGLA_LATITUDE);
					lons=data.get(Constants.INFO_BANGLA_LONGITUDE);
					data.clear();
					
					Intent in=new Intent(OptionActivityBangla.this,AddressActivityBangla.class);
					in.putExtra("ids", ids);
					in.putExtra("hospitals", hospitals);
					in.putExtra("addresses", addresses);
					in.putExtra("directions", directions);
					in.putExtra("lats", lats);
					in.putExtra("lons", lons);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
		
		
		
		
	}
	
	

	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস আপডেট করা নাই");
		}
		else
		{
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস সাপোর্ট করে না");
		}
		return false;
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(OptionActivityBangla.this,ExitActivityBangla.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		finish();
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}




	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		count++;
		try
		{
			Location location=locationClient.getLastLocation();
			double mLat=location.getLatitude();
			double mLon=location.getLongitude();
			
			ArrayList<String>hospitals=new ArrayList<String>();
			ArrayList<String>ambulances=new ArrayList<String>();
			ArrayList<String>bloods=new ArrayList<String>();
			ArrayList<String>pharmecies=new ArrayList<String>();
			ArrayList<String>hos_lats=new ArrayList<String>();
			ArrayList<String>hos_lons=new ArrayList<String>();
			ArrayList<String>amb_lats=new ArrayList<String>();
			ArrayList<String>amb_lons=new ArrayList<String>();
			ArrayList<String>blood_lats=new ArrayList<String>();
			ArrayList<String>blood_lons=new ArrayList<String>();
			ArrayList<String>phar_lats=new ArrayList<String>();
			ArrayList<String>phar_lons=new ArrayList<String>();
			
			ArrayList<String>columns=new ArrayList<String>();
			columns.add(Constants.INFO_BANGLA_NAME);
			columns.add(Constants.INFO_BANGLA_LATITUDE);
			columns.add(Constants.INFO_BANGLA_LONGITUDE);
			HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_NAME);
			hospitals=data.get(Constants.INFO_BANGLA_NAME);
			hos_lats=data.get(Constants.INFO_BANGLA_LATITUDE);
			hos_lons=data.get(Constants.INFO_BANGLA_LONGITUDE);
			
			ArrayList<String>columns1=new ArrayList<String>();
			columns1.add(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
			columns1.add(Constants.AMBULANCE_NUMBER_BANGLA_LAT);
			columns1.add(Constants.AMBULANCE_NUMBER_BANGLA_LON);
			HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columns1, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
			ambulances=data1.get(Constants.AMBULANCE_NUMBER_BANGLA_NAME);
			amb_lats=data1.get(Constants.AMBULANCE_NUMBER_BANGLA_LAT);
			amb_lons=data1.get(Constants.AMBULANCE_NUMBER_BANGLA_LON);
			
			ArrayList<String>columns2=new ArrayList<String>();
			columns2.add(Constants.BLOOD_BANK_BANGLA_NAME);
			columns2.add(Constants.BLOOD_BANK_BANGLA_LAT);
			columns2.add(Constants.BLOOD_BANK_BANGLA_LON);
			HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_BLOOD_BANK_BANGLA, columns2, Constants.BLOOD_BANK_BANGLA_NAME);
			bloods=data2.get(Constants.BLOOD_BANK_BANGLA_NAME);
			blood_lats=data2.get(Constants.BLOOD_BANK_BANGLA_LAT);
			blood_lons=data2.get(Constants.BLOOD_BANK_BANGLA_LON);
			
			ArrayList<String>columns3=new ArrayList<String>();
			columns3.add(Constants.PHARMECY_BANGLA_NAME);
			columns3.add(Constants.PHARMECY_BANGLA_LAT);
			columns3.add(Constants.PHARMECY_BANGLA_LON);
			HashMap<String,ArrayList<String>> data3	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_PHARMECY_BANGLA, columns3, Constants.PHARMECY_BANGLA_NAME);
			pharmecies=data3.get(Constants.PHARMECY_BANGLA_NAME);
			pharmecies=data3.get(Constants.PHARMECY_BANGLA_LAT);
			pharmecies=data3.get(Constants.PHARMECY_BANGLA_LON);
			
			Intent in=new Intent(OptionActivityBangla.this,NearestServiceActivityBangla.class);
			in.putExtra("hospitals", hospitals);
			in.putExtra("ambulances", ambulances);
			in.putExtra("bloods", bloods);
			in.putExtra("pharmecies", pharmecies);
			in.putExtra("hos_lats", hos_lats);
			in.putExtra("hos_lons", hos_lons);
			in.putExtra("amb_lats", amb_lats);
			in.putExtra("amb_lons", amb_lons);
			in.putExtra("blood_lats", blood_lats);
			in.putExtra("blood_lons", blood_lons);
			in.putExtra("phar_lats", phar_lats);
			in.putExtra("phar_lons", phar_lons);
			in.putExtra("lat", mLat);
			in.putExtra("lon", mLon);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			
		}
		catch(Exception e)
		{
			if(count<10)
			{
				locationClient.disconnect();
				locationClient.connect();
			}
			else
			{
				showToast("আপনার লোকেশন পাওয়া যায় নি");
			}
		}
	}




	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}	
	
	
	
	
	
	public void callNearest()
	{
		WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
		boolean isWifiEnabled=wifi.isWifiEnabled();
		if(isConnectingInternet()||isWifiEnabled)
		{
			if(isMapAvailable())
			{
				LocationManager locManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
				boolean gps=locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				boolean network=locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
				if(gps||network)
				{
					showToast("আপনার লোকেশন সার্চ করা হচ্ছে");
					locationClient=new LocationClient(this, this, this);
					count=0;
					locationClient.connect();
				}
				else
				{
					
					LayoutInflater li = LayoutInflater.from(OptionActivityBangla.this);
					View promptsView = li.inflate(R.layout.internet_dialog, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivityBangla.this);
					builder.setView(promptsView);
					builder.setCancelable(true);
					final AlertDialog alertDialog = builder.create();
					alertDialog.show();
					TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
					TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
					TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
					TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
					TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
					
					LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
					LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
					
					dialogTitle.setTypeface(banglaFont);
					txtMain.setTypeface(banglaFont);
					txtOptional.setTypeface(banglaFont);
					txtOk.setTypeface(banglaFont);
					txtCancel.setTypeface(banglaFont);
					
					String header="সতর্কবার্তা";
					String main="লোকেশন সার্ভিস দরকার";
					String optional="আপনার মোবাইল ফোনের লোকেশন সার্ভিসঃ জিপিএস এবং মোবাইল নেটওয়ার্ক উভয়ই অফ করা আছে। অপশনগুলো অন করতে ঠিক আছে বাটনে ক্লিক করুন। ";
					String ok="ঠিক আছে";
					String cancel="বাতিল";
					if(supported)
					{
						SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
						dialogTitle.setText(convertedHeader);
						SpannableString convertedMain=AndroidCustomFontSupport.getCorrectedBengaliFormat(main, banglaFont, (float)1);
						txtMain.setText(convertedMain);
						SpannableString convertedOptional=AndroidCustomFontSupport.getCorrectedBengaliFormat(optional, banglaFont, (float)1);
						txtOptional.setText(convertedOptional);
						SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
						txtOk.setText(convertedOk);
						SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
						txtCancel.setText(convertedCancel);
					}
					else
					{
						dialogTitle.setText(header);
						txtMain.setText(main);
						txtOptional.setText(optional);
						txtOk.setText(ok);
						txtCancel.setText(cancel);
					}
					
					linearCancel.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
						}
					});	
					
					linearOk.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub	    
							alertDialog.cancel();
							Intent in=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(in);
						}
					});
					
				}
			}
		}
		else
		{
			
			LayoutInflater li = LayoutInflater.from(OptionActivityBangla.this);
			View promptsView = li.inflate(R.layout.internet_dialog, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivityBangla.this);
			builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
			TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
			TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
			TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
			
			LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
			LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
			
			dialogTitle.setTypeface(banglaFont);
			txtMain.setTypeface(banglaFont);
			txtOptional.setTypeface(banglaFont);
			txtOk.setTypeface(banglaFont);
			txtCancel.setTypeface(banglaFont);
			
			String header="সতর্কবার্তা";
			String main="ইন্টারনেট কানেকশন দরকার";
			String optional="আপনার মোবাইল ফোনের ইন্টারনেট কানেকশন অন করতে ঠিক আছে বাটনে ক্লিক করুন";
			String ok="ঠিক আছে";
			String cancel="বাতিল";
			if(supported)
			{
				SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
				dialogTitle.setText(convertedHeader);
				SpannableString convertedMain=AndroidCustomFontSupport.getCorrectedBengaliFormat(main, banglaFont, (float)1);
				txtMain.setText(convertedMain);
				SpannableString convertedOptional=AndroidCustomFontSupport.getCorrectedBengaliFormat(optional, banglaFont, (float)1);
				txtOptional.setText(convertedOptional);
				SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
				txtOk.setText(convertedOk);
				SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
				txtCancel.setText(convertedCancel);
			}
			else
			{
				dialogTitle.setText(header);
				txtMain.setText(main);
				txtOptional.setText(optional);
				txtOk.setText(ok);
				txtCancel.setText(cancel);
			}
			
			linearCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
				}
			});	
			
			linearOk.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
			
		}
	}
	
	
	
	

}
