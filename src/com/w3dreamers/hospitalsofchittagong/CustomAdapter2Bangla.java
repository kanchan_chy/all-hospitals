package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.List;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter2Bangla extends BaseAdapter {
	
	//Typeface banglaFont;
	//SharedPreferences prefBangla;
	//boolean supported;

	
	LayoutInflater inFlater;
	
	Activity context;
	List<String>itemList;
	List<String>itemBodies;
	ArrayList<String>itemClicks;
	
	boolean supported;
	Typeface banglaFont;
	
	
	public CustomAdapter2Bangla(Activity context,List<String>itemList,List<String>itemBodies, ArrayList<String>itemClicks)
	{
		this.context=context;
		this.itemList=itemList;
		this.itemBodies=itemBodies;
		this.itemClicks=itemClicks;
		
		//banglaFont = Typeface.createFromAsset(context.getAssets(),"font/solaimanlipinormal.ttf");
		//prefBangla=context.getSharedPreferences("BanglaFont",0);
		//supported=prefBangla.getBoolean("supported", false);
		
		SharedPreferences prefSupport=context.getSharedPreferences("BanglaLibrary", 0);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(context.getAssets(), "font/solaimanlipinormal.ttf");
		
		inFlater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return itemList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		  
		
		ViewHolder holder=null;
		if(convertView==null)
		{
			convertView=inFlater.inflate(R.layout.item2, null);
		  
		    holder= new ViewHolder();
		   holder.txtItemTitle=(TextView)convertView.findViewById(R.id.txtItemTitle);
		   holder.txtItemBody=(TextView)convertView.findViewById(R.id.txtItemBody);
		   holder.txtClick=(TextView)convertView.findViewById(R.id.txtClick);
	//	   holder.linear1=(LinearLayout)convertView.findViewById(R.id.linearList1);
	//	   holder.linear2=(LinearLayout)convertView.findViewById(R.id.linearList2);
		    convertView.setTag(holder);
		}
		else
		{   
			    holder=(ViewHolder)convertView.getTag();
		}
		
	/*	int pos=position%4;
		if(pos==0)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#5BB05F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_green);
		}
		else if(pos==1)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#DF483F"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_red);
		}
		else if(pos==2)
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#FAA741"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_yellow);
		}
		else
		{
			holder.linear1.setBackgroundColor(Color.parseColor("#1E87C8"));
			holder.linear2.setBackgroundResource(R.drawable.option_selector_blue);
		}   */
		
		//holder.itemName.setTypeface(banglaFont);
		String title=itemList.get(position);
		String body=itemBodies.get(position);
		String click=itemClicks.get(position);
		
		holder.txtItemTitle.setTypeface(banglaFont);
		holder.txtItemBody.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title,banglaFont, (float) 1);
			holder.txtItemTitle.setText(convertedTitle);
			SpannableString convertedBody=AndroidCustomFontSupport.getCorrectedBengaliFormat(body,banglaFont, (float) 1);
			holder.txtItemBody.setText(convertedBody);
		}
		else
		{
			holder.txtItemTitle.setText(title);
			holder.txtItemBody.setText(body);
		}
		
		if(click.equals(""))
		{
			holder.txtClick.setText(click);
		}
		else
		{
			holder.txtClick.setTypeface(banglaFont);
			if(supported)
			{
				SpannableString convertedClick=AndroidCustomFontSupport.getCorrectedBengaliFormat(click,banglaFont, (float) 1);
				holder.txtClick.setText(convertedClick);
			}
			else
			{
				holder.txtClick.setText(click);
			}
		}
		
		return convertView;
	}

	
	public static class ViewHolder {
	    public TextView txtItemTitle,txtItemBody, txtClick;
	//    public LinearLayout linear1,linear2;
	    
	}

	
	

}
