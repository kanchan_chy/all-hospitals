package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;

public class DetailsActivityBangla extends FragmentActivity{
	
	TextView txtTitle,txtAddrTitle,txtAddr,txtDirTitle,txtDir,txtDeptTitle,txtDept,txtEmergTitle,txtEmerg,txtServTitle,txt24_7,txtAmbulance,txtBlood,txtConTitle,txtWeb,txtMail,txtClickAddr,txtClickDir,txtClickEmerg,txtClickWeb,txtClickMail;
	ImageView img24_7,imgAmbulance,imgBlood;
	LinearLayout linearFavourite;
	
	//String id;
	String name,address,direction,website,service24,lat,lon,bloodBank,ambulance,email,emergency;
	ArrayList<String>depts=new ArrayList<String>();
	
	String title,addrTitle,dirTitle,deptTitle,emergTitle,servTitle,serv24,servAmb,servBlood,conTitle,webTitle,mailTitle,clickAddr,clickDir,clickEmerg,clickWeb,clickMail;
	boolean isExists;
	
	DbHelper dbOpenHelper;
	
	boolean supported;
	Typeface banglaFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.details_layout);
		linearFavourite=(LinearLayout)findViewById(R.id.linearFavourite);
		img24_7=(ImageView)findViewById(R.id.img24_7);
		imgAmbulance=(ImageView)findViewById(R.id.imgAmbulance);
		imgBlood=(ImageView)findViewById(R.id.imgBlood);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtAddrTitle=(TextView)findViewById(R.id.txtAddressTitle);
		txtAddr=(TextView)findViewById(R.id.txtAddress);
		txtClickAddr=(TextView)findViewById(R.id.txtClickAddress);
		txtDirTitle=(TextView)findViewById(R.id.txtDirectionTitle);
		txtDir=(TextView)findViewById(R.id.txtDirection);
		txtClickDir=(TextView)findViewById(R.id.txtClickDirection);
		txtDeptTitle=(TextView)findViewById(R.id.txtDeptTitle);
		txtDept=(TextView)findViewById(R.id.txtDept);
		txtEmergTitle=(TextView)findViewById(R.id.txtEmergencyTitle);
		txtEmerg=(TextView)findViewById(R.id.txtEmergency);
		txtClickEmerg=(TextView)findViewById(R.id.txtClickEmergency);
		txtServTitle=(TextView)findViewById(R.id.txtServiceTitle);
		txt24_7=(TextView)findViewById(R.id.txt24_7);
		txtAmbulance=(TextView)findViewById(R.id.txtAmbulance);
		txtBlood=(TextView)findViewById(R.id.txtBlood);
		txtConTitle=(TextView)findViewById(R.id.txtContactTitle);
		txtWeb=(TextView)findViewById(R.id.txtWebsite);
		txtClickWeb=(TextView)findViewById(R.id.txtClickWebsite);
		txtMail=(TextView)findViewById(R.id.txtEmail);
		txtClickMail=(TextView)findViewById(R.id.txtClickEmail);
		
		//id=getIntent().getExtras().getString("id");
		name=getIntent().getExtras().getString("name");
		address=getIntent().getExtras().getString("address");
		direction=getIntent().getExtras().getString("direction");
		website=getIntent().getExtras().getString("website");
		service24=getIntent().getExtras().getString("service24");
		lat=getIntent().getExtras().getString("lat");
		lon=getIntent().getExtras().getString("lon");
		bloodBank=getIntent().getExtras().getString("bloodBank");
		ambulance=getIntent().getExtras().getString("ambulance");
		email=getIntent().getExtras().getString("email");
		emergency=getIntent().getExtras().getString("emergency");
		depts=(ArrayList<String>)getIntent().getSerializableExtra("depts");
		
		title=name;
		
		addrTitle="ঠিকানা";
		dirTitle="যাওয়ার পথ";
		deptTitle="ডিপার্টমেন্ট";
		emergTitle="জরুরী যোগাযোগ";
		servTitle="অন্যান্য সেবা";
		conTitle="যোগাযোগ";
		webTitle="ওয়েবসাইট";
		mailTitle="ইমেইল";
		serv24="২৪/৭";
		servAmb="অ্যাম্বুল্যান্স";
		servBlood="ব্লাড ব্যাংক";
		clickAddr="ম্যাপ দেখতে ক্লিক করুন";
		clickDir="ম্যাপ দেখতে ক্লিক করুন";
		clickEmerg="কল করতে ক্লিক করুন";
		clickWeb="ওয়েবসাইট দেখতে ক্লিক করুন";
		clickMail="মেইল পাঠাতে ক্লিক করুন";
		
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		txtAddr.setTypeface(banglaFont);
		txtAddrTitle.setTypeface(banglaFont);
		txtClickAddr.setTypeface(banglaFont);
		txtDir.setTypeface(banglaFont);
		txtDirTitle.setTypeface(banglaFont);
		txtClickDir.setTypeface(banglaFont);
		txtDeptTitle.setTypeface(banglaFont);
		txtDept.setTypeface(banglaFont);
		txtEmergTitle.setTypeface(banglaFont);
		txtEmerg.setTypeface(banglaFont);
		txtClickEmerg.setTypeface(banglaFont);
		txtServTitle.setTypeface(banglaFont);
		txt24_7.setTypeface(banglaFont);
		txtAmbulance.setTypeface(banglaFont);
		txtBlood.setTypeface(banglaFont);
		txtConTitle.setTypeface(banglaFont);
		txtWeb.setTypeface(banglaFont);
		txtMail.setTypeface(banglaFont);
		txtClickWeb.setTypeface(banglaFont);
		txtClickMail.setTypeface(banglaFont);
		
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedAddrTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(addrTitle, banglaFont, (float)1);
			txtAddrTitle.setText(convertedAddrTitle);
			SpannableString convertedDirTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(dirTitle, banglaFont, (float)1);
			txtDirTitle.setText(convertedDirTitle);
			SpannableString convertedDeptTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(deptTitle, banglaFont, (float)1);
			txtDeptTitle.setText(convertedDeptTitle);
			SpannableString convertedEmergTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(emergTitle, banglaFont, (float)1);
			txtEmergTitle.setText(convertedEmergTitle);
			SpannableString convertedservTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(servTitle, banglaFont, (float)1);
			txtServTitle.setText(convertedservTitle);
			SpannableString convertedConTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(conTitle, banglaFont, (float)1);
			txtConTitle.setText(convertedConTitle);
			SpannableString convertedServ24=AndroidCustomFontSupport.getCorrectedBengaliFormat(serv24, banglaFont, (float)1);
			txt24_7.setText(convertedServ24);
			SpannableString convertedServAmb=AndroidCustomFontSupport.getCorrectedBengaliFormat(servAmb, banglaFont, (float)1);
			txtAmbulance.setText(convertedServAmb);
			SpannableString convertedServBlood=AndroidCustomFontSupport.getCorrectedBengaliFormat(servBlood, banglaFont, (float)1);
			txtBlood.setText(convertedServBlood);
			SpannableString convertedClickAddr=AndroidCustomFontSupport.getCorrectedBengaliFormat(clickAddr, banglaFont, (float)1);
			txtClickAddr.setText(convertedClickAddr);
			SpannableString convertedClickDir=AndroidCustomFontSupport.getCorrectedBengaliFormat(clickDir, banglaFont, (float)1);
			txtClickDir.setText(convertedClickDir);
			SpannableString convertedClickEmerg=AndroidCustomFontSupport.getCorrectedBengaliFormat(clickEmerg, banglaFont, (float)1);
			txtClickEmerg.setText(convertedClickEmerg);
			SpannableString convertedClickWeb=AndroidCustomFontSupport.getCorrectedBengaliFormat(clickWeb, banglaFont, (float)1);
			txtClickWeb.setText(convertedClickWeb);
			SpannableString convertedClickMail=AndroidCustomFontSupport.getCorrectedBengaliFormat(clickMail, banglaFont, (float)1);
			txtClickMail.setText(convertedClickMail);
		}
		
		else
		{
			txtTitle.setText(title);
			txtAddrTitle.setText(addrTitle);
			txtDirTitle.setText(dirTitle);
			txtDeptTitle.setText(deptTitle);
			txtEmergTitle.setText(emergTitle);
			txtServTitle.setText(servTitle);
			txtConTitle.setText(conTitle);
			txt24_7.setText(serv24);
			txtAmbulance.setText(servAmb);
			txtBlood.setText(servBlood);
			txtClickAddr.setText(clickAddr);
			txtClickDir.setText(clickDir);
			txtClickEmerg.setText(clickEmerg);
			txtClickWeb.setText(clickWeb);
			txtClickMail.setText(clickMail);
		}
		
		setTextData();
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}	
        
        
            linearFavourite.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try
				{
					String sName="";
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.FAVOURITE_BANGLA_HOSPITAL);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_FAVOURITE_BANGLA, columns, Constants.FAVOURITE_BANGLA_HOSPITAL, Constants.STRING_ONLY, name, Constants.FAVOURITE_BANGLA_HOSPITAL);
					if(data.get(Constants.FAVOURITE_BANGLA_HOSPITAL).size()>0) sName=data.get(Constants.FAVOURITE_BANGLA_HOSPITAL).get(0);
					data.clear();
					
					if(sName.equals(name)) isExists=true;
					else isExists=false;
				}
				catch(Exception e) {}
				
				LayoutInflater inflater=LayoutInflater.from(DetailsActivityBangla.this);
				View view=inflater.inflate(R.layout.warning_dialog, null);
				LinearLayout linearYes,linearNo;
				TextView txtYes,txtNo,txtWarning;
				linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
				linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
				txtYes=(TextView)view.findViewById(R.id.txtYes);
				txtNo=(TextView)view.findViewById(R.id.txtNo);
				txtWarning=(TextView)view.findViewById(R.id.txtWarning);
				Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
				txtWarning.setTypeface(banglaFont);
				txtYes.setTypeface(banglaFont);
				txtNo.setTypeface(banglaFont);
				
				String warning;
				if(isExists) warning="হাসপাতালটি আপনার প্রিয় তালিকাতে আছে। আপনি কি ইহাকে আপনার প্রিয় তালিকা থেকে বের করতে চান?";
				else warning="আপনি কি হাসপাতালটিকে আপনার প্রিয় তালিকাতে যোগ করতে চান?";
				
				String yes="হ্যাঁ";
				String no="না";
				
				if(supported)
				{
					SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
					txtWarning.setText(convertedWarning);
					SpannableString convertedYes=AndroidCustomFontSupport.getCorrectedBengaliFormat(yes, banglaFont, (float)1);
					txtYes.setText(convertedYes);
					SpannableString convertedNo=AndroidCustomFontSupport.getCorrectedBengaliFormat(no, banglaFont, (float)1);
					txtNo.setText(convertedNo);
				}
				else
				{
					txtWarning.setText(warning);
					txtYes.setText(yes);
					txtNo.setText(no);
				}
				
				AlertDialog.Builder builder=new AlertDialog.Builder(DetailsActivityBangla.this);
				builder.setView(view);
				builder.setCancelable(true);
				
				final AlertDialog dialog=builder.create();
				dialog.show();
				
		        linearYes.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
						if(isExists) makeUnFavourite();
						else makeFavourite();
					}
				});
				
				
				linearNo.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				
			}
		});
        
		
	}
	
	
	
	
	public void makeFavourite()
	{
		try
		{
				boolean success=false;
				LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
				column.put(Constants.FAVOURITE_BANGLA_HOSPITAL, Constants.IS_STRING);
				
				HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
				ArrayList<String>data1= new ArrayList<String>();
				data1.add(name);
				value.put(0,data1);
				boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE_BANGLA, column, value);
				if(response==true)
				{
					ArrayList<String>wheres=new ArrayList<String>();
					ArrayList<String>values=new ArrayList<String>();
					int[] matchPositions=new int[6];
					int index=0;
					
					wheres.add(Constants.INFO_LATITUDE);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lat);
					index++;
					
					wheres.add(Constants.INFO_LONGITUDE);
					matchPositions[index]=Constants.STRING_ONLY;
					values.add(lon);
					index++;
					
					String bName="";
					ArrayList<String>columns2=new ArrayList<String>();
					columns2.add(Constants.INFO_NAME);
					HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_INFO, columns2, wheres, matchPositions, values, Constants.INFO_NAME);
					if(data2.get(Constants.INFO_NAME).size()>0) bName=data2.get(Constants.INFO_NAME).get(0);	
					data2.clear();
					
					if(!bName.equals(""))
					{
						LinkedHashMap<String, Integer>column3=new LinkedHashMap<String, Integer>();
						column3.put(Constants.FAVOURITE_HOSPITAL, Constants.IS_STRING);
						
						HashMap< Integer,ArrayList<String>>value3=new HashMap<Integer, ArrayList<String>>();
						ArrayList<String>data3= new ArrayList<String>();
						data3.add(bName);
						value3.put(0,data3);
						boolean response3=dbOpenHelper.insertRowByColumn(Constants.TABLE_FAVOURITE, column3, value3);
						if(response3==true) success=true;
					}
				}
				if(success==false)
				{
					showToast("দুঃখিত, হাসপাতালটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
				}
				else
				{
					showToast("হাসপাতালটিকে আপনার প্রিয় তালিকাতে যোগ করা হয়েছে");
				}
			
		}
		catch(Exception e) 
		{
			showToast("দুঃখিত, হাসপাতালটিকে আপনার প্রিয় তালিকাতে যোগ করা সম্ভব হয় নি");
		}
		
	}
	
	
	
	
	public void makeUnFavourite()
	{
		try
		{
			boolean isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE_BANGLA, Constants.FAVOURITE_BANGLA_HOSPITAL, Constants.STRING_ONLY, name);
			if(isDeleted)
			{
				ArrayList<String>wheres=new ArrayList<String>();
				ArrayList<String>values=new ArrayList<String>();
				int[] matchPositions=new int[6];
				int index=0;
				
				wheres.add(Constants.INFO_LATITUDE);
				matchPositions[index]=Constants.STRING_ONLY;
				values.add(lat);
				index++;
				
				wheres.add(Constants.INFO_LONGITUDE);
				matchPositions[index]=Constants.STRING_ONLY;
				values.add(lon);
				index++;
				
				String bName="";
				ArrayList<String>columns2=new ArrayList<String>();
				columns2.add(Constants.INFO_NAME);
				HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowStringMore(Constants.TABLE_INFO, columns2, wheres, matchPositions, values, Constants.INFO_NAME);
				if(data2.get(Constants.INFO_NAME).size()>0) bName=data2.get(Constants.INFO_NAME).get(0);	
				data2.clear();
				
				if(!bName.equals(""))
				{
					isDeleted=dbOpenHelper.deleteRowUsingString(Constants.TABLE_FAVOURITE, Constants.FAVOURITE_HOSPITAL, Constants.STRING_ONLY, bName);
					if(isDeleted) showToast("আপনার সিদ্ধান্ত ঠিকমত কার্যকর হয়েছে");
					else showToast("দুঃখিত, আপনার সিদ্ধান্ত ঠিকমত কার্যকর করা যায় নি");
				}
			}
		}
		catch(Exception e)
		{
			showToast("দুঃখিত, আপনার সিদ্ধান্ত ঠিকমত কার্যকর করা যায় নি");
		}
	}
	
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	
	
	
	public void optionClicked(View view)
	{
		if(view.getId()==R.id.linearAddress)
		{
			showMap();
		}
		else if(view.getId()==R.id.linearDirection)
		{
			showMap();
		}
		else if(view.getId()==R.id.linearDept)
		{
			if(depts.size()>1)
			{
				Intent intent=new Intent(DetailsActivityBangla.this,EachOptionActivityBangla.class);
				intent.putExtra("option", "ডিপার্টমেন্ট");
				intent.putExtra("title", name);
				intent.putExtra("items",depts);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			}
		}
		else if(view.getId()==R.id.linearEmergency)
		{
			if(!emergency.equals(""))
			{
				showCallDialog(emergency);
			}
		}
		else if(view.getId()==R.id.linearWebsite)
		{
			if(!website.equals("অ্যাভেইলেবল না"))
			{
				Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(website));
				startActivity(intent);
			}
		}
		else if(view.getId()==R.id.linearEmail)
		{
			if(!email.equals(""))
			{
				String[] sendTo={email};
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.setData(Uri.parse("mailto:"));
				emailIntent.setType("text/html");
				emailIntent.putExtra(Intent.EXTRA_EMAIL,sendTo);
				//emailIntent.putExtra(Intent.EXTRA_CC, CC);
			    //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
			    //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");
				try {
			         startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			      } catch (android.content.ActivityNotFoundException ex) {
			    	  showToast("আপনার মোবাইল ফোনে কোন ইমেইল ক্লায়েন্ট ইন্সটল করা নেই");
			      }
			}
		}
	}
	
	
	
	
	public void setTextData()
	{
		if(supported)
		{
			SpannableString convertedAddress=AndroidCustomFontSupport.getCorrectedBengaliFormat(address, banglaFont, (float)1);
			txtAddr.setText(convertedAddress);
		}
		else
		{
			txtAddr.setText(address);
		}
		
		if(supported)
		{
			SpannableString convertedDir=AndroidCustomFontSupport.getCorrectedBengaliFormat(direction, banglaFont, (float)1);
			txtDir.setText(convertedDir);
		}
		else
		{
			txtDir.setText(direction);
		}
		
		String url="";
		if(website.equals("অ্যাভেইলেবল না"))
		{
			url+="অ্যাভেইলেবল না";
			txtClickWeb.setVisibility(View.GONE);
		}
		else
		{
			boolean flag=false;
			for(int i=0;i<website.length();i++)
			{
				if(website.charAt(i)=='w') flag=true;
				if(flag==true&&website.charAt(i)!='/') url=url+website.charAt(i);
			}
		}
		
		if(supported)
		{
			SpannableString convertedWeb=AndroidCustomFontSupport.getCorrectedBengaliFormat(url, banglaFont, (float)1);
			txtWeb.setText(convertedWeb);
		}
		else
		{
			txtWeb.setText(url);
		}
		
		if(!email.equals(""))
		{
			if(supported)
			{
				SpannableString convertedEmail=AndroidCustomFontSupport.getCorrectedBengaliFormat(email, banglaFont, (float)1);
				txtMail.setText(convertedEmail);
			}
			else
			{
				txtMail.setText(email);
			}
		}
		else
		{
			if(supported)
			{
				SpannableString convertedEmail=AndroidCustomFontSupport.getCorrectedBengaliFormat("অ্যাভেইলেবল না", banglaFont, (float)1);
				txtMail.setText(convertedEmail);
			}
			else
			{
				txtMail.setText("অ্যাভেইলেবল না");
			}
			txtClickMail.setVisibility(View.GONE);
		}
		
		if(!emergency.equals(""))
		{
			if(supported)
			{
				SpannableString convertedEmerg=AndroidCustomFontSupport.getCorrectedBengaliFormat(emergency, banglaFont, (float)1);
				txtEmerg.setText(convertedEmerg);
			}
			else
			{
				txtEmerg.setText(emergency);
			}
		}
		else
		{
			if(supported)
			{
				SpannableString convertedEmerg=AndroidCustomFontSupport.getCorrectedBengaliFormat("অ্যাভেইলেবল না", banglaFont, (float)1);
				txtEmerg.setText(convertedEmerg);
			}
			else
			{
				txtEmerg.setText("অ্যাভেইলেবল না");
			}
			txtClickEmerg.setVisibility(View.GONE);
		}
		
		if(service24.equals("অ্যাভেইলেবল"))
		{
			img24_7.setImageResource(R.drawable.checkbox_checked);
		}
		else
		{
			img24_7.setImageResource(R.drawable.checkbox_unchecked);
		}
		
		if(bloodBank.equals("অ্যাভেইলেবল"))
		{
			imgBlood.setImageResource(R.drawable.checkbox_checked);
		}
		else
		{
			imgBlood.setImageResource(R.drawable.checkbox_unchecked);
		}
		
		if(!ambulance.equals("অ্যাভেইলেবল না")) 
		{
			imgAmbulance.setImageResource(R.drawable.checkbox_checked);
		}
		else
		{
			imgAmbulance.setImageResource(R.drawable.checkbox_unchecked);
		}
		
		if(depts.size()<=1)
		{
			if(supported)
			{
				SpannableString convertedDept=AndroidCustomFontSupport.getCorrectedBengaliFormat(depts.get(0), banglaFont, (float)1);
				txtDept.setText(convertedDept);
			}
			else
			{
				txtDept.setText(depts.get(0));
			}
		}
		else
		{
			if(supported)
			{
				SpannableString convertedDept=AndroidCustomFontSupport.getCorrectedBengaliFormat("ডিপার্টমেন্টগুলো দেখতে ক্লিক করুন", banglaFont, (float)1);
				txtDept.setText(convertedDept);
			}
			else
			{
				txtDept.setText("ডিপার্টমেন্টগুলো দেখতে ক্লিক করুন");
			}
		}
		
	}
	
	
	
	
	public void showCallDialog(final String number) 
	{
		
		/*	
		try
        {
            ContentResolver cr = this.getContentResolver();
            ContentValues cv = new ContentValues();
           // cv.put(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, "New Name");
            cv.put(ContactsContract.CommonDataKinds.Phone.NUMBER,number);
            cv.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
            cr.insert(ContactsContract.RawContacts.CONTENT_URI, cv);

            Toast.makeText(this, "Contact added", Toast.LENGTH_LONG).show();
        }
        catch(Exception e)
        {
            TextView tv = new TextView(this);
            tv.setText(e.toString());
            setContentView(tv);
        }   */
		
		LayoutInflater inflater=LayoutInflater.from(this);
		View view=inflater.inflate(R.layout.option_dialog, null);
		LinearLayout linearSendVia,linearSave,linearCall,linearClose;
		TextView txtDialogTitle,txtSendVia,txtSave,txtCall,txtClose;
		linearSendVia=(LinearLayout)view.findViewById(R.id.linearSendVia);
		linearSave=(LinearLayout)view.findViewById(R.id.linearSave);
		linearCall=(LinearLayout)view.findViewById(R.id.linearCall);
		linearClose=(LinearLayout)view.findViewById(R.id.linearClose);
		txtDialogTitle=(TextView)view.findViewById(R.id.txtTitle);
		txtSendVia=(TextView)view.findViewById(R.id.txtSendVia);
		txtSave=(TextView)view.findViewById(R.id.txtSave);
		txtCall=(TextView)view.findViewById(R.id.txtCall);
		txtClose=(TextView)view.findViewById(R.id.txtClose);
		
		String dialogTitle="বাছাই করুন";
		String send="ক্ষুদেবার্তার মাধ্যমে পাঠান";
		String save="মুঠোফোনে সংরক্ষণ করুন";
		String call="ফোন করুন";
		String close="বন্ধ করুন";
		
		txtDialogTitle.setTypeface(banglaFont);;
		txtSendVia.setTypeface(banglaFont);
		txtSave.setTypeface(banglaFont);
		txtCall.setTypeface(banglaFont);
		txtClose.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedDialogTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(dialogTitle, banglaFont, (float)1);
			SpannableString convertedSend=AndroidCustomFontSupport.getCorrectedBengaliFormat(send, banglaFont, (float)1);
			SpannableString convertedSave=AndroidCustomFontSupport.getCorrectedBengaliFormat(save, banglaFont, (float)1);
			SpannableString convertedCall=AndroidCustomFontSupport.getCorrectedBengaliFormat(call, banglaFont, (float)1);
			SpannableString convertedClose=AndroidCustomFontSupport.getCorrectedBengaliFormat(close, banglaFont, (float)1);
			txtDialogTitle.setText(convertedDialogTitle);
			txtSendVia.setText(convertedSend);
			txtSave.setText(convertedSave);
			txtCall.setText(convertedCall);
			txtClose.setText(convertedClose);
		}
		else
		{
			txtDialogTitle.setText(dialogTitle);
			txtSendVia.setText(send);
			txtSave.setText(save);
			txtCall.setText(call);
			txtClose.setText(close);
		}
		
		AlertDialog.Builder builder=new AlertDialog.Builder(this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearSendVia.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				
				String uriStr = "sms:";
				Uri smsUri = Uri.parse(uriStr);
				Intent smsIntent = new Intent(Intent.ACTION_VIEW, smsUri);
				smsIntent.putExtra("sms_body", number);
				startActivity(smsIntent);
				
			}
		});
		
		linearSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				  ArrayList<ContentValues> data = new ArrayList<ContentValues>();

				  ContentValues row1 = new ContentValues();
				  row1.put(Phone.NUMBER, number);
				  data.add(row1);  

				  Intent intent = new Intent(Intent.ACTION_INSERT, Contacts.CONTENT_URI);
				  intent.putExtra(Insert.PHONE,number);
				  startActivity(intent); 
				
			}
		});
		
		linearCall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// put the phone number in number variable
		        //String number="01.....";
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				String phone="tel:"+number;
				try {
		                callIntent.setData(Uri.parse(phone));
		                startActivity(callIntent);
		            }
		        catch (android.content.ActivityNotFoundException ex) 
		        {
		        	//Toast.makeText(context,"Call faild, please try again later.", Toast.LENGTH_LONG).show();
		        	showToast("কল করা সম্ভব হই নি, দয়া করে আবার চেষ্টা করুন");
		        }   
				
			}
		});
		
		linearClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
	}
	
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস আপডেট করা নাই");
		}
		else
		{
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস সাপোর্ট করে না");
		}
		return false;
	}
	
	
	
	public void showMap()
	{
		if(isConnectingInternet())
		{
			if(isMapAvailable())
			{
				Intent in=new Intent(DetailsActivityBangla.this,LocationActivityBangla.class);
				in.putExtra("name", name);
				//in.putExtra("id", id);
				in.putExtra("lat", lat);
				in.putExtra("lon", lon);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			}
		}
		else
		{
			
			LayoutInflater li = LayoutInflater.from(DetailsActivityBangla.this);
			View promptsView = li.inflate(R.layout.internet_dialog, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(DetailsActivityBangla.this);
			builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
			TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
			TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
			TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
			
			LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
			LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
			
			dialogTitle.setTypeface(banglaFont);
			txtMain.setTypeface(banglaFont);
			txtOptional.setTypeface(banglaFont);
			txtOk.setTypeface(banglaFont);
			txtCancel.setTypeface(banglaFont);
			
			String header="সতর্কবার্তা";
			String main="ইন্টারনেট কানেকশন দরকার";
			String optional="আপনার মোবাইল ফোনের ইন্টারনেট কানেকশন অন করতে ঠিক আছে বাটনে ক্লিক করুন";
			String ok="ঠিক আছে";
			String cancel="বাতিল";
			if(supported)
			{
				SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
				dialogTitle.setText(convertedHeader);
				SpannableString convertedMain=AndroidCustomFontSupport.getCorrectedBengaliFormat(main, banglaFont, (float)1);
				txtMain.setText(convertedMain);
				SpannableString convertedOptional=AndroidCustomFontSupport.getCorrectedBengaliFormat(optional, banglaFont, (float)1);
				txtOptional.setText(convertedOptional);
				SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
				txtOk.setText(convertedOk);
				SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
				txtCancel.setText(convertedCancel);
			}
			else
			{
				dialogTitle.setText(header);
				txtMain.setText(main);
				txtOptional.setText(optional);
				txtOk.setText(ok);
				txtCancel.setText(cancel);
			}
			
			linearCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
				}
			});	
			
			linearOk.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
			
			
		}
	}
	
	
	

}
