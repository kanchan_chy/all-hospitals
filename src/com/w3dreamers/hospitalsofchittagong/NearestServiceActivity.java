package com.w3dreamers.hospitalsofchittagong;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Intents.Insert;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

public class NearestServiceActivity extends FragmentActivity{
	
	    //TextView[] txtTabs=new TextView[6];
		LinearLayout[] linearTabs=new LinearLayout[6];
		int[] linearIds={R.id.linearHospital,R.id.linearAmbulance,R.id.linearBloodBank,R.id.linearPharmecy};
		//int[] txtIds={R.id.txtHospital,R.id.txtAmbulance,R.id.txtBloodBank,R.id.txtPharmecy};
		
		GoogleMap map;
		
		Double myLat,myLon;
		LatLng myLoc;
		Marker myMarker;
		
		LatLng[] locs=new LatLng[200];
		Marker[] markers=new Marker[200];
		
		boolean[] draw=new boolean[200];
		double[] lats=new double[200];
		double[] lons=new double[200];
		
		float minDistance;
		int pos,cur;
		boolean isLoaded;
		
		int selected;
		
		ArrayList<String>names=new ArrayList<String>();
		
		ArrayList<String>hospitals=new ArrayList<String>();
		ArrayList<String>ambulances=new ArrayList<String>();
		ArrayList<String>bloods=new ArrayList<String>();
		ArrayList<String>pharmecies=new ArrayList<String>();
		ArrayList<String>hos_lats=new ArrayList<String>();
		ArrayList<String>hos_lons=new ArrayList<String>();
		ArrayList<String>amb_lats=new ArrayList<String>();
		ArrayList<String>amb_lons=new ArrayList<String>();
		ArrayList<String>blood_lats=new ArrayList<String>();
		ArrayList<String>blood_lons=new ArrayList<String>();
		ArrayList<String>phar_lats=new ArrayList<String>();
		ArrayList<String>phar_lons=new ArrayList<String>();
		
		DbHelper dbOpenHelper;
		
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.nearest_service_layout);
		for(int i=0;i<linearIds.length;i++)
		{
			//txtTabs[i]=(TextView)findViewById(txtIds[i]);
			linearTabs[i]=(LinearLayout)findViewById(linearIds[i]);
		}
		
		hospitals=(ArrayList<String>)getIntent().getSerializableExtra("hospitals");
		ambulances=(ArrayList<String>)getIntent().getSerializableExtra("ambulances");
		bloods=(ArrayList<String>)getIntent().getSerializableExtra("bloods");
		pharmecies=(ArrayList<String>)getIntent().getSerializableExtra("pharmecies");
		hos_lats=(ArrayList<String>)getIntent().getSerializableExtra("hos_lats");
		hos_lons=(ArrayList<String>)getIntent().getSerializableExtra("hos_lons");
		amb_lats=(ArrayList<String>)getIntent().getSerializableExtra("amb_lats");
		amb_lons=(ArrayList<String>)getIntent().getSerializableExtra("amb_lons");
		blood_lats=(ArrayList<String>)getIntent().getSerializableExtra("blood_lats");
		blood_lons=(ArrayList<String>)getIntent().getSerializableExtra("blood_lons");
		phar_lats=(ArrayList<String>)getIntent().getSerializableExtra("phar_lats");
		phar_lons=(ArrayList<String>)getIntent().getSerializableExtra("phar_lons");
		
		myLat=getIntent().getExtras().getDouble("lat");
		myLon=getIntent().getExtras().getDouble("lon");
		
		isLoaded=false;
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		
		try
		{
			map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			map.setMyLocationEnabled(true);
		}
		catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), "Sorry... Map can not be shown", Toast.LENGTH_LONG).show();
			finish();
		}  
		
		selected=0;
		setMap(hospitals, hos_lats, hos_lons);
	
		
        linearTabs[0].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[0].setBackgroundResource(R.drawable.selected);
				try
				{
					map.clear();
				}
				catch(Exception e)
				{
					Toast.makeText(getApplicationContext(), "Previous map can't be cleared", Toast.LENGTH_LONG).show();
				}
				selected=0;
				setMap(hospitals, hos_lats, hos_lons);
			}
		});
		
		
		linearTabs[1].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[1].setBackgroundResource(R.drawable.selected);				
				try
				{
					map.clear();
				}
				catch(Exception e)
				{
					Toast.makeText(getApplicationContext(), "Previous map can't be cleared", Toast.LENGTH_LONG).show();
				}
				selected=1;
				setMap(ambulances, amb_lats, amb_lons);
			}
		});
		
		
		linearTabs[2].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[2].setBackgroundResource(R.drawable.selected);				
				try
				{
					map.clear();
				}
				catch(Exception e)
				{
					Toast.makeText(getApplicationContext(), "Previous map can't be cleared", Toast.LENGTH_LONG).show();
				}
				selected=2;
				setMap(bloods, blood_lats, blood_lons);
			}
		});
		
		
		linearTabs[3].setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearTabs[selected].setBackgroundResource(R.drawable.unselected);
				linearTabs[3].setBackgroundResource(R.drawable.selected);				
				try
				{
					map.clear();
				}
				catch(Exception e)
				{
					Toast.makeText(getApplicationContext(), "Previous map can't be cleared", Toast.LENGTH_LONG).show();
				}
				selected=3;
				setMap(pharmecies, phar_lats, phar_lons);
			}
		});
		
		
		
           map.setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(Marker marker) {
				// TODO Auto-generated method stub
				if(marker.getTitle().equals("You are here")) return null;
				else
				{
					 View v=getLayoutInflater().inflate(R.layout.infowindow, null);
					 TextView txtHeader=(TextView)v.findViewById(R.id.txtHeader);
					 TextView txtBody=(TextView)v.findViewById(R.id.txtBody);
					 
					 txtHeader.setText(marker.getTitle());
					 txtBody.setText(marker.getSnippet());
					 
					 return v;
				}
			}
			
			@Override
			public View getInfoContents(Marker marker) {
				// TODO Auto-generated method stub
				return null;

			}
		});
           
           
           
           
           map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
   		    @Override
   		    public void onMapLoaded() {
   		    	isLoaded=true;
   		    	try
   		    	{
   		    		LatLngBounds.Builder builder = new LatLngBounds.Builder();
   		    		for(int i=0;i<names.size();i++)
   			    	{
   			    		builder.include(locs[i]);
   			    	}
   		    		builder.include(myLoc);
   			    	LatLngBounds bounds=builder.build();
   			        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
   			        markers[pos].showInfoWindow();
   			        cur=pos;
   		    	}
   		    	catch(Exception e){}
   		    }
   		});
           
           
           
           
           map.setOnMarkerClickListener(new OnMarkerClickListener() {
   			
   			@Override
   			public boolean onMarkerClick(Marker marker) {
   				// TODO Auto-generated method stub
   				if(marker.getTitle().equals("You are here"))
   				{
   					if(draw[cur]==false)
   					{
   						try
   						{
   							if(marker.isInfoWindowShown()) marker.hideInfoWindow();
   							markers[cur].showInfoWindow();
   							draw[cur]=true;
   							Toast.makeText(getApplicationContext(), "Please wait to see the path", Toast.LENGTH_LONG).show();
   							try{
   								 final LatLngBounds bounds = new LatLngBounds.Builder().include(myLoc).include(locs[cur]).build();
   							     map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
   							}
   							catch(Exception e)
   							{
   								Toast.makeText(getApplicationContext(), "Showing path was not possible, please try again", Toast.LENGTH_LONG).show();
   							}
   						    String str_origin = "origin="+myLat+","+myLon;
   					        // Destination of route
   					        String str_dest = "destination="+lats[cur]+","+lons[cur]; 
   					        // Sensor enabled
   					        String sensor = "sensor=false"; 
   					        // Building the parameters to the web service
   					        String parameters = str_origin+"&"+str_dest+"&"+sensor;	 
   					        // Output format
   					        String output = "json";	 
   					        // Building the url to the web service
   					        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
   					        DownloadTask downloadTask = new DownloadTask();	        
   				         // Start downloading json data from Google Directions API
   				            downloadTask.execute(url); 
   						}
   						catch(Exception e)
   						{
   							Toast.makeText(getApplicationContext(), "Showing path was not possible, please try again", Toast.LENGTH_LONG).show();
   						}
   					}
   					else Toast.makeText(getApplicationContext(), "Please wait to see the path", Toast.LENGTH_LONG).show();
   				}
   				else
   				{
   					for(int i=0;i<names.size();i++)
   					{
   						if(marker.getTitle().equals(names.get(i)))
   						{
   							markers[cur].hideInfoWindow();
   							markers[i].showInfoWindow();
   							cur=i;
   							break;
   						}
   					}
   				}
   				return true;
   			}
   		});
           
           
           
           
           
           map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
   			
   			@Override
   			public void onInfoWindowClick(Marker marker) {
   				// TODO Auto-generated method stub
   				try
   				{
   					String name=marker.getTitle();
   	   				LatLng sLoc=marker.getPosition();
   	   				double sLat=sLoc.latitude;
   	   				double sLon=sLoc.longitude;
   	   				
   						try
   						{
   							String number="",address="";
   							if(selected==0)
   							{
   								HospitalInfoRetreiver retreiver=new HospitalInfoRetreiver(NearestServiceActivity.this);
   								retreiver.DetailActivityCaller(name);
   							}
   							else if(selected==1)
   		   					{  		   	   					
   		   	   				    ArrayList<String>columnName=new ArrayList<String>();
   								columnName.add(Constants.AMBULANCE_NUMBER_NUMBER);
   								columnName.add(Constants.AMBULANCE_NUMBER_ADDRESS);
   								HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER, columnName, Constants.AMBULANCE_NUMBER_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_NAME);
   								if(data.get(Constants.AMBULANCE_NUMBER_NUMBER).size()>0) number=data.get(Constants.AMBULANCE_NUMBER_NUMBER).get(0);
   								if(data.get(Constants.AMBULANCE_NUMBER_ADDRESS).size()>0) address=data.get(Constants.AMBULANCE_NUMBER_ADDRESS).get(0);
   		   	   					
   		   						//showCallDialog(number);
   								Intent in=new Intent(NearestServiceActivity.this,DetailsPharmecyActivity.class);
   								in.putExtra("title", "Ambulance");
   								in.putExtra("name", name);
   								in.putExtra("number", number);
   								in.putExtra("address", address);
   								in.putExtra("lat", ""+sLat);
   								in.putExtra("lon", ""+sLon);
   								startActivity(in);
   								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
   		   					}
   							else if(selected==2)
   							{
   								ArrayList<String>columnName=new ArrayList<String>();
   								columnName.add(Constants.BLOOD_BANK_NUMBER);
   								columnName.add(Constants.BLOOD_BANK_ADDRESS);
   								HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_BLOOD_BANK, columnName, Constants.BLOOD_BANK_NAME, Constants.STRING_ONLY, name, Constants.BLOOD_BANK_NAME);
   								if(data.get(Constants.BLOOD_BANK_NUMBER).size()>0) number=data.get(Constants.BLOOD_BANK_NUMBER).get(0);
   								if(data.get(Constants.BLOOD_BANK_ADDRESS).size()>0) address=data.get(Constants.BLOOD_BANK_ADDRESS).get(0);
   								Intent in=new Intent(NearestServiceActivity.this,DetailsPharmecyActivity.class);
   								in.putExtra("title","Blood Bank");
   								in.putExtra("name", name);
   								in.putExtra("number", number);
   								in.putExtra("address", address);
   								in.putExtra("lat", ""+sLat);
   								in.putExtra("lon", ""+sLon);
   								startActivity(in);
   								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
   							}
   							else if(selected==3)
   							{
   								ArrayList<String>columnName=new ArrayList<String>();
   								columnName.add(Constants.PHARMECY_NUMBER);
   								columnName.add(Constants.PHARMECY_ADDRESS);
   								HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_PHARMECY, columnName, Constants.PHARMECY_NAME, Constants.STRING_ONLY, name, Constants.PHARMECY_NAME);
   								if(data.get(Constants.PHARMECY_NUMBER).size()>0) number=data.get(Constants.PHARMECY_NUMBER).get(0);
   								if(data.get(Constants.PHARMECY_ADDRESS).size()>0) address=data.get(Constants.PHARMECY_ADDRESS).get(0);
   								Intent in=new Intent(NearestServiceActivity.this,DetailsPharmecyActivity.class);
   								in.putExtra("title","Pharmecy");
   								in.putExtra("name", name);
   								in.putExtra("number", number);
   								in.putExtra("address", address);
   								in.putExtra("lat", ""+sLat);
   								in.putExtra("lon", ""+sLon);
   								startActivity(in);
   								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
   							}
   						}
   						catch(Exception e)
   						{
   							
   						}
   	   				
   				}
   				catch(Exception e) {}
   			}
   		});
           
           
		
		
		
	}
	
	
	
	public void setMap(ArrayList<String>names,ArrayList<String>latitudes,ArrayList<String>longitudes)
	{
		this.names=names;
		cur=pos=0;
		for(int i=0;i<names.size();i++)
		{
			lats[i]=Double.valueOf(latitudes.get(i));
			lons[i]=Double.valueOf(longitudes.get(i));
			draw[i]=false;
		}
		
		try
		{
			//map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			//map.setMyLocationEnabled(true);
			
			myLoc=new LatLng(myLat, myLon);
			myMarker =map.addMarker(new MarkerOptions().position(myLoc).title("You are here").snippet("Click to see the path"));
			myMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.i_m_here));
			float x=(float)0.5;
			float y=(float)0.3;
			myMarker.setAnchor(x,y);
			
			float[] results = new float[1];
			float distance;
			minDistance=100000000;
			
			String snippet;
			if(selected==1) snippet="Click to call";
			else snippet="Click to see details";
			for(int i=0;i<names.size();i++)
			{
				locs[i] = new LatLng(lats[i], lons[i]);
				markers[i] =map.addMarker(new MarkerOptions().position(locs[i]).title(names.get(i)).snippet(snippet));
				markers[i].setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
				draw[i]=false;
				
				Location.distanceBetween(myLat,myLon,lats[i], lons[i], results);
			    distance=results[0]/1000;
			    if(distance<minDistance)
			    {
			    	minDistance=distance;
			    	pos=i;
			    }
			}
			
			
			if(isLoaded)
			{
				LatLngBounds.Builder builder = new LatLngBounds.Builder();
	    		for(int i=0;i<names.size();i++)
		    	{
		    		builder.include(locs[i]);
		    	}
	    		builder.include(myLoc);
		    	LatLngBounds bounds=builder.build();
		        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
		        markers[pos].showInfoWindow();
		        cur=pos;
			}
			
		}
		catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), "Sorry... Map can not be shown", Toast.LENGTH_LONG).show();
			finish();
		}
		
	}
	
	
	
	
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }
            data = sb.toString();
            br.close();
 
        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    
    
    
    
    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String>{

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }
    
    
    
    
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            map.addPolyline(lineOptions);
        }

    }
	
	
	
	

}
