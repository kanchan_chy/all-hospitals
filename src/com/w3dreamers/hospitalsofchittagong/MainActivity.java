package com.w3dreamers.hospitalsofchittagong;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Window;
import android.widget.TextView;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

public class MainActivity extends Activity {
	
	TextView txtTitle;
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	boolean notUpdated=false;
	String value="",language;
	
	DbHelper dbOpenHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		
		SharedPreferences prefLang=getSharedPreferences("Language", MODE_PRIVATE);
		language=prefLang.getString("language", "English");
		
		if(language.equals("English"))
		{
			txtTitle.setText("Hospitals of Chittagong");
		}
		else
		{
			SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
			boolean supported=prefSupport.getBoolean("supported", true);
			
			String title="চট্টগ্রামের হাসপাতালসমূহ";
			Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
			txtTitle.setTypeface(banglaFont);
			if(supported)
			{
				SpannableString convertedtitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
				txtTitle.setText(convertedtitle);;
			}
			else
			{
				txtTitle.setText(title);
			}
		}
		
		pref=getSharedPreferences("database_hospital",0);
		value=pref.getString("load", "no");
		if(value.equals("no"))
		{
			notUpdated=true;
		}
		else
		{
			notUpdated=false;
		}
		
		TimeSpender spender=new TimeSpender();
		spender.execute();
		
	}

	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}
	
	
	
	class TimeSpender extends AsyncTask<String, String, String>
	{
		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			
			try
			{
				if(notUpdated)
				{
					dbOpenHelper = new DbHelper(MainActivity.this, Constants.DATABASE_NAME);

					editor=pref.edit();
					editor.putString("load","yes");			
					editor.commit();
				}
			}
			catch(Exception e) {}
			
			try
			{
				Thread.sleep(1500);
			}
			catch(Exception e) {}
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if(language.equals("English"))
			{
				Intent in=new Intent(MainActivity.this,OptionActivity.class);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				finish();
			}
			else
			{
				Intent in=new Intent(MainActivity.this,OptionActivityBangla.class);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				finish();
			}
		}
		
	}
	
	

}
