package com.w3dreamers.hospitalsofchittagong;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class NearestHospitalActivityBangla extends FragmentActivity implements GooglePlayServicesClient.OnConnectionFailedListener,GooglePlayServicesClient.ConnectionCallbacks, LocationListener{
	
    TextView txtTitle;
	
	LocationClient locationClient;
	GoogleMap map;
	
	Double myLat,myLon;
	LatLng myLoc;
	Marker myMarker;
	
	LatLng[] locs=new LatLng[200];
	Marker[] markers=new Marker[200];
	
	boolean[] draw=new boolean[200];
	double[] lats=new double[200];
	double[] lons=new double[200];
	
	float minDistance;
	int pos,cur;
	boolean isLoaded=false,isRetreived=false;
	
	boolean supported;
	Typeface banglaFont;
	
	ArrayList<String> hospitals=new ArrayList<String>();
	ArrayList<String> latitudes=new ArrayList<String>();
	ArrayList<String> longitudes=new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.nearest_hospital_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		
		hospitals=(ArrayList<String>)getIntent().getSerializableExtra("hospitals");
		latitudes=(ArrayList<String>)getIntent().getSerializableExtra("lats");
		longitudes=(ArrayList<String>)getIntent().getSerializableExtra("lons");
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		String title="চট্টগ্রামের হাসপাতালসমূহ";
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
		}
		else
		{
			txtTitle.setText(title);
		}
		
		cur=pos=0;
		try
		{
			for(int i=0;i<hospitals.size();i++)
			{
				lats[i]=Double.valueOf(latitudes.get(i));
				lons[i]=Double.valueOf(longitudes.get(i));
				draw[i]=false;
			}
		}
		catch(Exception e)
		{
			finish();
		}
		
		try
		{
			map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			map.setMyLocationEnabled(true);
			
			for(int i=0;i<hospitals.size();i++)
			{
				locs[i] = new LatLng(lats[i], lons[i]);
				markers[i] =map.addMarker(new MarkerOptions().position(locs[i]).title(hospitals.get(i)).snippet("বিস্তারিত দেখতে ক্লিক করুন"));
				markers[i].setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			}
		}
		catch(Exception e)
		{
			showToast("দুঃখিত, ম্যাপ দেখানো সম্ভব হছে না");
			finish();
		}
		
		try
		{
			locationClient=new LocationClient(this, this, this);
		}
		catch(Exception e){}
		
		
		
		
		map.setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(Marker marker) {
				// TODO Auto-generated method stub
				if(marker.getTitle().equals("আপনি এখানে")) return null;
				else
				{
					 View v=getLayoutInflater().inflate(R.layout.infowindow, null);
					 TextView txtHeader=(TextView)v.findViewById(R.id.txtHeader);
					 TextView txtBody=(TextView)v.findViewById(R.id.txtBody);
					 txtHeader.setTypeface(banglaFont);
					 txtBody.setTypeface(banglaFont);
					 String markerTitle=marker.getTitle();
					 String markerSnippet=marker.getSnippet();
					 if(supported)
					 {
						 SpannableString convertedMarkerTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(markerTitle, banglaFont, (float)1);
					     txtHeader.setText(convertedMarkerTitle);
					     SpannableString convertedMarkerSnippet=AndroidCustomFontSupport.getCorrectedBengaliFormat(markerSnippet, banglaFont, (float)1);
					     txtBody.setText(convertedMarkerSnippet);
					 }
					 else
					 {
						 txtHeader.setText(markerTitle);
						 txtBody.setText(markerSnippet);
					 }
					 
					 return v;
				}
			}
			
			@Override
			public View getInfoContents(Marker marker) {
				// TODO Auto-generated method stub
				return null;

			}
		});
		
		
		
		map.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
		    @Override
		    public void onMapLoaded() {
		    	try
		    	{
		    		LatLngBounds.Builder builder = new LatLngBounds.Builder();
			    	if(isRetreived==false)
			    	{
			    		for(int i=0;i<hospitals.size();i++)
				    	{
				    		builder.include(locs[i]);
				    	}
			    	}
			    	else
			    	{
			    		builder.include(myLoc).include(locs[pos]);
			    	}
			    	LatLngBounds bounds=builder.build();
			        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
			        markers[pos].showInfoWindow();
			        cur=pos;
		    	}
		    	catch(Exception e){}
		    	isLoaded=true;
		    }
		});
		
		
		
		map.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				if(marker.getTitle().equals("আপনি এখানে"))
				{
					if(draw[cur]==false)
					{
						try
						{
							if(marker.isInfoWindowShown()) marker.hideInfoWindow();
							markers[cur].showInfoWindow();
							draw[cur]=true;
							showToast("পথ দেখতে অপেক্ষা করুন");
							try{
								 final LatLngBounds bounds = new LatLngBounds.Builder().include(myLoc).include(locs[cur]).build();
							     map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
							}
							catch(Exception e)
							{
								showToast("পথ দেখানো সম্ভব হচ্ছে না, দয়া করে আবার চেষ্টা করুন");
							}
						    String str_origin = "origin="+myLat+","+myLon;
					        // Destination of route
					        String str_dest = "destination="+lats[cur]+","+lons[cur]; 
					        // Sensor enabled
					        String sensor = "sensor=false"; 
					        // Building the parameters to the web service
					        String parameters = str_origin+"&"+str_dest+"&"+sensor;	 
					        // Output format
					        String output = "json";	 
					        // Building the url to the web service
					        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
					        DownloadTask downloadTask = new DownloadTask();	        
				         // Start downloading json data from Google Directions API
				            downloadTask.execute(url); 
						}
						catch(Exception e)
						{
							showToast("পথ দেখানো সম্ভব হচ্ছে না, দয়া করে আবার চেষ্টা করুন");
						}
					}
					else showToast("পথ দেখতে অপেক্ষা করুন");
				}
				else
				{
					for(int i=0;i<hospitals.size();i++)
					{
						if(marker.getTitle().equals(hospitals.get(i)))
						{
							markers[cur].hideInfoWindow();
							markers[i].showInfoWindow();
							cur=i;
							break;
						}
					}
				}
				return true;
			}
		});
		
		
		
		
		map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			
			@Override
			public void onInfoWindowClick(Marker marker) {
				// TODO Auto-generated method stub
				String name=marker.getTitle();
				HospitalInfoRetreiverBangla retreiver=new HospitalInfoRetreiverBangla(NearestHospitalActivityBangla.this);
				retreiver.DetailActivityCaller(name);
				map.clear();
				//finish();
			}
		});
		
		
		
		
		
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try
		{
			locationClient.connect();
		}
		catch(Exception e){}
	}
	
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try
		{
			locationClient.disconnect();
		}
		catch(Exception e){}
	}
	
	public void connectMap()
	{
		try
		{
			locationClient.disconnect();
			locationClient.connect();
		}
		catch(Exception e){}
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		map.clear();
		finish();
	} 


	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		Location currentLocation = locationClient.getLastLocation();
		try{
			myLat = currentLocation.getLatitude();
			myLon = currentLocation.getLongitude();
			
			myLoc=new LatLng(myLat, myLon);
			myMarker =map.addMarker(new MarkerOptions().position(myLoc).title("আপনি এখানে").snippet("পথ দেখতে ক্লিক করুন"));
			myMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.i_m_here_bn));
			float x=(float)0.5;
			float y=(float)0.3;
			myMarker.setAnchor(x,y);
			//map.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 12.0f));
			
			float[] results = new float[1];
			float distance;
			minDistance=100000000;
			for(int i=0;i<hospitals.size();i++)
			{	
				Location.distanceBetween(myLat,myLon,lats[i], lons[i], results);
			    distance=results[0]/1000;
			    if(distance<minDistance)
			    {
			    	minDistance=distance;
			    	pos=i;
			    }
			}
			isRetreived=true;
			if(isLoaded)
			{
		    	LatLngBounds bounds=new LatLngBounds.Builder().include(myLoc).include(locs[pos]).build();
		        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
		        markers[pos].showInfoWindow();
		        cur=pos;
			}
		}
		catch(Exception e)
		{
			connectMap();
			//showToast("দুঃখিত, আপনার বর্তমান অবস্থান পাওয়া যাই নাই");
		}
	}


	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	
	
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }
            data = sb.toString();
            br.close();
 
        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    
    
    
    
    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String>{

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }
    
    
    
    
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            map.addPolyline(lineOptions);
        }

    }
    
	
	
	

}
