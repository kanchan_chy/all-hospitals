package com.w3dreamers.hospitalsofchittagong;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationActivity extends FragmentActivity{
	
	TextView txtTitle;
	
	double lat,lon;
	GoogleMap map;
	LatLng loc;
	Marker marker;
	
	//String id;
	String name,latitude,longitude;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.location_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		
		name=getIntent().getExtras().getString("name");
		//id=getIntent().getExtras().getString("id");
		latitude=getIntent().getExtras().getString("lat");
		longitude=getIntent().getExtras().getString("lon");
		
		lat=Double.valueOf(latitude);
		lon=Double.valueOf(longitude);
		
    //  lat = 23.7289043;
	//	lon =90.40868929999999;
		
		
		try
		{
			map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		}
		catch(Exception e)
		{
			Toast.makeText(getApplicationContext(), "Map can not be shown", Toast.LENGTH_LONG).show();
			finish();
		}
		
		if(map!=null)
		{
			map.setMyLocationEnabled(true); 
			loc=new LatLng(lat, lon);
			marker=map.addMarker(new MarkerOptions().position(loc).title(name).snippet("Click to see details"));
			marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
			marker.showInfoWindow();
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 12.0f));
			
			
			map.setInfoWindowAdapter(new InfoWindowAdapter() {
				
				@Override
				public View getInfoWindow(Marker marker) {
					// TODO Auto-generated method stub
					 View v=getLayoutInflater().inflate(R.layout.infowindow, null);
					 TextView txtWindowHeader=(TextView)v.findViewById(R.id.txtHeader);
					 TextView txtWindowBody=(TextView)v.findViewById(R.id.txtBody);
					 
					 txtWindowHeader.setText(marker.getTitle());
					 txtWindowBody.setText(marker.getSnippet());
					 
					 return v;
				}
				
				@Override
				public View getInfoContents(Marker marker) {
					// TODO Auto-generated method stub
					 return null;
				}
			});
			
			
			
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(Marker marker1) {
					// TODO Auto-generated method stub
					HospitalInfoRetreiver retreiver=new HospitalInfoRetreiver(LocationActivity.this);
					retreiver.DetailActivityCaller(name);
					//map.clear();
					//finish();
				}
			});
			
			
			
		}
		
		
		
		
	}
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		map.clear();
		finish();
	} 
	
	

}
