package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CategoriesActivityBangla extends Activity{
	
	ListView list;
	TextView txtTitle,txtAll;
	LinearLayout linearAll,linearSearchOption,linearTitle,linearSearch;
	ImageView imgSearchOption,imgIcon2;
	EditText edtSearch;
	
	CustomAdapterBangla adapter;
	
	ArrayList<String>categories=new ArrayList<String>();
	
	DbHelper dbOpenHelper;
	String title;
	
	boolean supported;
	Typeface banglaFont;
	
	boolean titleVisibility;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.category_layout);
		list=(ListView)findViewById(R.id.listView1);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtAll=(TextView)findViewById(R.id.txtAll);
		imgSearchOption=(ImageView)findViewById(R.id.imgSearchOption);
		imgIcon2=(ImageView)findViewById(R.id.imgIcon2);
		linearAll=(LinearLayout)findViewById(R.id.linearAll);
		linearSearchOption=(LinearLayout)findViewById(R.id.linearSearchOption);
		linearTitle=(LinearLayout)findViewById(R.id.linearTitle);
		linearSearch=(LinearLayout)findViewById(R.id.linearSearch);
		edtSearch=(EditText)findViewById(R.id.edtSearch);
		
		linearSearch.setVisibility(View.INVISIBLE);
		titleVisibility=true;
		
		categories=(ArrayList<String>)getIntent().getSerializableExtra("categories");
		
		title="চট্টগ্রামের হাসপাতালসমূহ";
		String all="সব ক্যাটেগরি";
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		txtAll.setTypeface(banglaFont);
        edtSearch.setTypeface(banglaFont);
		
		String hint="ক্যাটেগরির নাম টাইপ করুন";
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedAll=AndroidCustomFontSupport.getCorrectedBengaliFormat(all, banglaFont, (float)1);
			txtAll.setText(convertedAll);
			SpannableString convertedHint=AndroidCustomFontSupport.getCorrectedBengaliFormat(hint, banglaFont, (float)1);
			edtSearch.setHint(convertedHint);
		}
		else
		{
			txtTitle.setText(title);
			txtAll.setText(all);
			edtSearch.setHint(hint);
		}
	    
	    adapter=new CustomAdapterBangla(this,categories,"ক্যাটেগরি");
	    list.setAdapter(adapter);
	    
	    list.setTextFilterEnabled(true);
	    
	    try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
	    
	    
	    list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View arg1, int pos,
					long arg3) {
				// TODO Auto-generated method stub
               //String category=categories.get(pos);
				
				String category=(String)parent.getItemAtPosition(pos);
				
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_CATEGORY, Constants.STRING_ONLY, category, Constants.INFO_BANGLA_NAME);
					names=data.get(Constants.INFO_BANGLA_NAME);
					data.clear();
					
					Intent in=new Intent(CategoriesActivityBangla.this,HospitalListActivityBangla.class);
					in.putExtra("title", category);
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
				
			}
		});
	    
	    linearAll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try
				{
					ArrayList<String> names=new ArrayList<String>();
					ArrayList<String>columns=new ArrayList<String>();
					columns.add(Constants.INFO_BANGLA_NAME);
					HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_NAME);
					names=data.get(Constants.INFO_BANGLA_NAME);
					data.clear();
					
					Intent in=new Intent(CategoriesActivityBangla.this,HospitalListActivityBangla.class);
					in.putExtra("title", "চট্টগ্রামের হাসপাতালসমূহ");
					in.putExtra("names", names);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				catch(Exception e) {}
			}
		});
	    
	    
       linearSearchOption.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				linearTitle.setVisibility(View.INVISIBLE);
				linearSearch.setVisibility(View.VISIBLE);
				titleVisibility=false;
			}
		});
		
		
         imgIcon2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearSearch.setVisibility(View.INVISIBLE);
				linearTitle.setVisibility(View.VISIBLE);
				titleVisibility=true;
			}
		});
         
         
         
         
         edtSearch.addTextChangedListener(new TextWatcher() {
 			
 			@Override
 			public void onTextChanged(CharSequence cs, int start, int before, int count) {
 				// TODO Auto-generated method stub
 				adapter.getFilter().filter(cs);
 			}
 			
 			@Override
 			public void beforeTextChanged(CharSequence s, int start, int count,
 					int after) {
 				// TODO Auto-generated method stub
 				
 			}
 			
 			@Override
 			public void afterTextChanged(Editable s) {
 				// TODO Auto-generated method stub
 				
 			}
 		});
	    
		
	}
	
	

}
