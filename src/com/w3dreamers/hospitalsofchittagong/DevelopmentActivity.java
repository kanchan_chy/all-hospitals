package com.w3dreamers.hospitalsofchittagong;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

public class DevelopmentActivity extends Activity{
	
	TextView txtTitle;
	WebView web;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.idea_development);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		web=(WebView)findViewById(R.id.webView1);
		
		WebSettings webSettings=web.getSettings();
		webSettings.setAppCacheEnabled(false);
		webSettings.setBlockNetworkImage(true);
		webSettings.setLoadsImagesAutomatically(true);
		webSettings.setGeolocationEnabled(false);
		webSettings.setNeedInitialFocus(false);
		webSettings.setSaveFormData(false);
		
		web.getSettings().setJavaScriptEnabled(true);
		//web.getSettings().setBuiltInZoomControls(true);
	    web.loadUrl("file:///android_asset/web_pages/ideaanddevelopment.html");
		
		
	}

}
