package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class HospitalInfoRetreiverBangla {
	
	Activity activity;
	DbHelper dbOpenHelper;
	
	public HospitalInfoRetreiverBangla(Activity activity)
	{
		this.activity=activity;
	}
	
	public void DetailActivityCaller(String name)
	{
		try
		{
			dbOpenHelper=new DbHelper(activity, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
		try
		{
			String address="",direction="",website="",service24="",lat="",lon="",bloodBank="",ambulance="",email="",emergency="";
			ArrayList<String>columns=new ArrayList<String>();
			columns.add(Constants.INFO_BANGLA_ADDRESS);
			columns.add(Constants.INFO_BANGLA_DIRECTION);
			columns.add(Constants.INFO_BANGLA_WEBSITE);
			columns.add(Constants.INFO_BANGLA_SERVICE_24);
			columns.add(Constants.INFO_BANGLA_LATITUDE);
			columns.add(Constants.INFO_BANGLA_LONGITUDE);
			columns.add(Constants.INFO_BANGLA_BLOOD_BANK);
			columns.add(Constants.INFO_BANGLA_AMBULANCE);
			
			HashMap<String,ArrayList<String>> data	=dbOpenHelper.getSelectedRowString(Constants.TABLE_INFO_BANGLA, columns, Constants.INFO_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.INFO_BANGLA_NAME);
			if(data.get(Constants.INFO_BANGLA_ADDRESS).size()>0) address=data.get(Constants.INFO_BANGLA_ADDRESS).get(0);
			if(data.get(Constants.INFO_BANGLA_DIRECTION).size()>0) direction=data.get(Constants.INFO_BANGLA_DIRECTION).get(0);
			if(data.get(Constants.INFO_BANGLA_WEBSITE).size()>0) website=data.get(Constants.INFO_BANGLA_WEBSITE).get(0);
			if(data.get(Constants.INFO_BANGLA_SERVICE_24).size()>0) service24=data.get(Constants.INFO_BANGLA_SERVICE_24).get(0);
			if(data.get(Constants.INFO_BANGLA_LATITUDE).size()>0) lat=data.get(Constants.INFO_BANGLA_LATITUDE).get(0);
			if(data.get(Constants.INFO_BANGLA_LONGITUDE).size()>0) lon=data.get(Constants.INFO_BANGLA_LONGITUDE).get(0);
			if(data.get(Constants.INFO_BANGLA_BLOOD_BANK).size()>0) bloodBank=data.get(Constants.INFO_BANGLA_BLOOD_BANK).get(0);
			if(data.get(Constants.INFO_BANGLA_AMBULANCE).size()>0) ambulance=data.get(Constants.INFO_BANGLA_AMBULANCE).get(0);
			data.clear();
			
			ArrayList<String>columns1=new ArrayList<String>();
			columns1.add(Constants.EMAIL_BANGLA_MAIL);
			HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getSelectedRowString(Constants.TABLE_EMAIL_BANGLA, columns1, Constants.EMAIL_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.EMAIL_BANGLA_NAME);
			if(data1.get(Constants.EMAIL_BANGLA_MAIL).size()>0) email=data1.get(Constants.EMAIL_BANGLA_MAIL).get(0);
			data1.clear();
			
			ArrayList<String>columns2=new ArrayList<String>();
			columns2.add(Constants.EMERGENCY_BANGLA_NUMBER);
			HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getSelectedRowString(Constants.TABLE_EMERGENCY_BANGLA, columns2, Constants.EMERGENCY_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.EMERGENCY_BANGLA_NAME);
			if(data2.get(Constants.EMERGENCY_BANGLA_NUMBER).size()>0) emergency=data2.get(Constants.EMERGENCY_BANGLA_NUMBER).get(0);
			data2.clear();
			
			ArrayList<String>depts=new ArrayList<String>();
			ArrayList<String>columns3=new ArrayList<String>();
			columns3.add(Constants.DEPARTMENT_BANGLA_DEPT);
			HashMap<String,ArrayList<String>> data3	=dbOpenHelper.getSelectedRowString(Constants.TABLE_DEPARTMENT_BANGLA, columns3, Constants.DEPARTMENT_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.DEPARTMENT_BANGLA_NAME);
			depts=data3.get(Constants.DEPARTMENT_BANGLA_DEPT);
			data3.clear();
			
			if(ambulance.equals("অ্যাভেইলেবল"))
			{
				ArrayList<String>columns4=new ArrayList<String>();
				columns4.add(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER);
				HashMap<String,ArrayList<String>> data4	=dbOpenHelper.getSelectedRowString(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, columns4, Constants.AMBULANCE_NUMBER_BANGLA_NAME, Constants.STRING_ONLY, name, Constants.AMBULANCE_NUMBER_BANGLA_NAME);
				if(data4.get(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER).size()>0) ambulance=data4.get(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER).get(0);
				data4.clear();
			}
			
			Intent in=new Intent(activity,DetailsActivityBangla.class);
			//in.putExtra("id", id);
			in.putExtra("name", name);
			in.putExtra("address", address);
			in.putExtra("direction", direction);
			in.putExtra("website", website);
			in.putExtra("service24", service24);
			in.putExtra("lat", lat);
			in.putExtra("lon", lon);
			in.putExtra("ambulance", ambulance);
			in.putExtra("bloodBank", bloodBank);
			in.putExtra("email", email);
			in.putExtra("emergency", emergency);
			in.putExtra("depts", depts);
			activity.startActivity(in);
			activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		catch(Exception e) {}
	}
	

}
