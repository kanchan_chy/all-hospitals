package com.w3dreamers.hospitalsofchittagong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ExitActivity extends Activity{
	
	TextView txtGuide,txtRate,txtTell,txtFacebook,txtDevelopment,txtBack,txtUpdate;
	//LinearLayout linearGuide,linearRate,linearTell,linearFeedback,linearDevelopment,linearBack,linearUpdate;
	
	int success;
	JSONParser jParser;
	DbHelper dbOpenHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.exit_layout);
		txtGuide=(TextView)findViewById(R.id.txtGuide);
		txtRate=(TextView)findViewById(R.id.txtRate);
		txtTell=(TextView)findViewById(R.id.txtTell);
		txtFacebook=(TextView)findViewById(R.id.txtFacebook);
		txtDevelopment=(TextView)findViewById(R.id.txtDevelopment);
		txtBack=(TextView)findViewById(R.id.txtBack);
		txtUpdate=(TextView)findViewById(R.id.txtUpdate);
	/*	linearGuide=(LinearLayout)findViewById(R.id.linearGuide);
		linearRate=(LinearLayout)findViewById(R.id.linearRate);
		linearTell=(LinearLayout)findViewById(R.id.linearTell);
		linearFeedback=(LinearLayout)findViewById(R.id.linearFeedback);
		linearDevelopment=(LinearLayout)findViewById(R.id.linearDevelopment);
		linearBack=(LinearLayout)findViewById(R.id.linearBack);
		linearUpdate=(LinearLayout)findViewById(R.id.linearUpdate);  */
		
		
		
		try
		{
			dbOpenHelper=new DbHelper(this, Constants.DATABASE_NAME, 1);
		}
		catch(Exception e) {}
			
		
	}
	
	
	
	public void optionClicked(View view)
	{
		if(view.getId()==R.id.linearGuide)
		{
			Intent intent=new Intent(ExitActivity.this,GuideActivity.class);
			startActivity(intent);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearRate)
		{
			Uri uri = Uri.parse("market://details?id=" + getPackageName());
		    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		    try {
		        startActivity(myAppLinkToMarket);
		    } catch (ActivityNotFoundException e) {
		    	Toast.makeText(getApplicationContext(), "Sorry. Rating of this application is not possible", Toast.LENGTH_LONG).show();
		    }
		}
		else if(view.getId()==R.id.linearTell)
		{
			try
			{
			
				Intent sendIntent=new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				
				String link="https://play.google.com/store/apps/details?id="+getPackageName();
				sendIntent.putExtra(Intent.EXTRA_TEXT, link);
				sendIntent.setType("text/plain");
				startActivity(sendIntent);
				
			}
			
			catch(Exception e)
			{
				Toast.makeText(getApplicationContext(), "Sorry. Sharing of this application is not possible", Toast.LENGTH_LONG).show();
			}
		}
		else if(view.getId()==R.id.linearFacebook)
		{
			
			LayoutInflater inflater=LayoutInflater.from(ExitActivity.this);
			View view1=inflater.inflate(R.layout.facebook_dialog, null);
			LinearLayout linearLike,linearMeet,linearFeedback,linearClose;
			TextView txtLike,txtMeet,txtFeedback,txtClose;
			linearLike=(LinearLayout)view1.findViewById(R.id.linearLike);
			linearMeet=(LinearLayout)view1.findViewById(R.id.linearMeet);
			linearFeedback=(LinearLayout)view1.findViewById(R.id.linearFeedback);
			linearClose=(LinearLayout)view1.findViewById(R.id.linearClose);
			txtLike=(TextView)view1.findViewById(R.id.txtLike);
			txtMeet=(TextView)view1.findViewById(R.id.txtMeet);
			txtFeedback=(TextView)view1.findViewById(R.id.txtFeedback);
			txtClose=(TextView)view1.findViewById(R.id.txtClose);
			
			AlertDialog.Builder builder=new AlertDialog.Builder(ExitActivity.this);
			builder.setView(view1);
			builder.setCancelable(true);
			
			final AlertDialog dialog=builder.create();
			dialog.show();
			
			linearLike.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/w3dreamers.hospitalsapp?__mref=message")));
				}
			});
			
			
			linearMeet.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/pages/W3-Dreamers/279123222268078")));
				}
			});
			
			
			linearFeedback.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/w3dreamers.hospitalsapp?__mref=message")));
				}
			});
			
			
			linearClose.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			
		}
		else if(view.getId()==R.id.linearDevelopment)
		{
			Intent in=new Intent(ExitActivity.this,DevelopmentActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearBack)
		{
			Intent in=new Intent(ExitActivity.this,OptionActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
			finish();
		}
		else if(view.getId()==R.id.linearUpdate)
		{
			updateData();
		}
	}
	
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater=LayoutInflater.from(ExitActivity.this);
		View view=inflater.inflate(R.layout.warning_dialog, null);
		LinearLayout linearYes,linearNo;
		TextView txtYes,txtNo,txtWarning;
		linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
		linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
		txtYes=(TextView)view.findViewById(R.id.txtYes);
		txtNo=(TextView)view.findViewById(R.id.txtNo);
		txtWarning=(TextView)view.findViewById(R.id.txtWarning);
		
		txtWarning.setText("Do you want to exit right now?");
		txtYes.setText("Exit");
		txtNo.setText("Cancel");
		
		AlertDialog.Builder builder=new AlertDialog.Builder(ExitActivity.this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
				System.exit(1);
			}
		});
		
		
		linearNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
	}
	
	
	
	
	public void updateData()
	{
		if(isConnectingInternet())
		{
			
			LayoutInflater inflater=LayoutInflater.from(ExitActivity.this);
			View view=inflater.inflate(R.layout.warning_dialog, null);
			LinearLayout linearYes,linearNo;
			TextView txtYes,txtNo,txtWarning;
			linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
			linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
			txtYes=(TextView)view.findViewById(R.id.txtYes);
			txtNo=(TextView)view.findViewById(R.id.txtNo);
			txtWarning=(TextView)view.findViewById(R.id.txtWarning);
			
			txtWarning.setText("Do you want to update hospitals' information?");
			txtYes.setText("Yes");
			txtNo.setText("No");
			
			AlertDialog.Builder builder=new AlertDialog.Builder(ExitActivity.this);
			builder.setView(view);
			builder.setCancelable(true);
			
			final AlertDialog dialog=builder.create();
			dialog.show();
			
			linearYes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
					jParser = new JSONParser();
					UpdateChecker checker=new UpdateChecker();
					checker.execute();
				}
			});
			
			
			linearNo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			
		}
		else
		{
			LayoutInflater li = LayoutInflater.from(ExitActivity.this);
			View promptsView = li.inflate(R.layout.internet_dialog, null);
			AlertDialog.Builder builder = new AlertDialog.Builder(ExitActivity.this);
			builder.setView(promptsView);
			builder.setCancelable(true);
			final AlertDialog alertDialog = builder.create();
			alertDialog.show();
			TextView dialogTitle=(TextView)promptsView.findViewById(R.id.dialogTitle);
			TextView txtMain=(TextView)promptsView.findViewById(R.id.txtMain);
			TextView txtOptional=(TextView)promptsView.findViewById(R.id.txtOptional);
			TextView txtOk=(TextView)promptsView.findViewById(R.id.txtOk);
			TextView txtCancel=(TextView)promptsView.findViewById(R.id.txtCancel);
			
			LinearLayout linearOk=(LinearLayout)promptsView.findViewById(R.id.linearOk);
			LinearLayout linearCancel=(LinearLayout)promptsView.findViewById(R.id.linearCancel);
			
			txtMain.setText("Internet Connection required");
			txtOptional.setText("Click Ok button to turn ON internet connection of your device");
			
			linearCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
				}
			});	
			
			linearOk.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub	    
					alertDialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
		}
	}
	
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	
	public void callDataUpdate(boolean isAvailable)
	{
		if(isAvailable)
		{
			Toast.makeText(getApplicationContext(), "Update found", Toast.LENGTH_LONG).show();
			success=0;
			DataUpdate updator=new DataUpdate();
			updator.execute();
		}
		else Toast.makeText(getApplicationContext(), "No update found. You are using latest updated information", Toast.LENGTH_LONG).show();
	}
	
	
	
	class UpdateChecker extends AsyncTask<String, String, String>
	{
		AlertDialog alertDialog;
		
		int isSuccess;
		boolean isAvailable;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			isSuccess=0;
			isAvailable=false;
			
			LayoutInflater li = LayoutInflater.from(ExitActivity.this);
			View promptsView = li.inflate(R.layout.loading_dialog, null);
			TextView txtLoad=(TextView)promptsView.findViewById(R.id.txtLoad);
			String loadingText="Checking for update, please wait...";
			txtLoad.setText(loadingText);
			AlertDialog.Builder builder = new AlertDialog.Builder(ExitActivity.this);
			builder.setView(promptsView);
			builder.setCancelable(false);
		    alertDialog = builder.create();
			alertDialog.show();	
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			alertDialog.cancel();
			if(isSuccess==1) callDataUpdate(isAvailable);
			else Toast.makeText(getApplicationContext(), "Sorry, checking of update was not possible", Toast.LENGTH_LONG).show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try
			{
				List<NameValuePair> pair= new ArrayList<NameValuePair>();

				//JSONObject json=jParser.makeHttpRequest("http://192.168.43.158/cuet_admission/registration.php", "GET", pair);
				//	JSONObject json=jParser.makeHttpRequest("http://www.calvinfashion.net/demo/dreamers/EATL/Famous_foods/get_food_detail.php", "GET", pair);
				  JSONObject json=jParser.makeHttpRequest("http://10.0.2.2/Hospital_App/get_update.php", "GET", pair);
			    //JSONObject json=jParser.makeHttpRequest("http://api.national500apps.com/team_api/b/Famous_foods/get_food_detail.php", "GET", pair);
					
				isSuccess=json.getInt("success");
				if(isSuccess==1)
				{
					String code=json.getString("code");
					double version=Double.valueOf(code);				
					try
					{
						String currentCode="";
						ArrayList<String>columns=new ArrayList<String>();
						columns.add(Constants.VERSION_VERSION_CODE);
						HashMap<String,ArrayList<String>> data	=dbOpenHelper.getAllRowByColumn(Constants.TABLE_VERSION, columns, Constants.VERSION_VERSION_CODE);
						if(data.get(Constants.VERSION_VERSION_CODE).size()>0)
						{
							currentCode=data.get(Constants.VERSION_VERSION_CODE).get(0);
							if(!currentCode.equals(""))
							{
								double currentVersion=Double.valueOf(currentCode);
								if(version>currentVersion) isAvailable=true;
								else isAvailable=false;
							}
						}
					}
					catch(Exception e)
					{}
					
				}
			}
			catch(Exception e) {}
			return null;
		}
		
	}
	
	
	
	
	
	class DataUpdate extends AsyncTask<String, String, String>
	{
		AlertDialog alertDialog;
		
		ArrayList<String> info_ids=new ArrayList<String>();
		ArrayList<String> info_names=new ArrayList<String>();
		ArrayList<String> info_addresses=new ArrayList<String>();
		ArrayList<String> info_directions=new ArrayList<String>();
		ArrayList<String> info_websites=new ArrayList<String>();
		ArrayList<String> info_services=new ArrayList<String>();
		ArrayList<String> info_lats=new ArrayList<String>();
		ArrayList<String> info_lons=new ArrayList<String>();
		ArrayList<String> info_ambulances=new ArrayList<String>();
		ArrayList<String> info_bloods=new ArrayList<String>();
		ArrayList<String> info_categories=new ArrayList<String>();
		ArrayList<String> info_places=new ArrayList<String>();
		
		ArrayList<String> info_bangla_ids=new ArrayList<String>();
		ArrayList<String> info_bangla_names=new ArrayList<String>();
		ArrayList<String> info_bangla_addresses=new ArrayList<String>();
		ArrayList<String> info_bangla_directions=new ArrayList<String>();
		ArrayList<String> info_bangla_websites=new ArrayList<String>();
		ArrayList<String> info_bangla_services=new ArrayList<String>();
		ArrayList<String> info_bangla_lats=new ArrayList<String>();
		ArrayList<String> info_bangla_lons=new ArrayList<String>();
		ArrayList<String> info_bangla_ambulances=new ArrayList<String>();
		ArrayList<String> info_bangla_bloods=new ArrayList<String>();
		ArrayList<String> info_bangla_categories=new ArrayList<String>();
		ArrayList<String> info_bangla_places=new ArrayList<String>();
		
		ArrayList<String> category_ids=new ArrayList<String>();
		ArrayList<String> category_category_names=new ArrayList<String>();
		
		ArrayList<String> category_bangla_ids=new ArrayList<String>();
		ArrayList<String> category_bangla_category_names=new ArrayList<String>();
		
		ArrayList<String> place_ids=new ArrayList<String>();
		ArrayList<String> place_place_names=new ArrayList<String>();
		
		ArrayList<String> place_bangla_ids=new ArrayList<String>();
		ArrayList<String> place_bangla_place_names=new ArrayList<String>();
		
		ArrayList<String> ambulance_ids=new ArrayList<String>();
		ArrayList<String> ambulance_names=new ArrayList<String>();
		ArrayList<String> ambulance_numbers=new ArrayList<String>();
		
		ArrayList<String> ambulance_bangla_ids=new ArrayList<String>();
		ArrayList<String> ambulance_bangla_names=new ArrayList<String>();
		ArrayList<String> ambulance_bangla_numbers=new ArrayList<String>();
		
		ArrayList<String> emergency_ids=new ArrayList<String>();
		ArrayList<String> emergency_names=new ArrayList<String>();
		ArrayList<String> emergency_numbers=new ArrayList<String>();
		
		ArrayList<String> emergency_bangla_ids=new ArrayList<String>();
		ArrayList<String> emergency_bangla_names=new ArrayList<String>();
		ArrayList<String> emergency_bangla_numbers=new ArrayList<String>();
		
		ArrayList<String> email_ids=new ArrayList<String>();
		ArrayList<String> email_names=new ArrayList<String>();
		ArrayList<String> email_mails=new ArrayList<String>();
		
		ArrayList<String> email_bangla_ids=new ArrayList<String>();
		ArrayList<String> email_bangla_names=new ArrayList<String>();
		ArrayList<String> email_bangla_mails=new ArrayList<String>();
		
		ArrayList<String> department_ids=new ArrayList<String>();
		ArrayList<String> department_names=new ArrayList<String>();
		ArrayList<String> department_depts=new ArrayList<String>();
		
		ArrayList<String> department_bangla_ids=new ArrayList<String>();
		ArrayList<String> department_bangla_names=new ArrayList<String>();
		ArrayList<String> department_bangla_depts=new ArrayList<String>();
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			LayoutInflater li = LayoutInflater.from(ExitActivity.this);
			View promptsView = li.inflate(R.layout.loading_dialog, null);
			TextView txtLoad=(TextView)promptsView.findViewById(R.id.txtLoad);
			String loadingText="Updating hospitals' information, please wait...";
			txtLoad.setText(loadingText);
			AlertDialog.Builder builder = new AlertDialog.Builder(ExitActivity.this);
			builder.setView(promptsView);
			builder.setCancelable(false);
		    alertDialog = builder.create();
			alertDialog.show();
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			alertDialog.cancel();
			if(success==0) Toast.makeText(getApplicationContext(), "Sorry, hospitals' information was not updated", Toast.LENGTH_LONG).show();
			else Toast.makeText(getApplicationContext(), "Hospitals' information was successfully updated", Toast.LENGTH_LONG).show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			try
			{

				List<NameValuePair> pair= new ArrayList<NameValuePair>();

			//JSONObject json=jParser.makeHttpRequest("http://192.168.43.158/cuet_admission/registration.php", "GET", pair);
			//	JSONObject json=jParser.makeHttpRequest("http://www.calvinfashion.net/demo/dreamers/EATL/Famous_foods/get_food_detail.php", "GET", pair);
			  JSONObject json=jParser.makeHttpRequest("http://10.0.2.2/Hospital_App/get_all_data.php", "GET", pair);
		    //JSONObject json=jParser.makeHttpRequest("http://api.national500apps.com/team_api/b/Famous_foods/get_food_detail.php", "GET", pair);
				
				success=json.getInt("success");	
				if(success==1)
				{
					try
					{
						JSONArray _info_ids= json.getJSONArray("info_ids");
						JSONArray _info_names= json.getJSONArray("info_names");
						JSONArray _info_addresses= json.getJSONArray("info_addresses");
						JSONArray _info_directions= json.getJSONArray("info_directions");
						JSONArray _info_websites= json.getJSONArray("info_websites");
						JSONArray _info_services= json.getJSONArray("info_services");
						JSONArray _info_lats= json.getJSONArray("info_lats");
						JSONArray _info_lons= json.getJSONArray("info_lons");
						JSONArray _info_bloods= json.getJSONArray("info_bloods");
						JSONArray _info_ambulances= json.getJSONArray("info_ambulances");
						JSONArray _info_categories= json.getJSONArray("info_categories");
						JSONArray _info_places= json.getJSONArray("info_places");
						
						for(int i=0;i<_info_ids.length();i++)
						{
							JSONObject item=_info_ids.getJSONObject(i);						
							info_ids.add(item.getString("info_id"));
							item=_info_names.getJSONObject(i);						
							info_names.add(item.getString("info_name"));
							item=_info_addresses.getJSONObject(i);						
							info_addresses.add(item.getString("info_address"));
							item=_info_directions.getJSONObject(i);						
							info_directions.add(item.getString("info_direction"));
							item=_info_websites.getJSONObject(i);						
							info_websites.add(item.getString("info_website"));
							item=_info_services.getJSONObject(i);						
							info_services.add(item.getString("info_service"));
							item=_info_lats.getJSONObject(i);						
							info_lats.add(item.getString("info_lat"));
							item=_info_lons.getJSONObject(i);						
							info_lons.add(item.getString("info_lon"));
							item=_info_bloods.getJSONObject(i);						
							info_bloods.add(item.getString("info_blood"));
							item=_info_ambulances.getJSONObject(i);						
							info_ambulances.add(item.getString("info_ambulance"));
							item=_info_categories.getJSONObject(i);						
							info_categories.add(item.getString("info_category"));
							item=_info_places.getJSONObject(i);						
							info_places.add(item.getString("info_place"));
						}
						
						
						JSONArray _info_bangla_ids= json.getJSONArray("info_bangla_ids");
						JSONArray _info_bangla_names= json.getJSONArray("info_bangla_names");
						JSONArray _info_bangla_addresses= json.getJSONArray("info_bangla_addresses");
						JSONArray _info_bangla_directions= json.getJSONArray("info_bangla_directions");
						JSONArray _info_bangla_websites= json.getJSONArray("info_bangla_websites");
						JSONArray _info_bangla_services= json.getJSONArray("info_bangla_services");
						JSONArray _info_bangla_lats= json.getJSONArray("info_bangla_lats");
						JSONArray _info_bangla_lons= json.getJSONArray("info_bangla_lons");
						JSONArray _info_bangla_bloods= json.getJSONArray("info_bangla_bloods");
						JSONArray _info_bangla_ambulances= json.getJSONArray("info_bangla_ambulances");
						JSONArray _info_bangla_categories= json.getJSONArray("info_bangla_categories");
						JSONArray _info_bangla_places= json.getJSONArray("info_bangla_places");
						
						for(int i=0;i<_info_bangla_ids.length();i++)
						{
							JSONObject item=_info_bangla_ids.getJSONObject(i);						
							info_bangla_ids.add(item.getString("info_id"));
							item=_info_bangla_names.getJSONObject(i);						
							info_bangla_names.add(item.getString("info_name"));
							item=_info_bangla_addresses.getJSONObject(i);						
							info_bangla_addresses.add(item.getString("info_address"));
							item=_info_bangla_directions.getJSONObject(i);						
							info_bangla_directions.add(item.getString("info_direction"));
							item=_info_bangla_websites.getJSONObject(i);						
							info_bangla_websites.add(item.getString("info_website"));
							item=_info_bangla_services.getJSONObject(i);						
							info_bangla_services.add(item.getString("info_service"));
							item=_info_bangla_lats.getJSONObject(i);						
							info_bangla_lats.add(item.getString("info_lat"));
							item=_info_bangla_lons.getJSONObject(i);						
							info_bangla_lons.add(item.getString("info_lon"));
							item=_info_bangla_bloods.getJSONObject(i);						
							info_bangla_bloods.add(item.getString("info_blood"));
							item=_info_bangla_ambulances.getJSONObject(i);						
							info_bangla_ambulances.add(item.getString("info_ambulance"));
							item=_info_bangla_categories.getJSONObject(i);						
							info_bangla_categories.add(item.getString("info_category"));
							item=_info_bangla_places.getJSONObject(i);						
							info_bangla_places.add(item.getString("info_place"));
						}
						
						
						JSONArray _ids= json.getJSONArray("category_ids");
						JSONArray _names= json.getJSONArray("category_category_names");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							category_ids.add(item.getString("category_id"));
							item=_names.getJSONObject(i);						
							category_category_names.add(item.getString("category_category_name"));
						}
						
						
					  _ids= json.getJSONArray("category_bangla_ids");
					  _names= json.getJSONArray("category_bangla_category_names");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							category_bangla_ids.add(item.getString("category_id"));
							item=_names.getJSONObject(i);						
							category_bangla_category_names.add(item.getString("category_category_name"));
						}
						
						
						_ids= json.getJSONArray("place_ids");
						_names= json.getJSONArray("place_place_names");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							place_ids.add(item.getString("place_id"));
							item=_names.getJSONObject(i);						
							place_place_names.add(item.getString("place_place_name"));
						}
						
						
						_ids= json.getJSONArray("place_bangla_ids");
						_names= json.getJSONArray("place_bangla_place_names");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							place_bangla_ids.add(item.getString("place_id"));
							item=_names.getJSONObject(i);						
							place_bangla_place_names.add(item.getString("place_place_name"));
						}
						
						
						_ids= json.getJSONArray("ambulance_ids");
						_names= json.getJSONArray("ambulance_names");
						JSONArray _numbers= json.getJSONArray("ambulance_numbers");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							ambulance_ids.add(item.getString("ambulance_id"));
							item=_names.getJSONObject(i);						
							ambulance_names.add(item.getString("ambulance_name"));
							item=_numbers.getJSONObject(i);						
							ambulance_numbers.add(item.getString("ambulance_number"));
						}
						
						
						_ids= json.getJSONArray("ambulance_bangla_ids");
						_names= json.getJSONArray("ambulance_bangla_names");
						_numbers= json.getJSONArray("ambulance_bangla_numbers");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							ambulance_bangla_ids.add(item.getString("ambulance_id"));
							item=_names.getJSONObject(i);						
							ambulance_bangla_names.add(item.getString("ambulance_name"));
							item=_numbers.getJSONObject(i);						
							ambulance_bangla_numbers.add(item.getString("ambulance_number"));
						}
						
						
						_ids= json.getJSONArray("emergency_ids");
						_names= json.getJSONArray("emergency_names");
						_numbers= json.getJSONArray("emergency_numbers");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							emergency_ids.add(item.getString("emergency_id"));
							item=_names.getJSONObject(i);						
							emergency_names.add(item.getString("emergency_name"));
							item=_numbers.getJSONObject(i);						
							emergency_numbers.add(item.getString("emergency_number"));
						}
						
						
						_ids= json.getJSONArray("emergency_bangla_ids");
						_names= json.getJSONArray("emergency_bangla_names");
						_numbers= json.getJSONArray("emergency_bangla_numbers");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							emergency_bangla_ids.add(item.getString("emergency_id"));
							item=_names.getJSONObject(i);						
							emergency_bangla_names.add(item.getString("emergency_name"));
							item=_numbers.getJSONObject(i);						
							emergency_bangla_numbers.add(item.getString("emergency_number"));
						}
						
						
						_ids= json.getJSONArray("department_ids");
						_names= json.getJSONArray("department_names");
						JSONArray _depts= json.getJSONArray("department_depts");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							department_ids.add(item.getString("department_id"));
							item=_names.getJSONObject(i);						
							department_names.add(item.getString("department_name"));
							item=_depts.getJSONObject(i);						
							department_depts.add(item.getString("department_dept"));
						}
						
						
						_ids= json.getJSONArray("department_bangla_ids");
						_names= json.getJSONArray("department_bangla_names");
						_depts= json.getJSONArray("department_bangla_depts");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							department_bangla_ids.add(item.getString("department_id"));
							item=_names.getJSONObject(i);						
							department_bangla_names.add(item.getString("department_name"));
							item=_depts.getJSONObject(i);						
							department_bangla_depts.add(item.getString("department_dept"));
						}
						
						
						_ids= json.getJSONArray("email_ids");
						_names= json.getJSONArray("email_names");
						JSONArray _mails= json.getJSONArray("email_mails");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							email_ids.add(item.getString("email_id"));
							item=_names.getJSONObject(i);						
							email_names.add(item.getString("email_name"));
							item=_mails.getJSONObject(i);						
							email_mails.add(item.getString("email_mail"));
						}
						
						
						_ids= json.getJSONArray("email_bangla_ids");
						_names= json.getJSONArray("email_bangla_names");
					    _mails= json.getJSONArray("email_bangla_mails");
						
						for(int i=0;i<_ids.length();i++)
						{
							JSONObject item=_ids.getJSONObject(i);						
							email_bangla_ids.add(item.getString("email_id"));
							item=_names.getJSONObject(i);						
							email_bangla_names.add(item.getString("email_name"));
							item=_mails.getJSONObject(i);						
							email_bangla_mails.add(item.getString("email_mail"));
						}
						
						
					}
					catch(Exception e)
					{
						success=0;
					}
				}
					
					if(success==1)
					{
						try
						{
							
							boolean isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_INFO);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.INFO_ID, Constants.IS_NUMBER);
								column.put(Constants.INFO_NAME, Constants.IS_STRING);
								column.put(Constants.INFO_ADDRESS, Constants.IS_STRING);
								column.put(Constants.INFO_DIRECTION, Constants.IS_STRING);
								column.put(Constants.INFO_WEBSITE, Constants.IS_STRING);
								column.put(Constants.INFO_SERVICE_24, Constants.IS_STRING);
								column.put(Constants.INFO_LATITUDE, Constants.IS_STRING);
								column.put(Constants.INFO_LONGITUDE, Constants.IS_STRING);
								column.put(Constants.INFO_BLOOD_BANK, Constants.IS_STRING);
								column.put(Constants.INFO_AMBULANCE, Constants.IS_STRING);
								column.put(Constants.INFO_CATEGORY, Constants.IS_STRING);
								column.put(Constants.INFO_PLACE, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<info_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(info_ids.get(i));
									data.add(info_names.get(i));
									data.add(info_addresses.get(i));
									data.add(info_directions.get(i));
									data.add(info_websites.get(i));
									data.add(info_services.get(i));
									data.add(info_lats.get(i));
									data.add(info_lons.get(i));
									data.add(info_bloods.get(i));
									data.add(info_ambulances.get(i));
									data.add(info_categories.get(i));
									data.add(info_places.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_INFO, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_INFO_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.INFO_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.INFO_BANGLA_NAME, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_ADDRESS, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_DIRECTION, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_WEBSITE, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_SERVICE_24, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_LATITUDE, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_LONGITUDE, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_BLOOD_BANK, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_AMBULANCE, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_CATEGORY, Constants.IS_STRING);
								column.put(Constants.INFO_BANGLA_PLACE, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<info_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(info_bangla_ids.get(i));
									data.add(info_bangla_names.get(i));
									data.add(info_bangla_addresses.get(i));
									data.add(info_bangla_directions.get(i));
									data.add(info_bangla_websites.get(i));
									data.add(info_bangla_services.get(i));
									data.add(info_bangla_lats.get(i));
									data.add(info_bangla_lons.get(i));
									data.add(info_bangla_bloods.get(i));
									data.add(info_bangla_ambulances.get(i));
									data.add(info_bangla_categories.get(i));
									data.add(info_bangla_places.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_INFO_BANGLA, column, value);
								if(response==false) success=0;		
							}
							else success=0;
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_ALL_CATEGORY);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.ALL_CATEGORY_ID, Constants.IS_NUMBER);
								column.put(Constants.ALL_CATEGORY_CATEGORY_NAME, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<category_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(category_ids.get(i));
									data.add(category_category_names.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_ALL_CATEGORY, column, value);
								if(response==false) success=0;	
							}
							else success=0;	
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_ALL_CATEGORY_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.ALL_CATEGORY_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.ALL_CATEGORY_BANGLA_CATEGORY_NAME, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<category_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(category_bangla_ids.get(i));
									data.add(category_bangla_category_names.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_ALL_CATEGORY_BANGLA, column, value);
								if(response==false) success=0;	
							}
							else success=0;	
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_ALL_PLACE);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.ALL_PLACE_ID, Constants.IS_NUMBER);
								column.put(Constants.ALL_PLACE_PLACE_NAME, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<place_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(place_ids.get(i));
									data.add(place_place_names.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_ALL_PLACE, column, value);
								if(response==false) success=0;	
							}
							else success=0;	
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_ALL_PLACE_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.ALL_PLACE_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.ALL_PLACE_BANGLA_PLACE_NAME, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<place_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(place_bangla_ids.get(i));
									data.add(place_bangla_place_names.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_ALL_PLACE_BANGLA, column, value);
								if(response==false) success=0;	
							}
							else success=0;	
							
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_EMAIL);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.EMAIL_ID, Constants.IS_NUMBER);
								column.put(Constants.EMAIL_NAME, Constants.IS_STRING);
								column.put(Constants.EMAIL_MAIL, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<email_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(email_ids.get(i));
									data.add(email_names.get(i));
									data.add(email_mails.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_EMAIL, column, value);
								if(response==false) success=0;	
							}
							else success=0;	
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_EMAIL_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.EMAIL_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.EMAIL_BANGLA_NAME, Constants.IS_STRING);
								column.put(Constants.EMAIL_BANGLA_MAIL, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<email_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(email_bangla_ids.get(i));
									data.add(email_bangla_names.get(i));
									data.add(email_bangla_mails.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_EMAIL_BANGLA, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_EMERGENCY);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.EMERGENCY_ID, Constants.IS_NUMBER);
								column.put(Constants.EMERGENCY_NAME, Constants.IS_STRING);
								column.put(Constants.EMERGENCY_NUMBER, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<emergency_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(emergency_ids.get(i));
									data.add(emergency_names.get(i));
									data.add(emergency_numbers.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_EMERGENCY, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_EMERGENCY_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.EMERGENCY_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.EMERGENCY_BANGLA_NAME, Constants.IS_STRING);
								column.put(Constants.EMERGENCY_BANGLA_NUMBER, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<emergency_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(emergency_bangla_ids.get(i));
									data.add(emergency_bangla_names.get(i));
									data.add(emergency_bangla_numbers.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_EMERGENCY_BANGLA, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_AMBULANCE_NUMBER);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.AMBULANCE_NUMBER_ID, Constants.IS_NUMBER);
								column.put(Constants.AMBULANCE_NUMBER_NAME, Constants.IS_STRING);
								column.put(Constants.AMBULANCE_NUMBER_NUMBER, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<ambulance_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(ambulance_ids.get(i));
									data.add(ambulance_names.get(i));
									data.add(ambulance_numbers.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_AMBULANCE_NUMBER, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_AMBULANCE_NUMBER_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.AMBULANCE_NUMBER_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.AMBULANCE_NUMBER_BANGLA_NAME, Constants.IS_STRING);
								column.put(Constants.AMBULANCE_NUMBER_BANGLA_NUMBER, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<ambulance_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(ambulance_bangla_ids.get(i));
									data.add(ambulance_bangla_names.get(i));
									data.add(ambulance_bangla_numbers.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_AMBULANCE_NUMBER_BANGLA, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_DEPARTMENT);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.DEPARTMENT_ID, Constants.IS_NUMBER);
								column.put(Constants.DEPARTMENT_NAME, Constants.IS_STRING);
								column.put(Constants.DEPARTMENT_DEPT, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<department_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(department_ids.get(i));
									data.add(department_names.get(i));
									data.add(department_depts.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_DEPARTMENT, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
							
							
							isDeleted=dbOpenHelper.deleteTable(Constants.TABLE_DEPARTMENT_BANGLA);
							if(isDeleted)
							{
								LinkedHashMap<String, Integer>column=new LinkedHashMap<String, Integer>();
								column.put(Constants.DEPARTMENT_BANGLA_ID, Constants.IS_NUMBER);
								column.put(Constants.DEPARTMENT_BANGLA_NAME, Constants.IS_STRING);
								column.put(Constants.DEPARTMENT_BANGLA_DEPT, Constants.IS_STRING);
								
								HashMap< Integer,ArrayList<String>>value=new HashMap<Integer, ArrayList<String>>();
								for(int i=0;i<department_bangla_ids.size();i++)
								{
									ArrayList<String>data= new ArrayList<String>();
									data.add(department_bangla_ids.get(i));
									data.add(department_bangla_names.get(i));
									data.add(department_bangla_depts.get(i));
									value.put(i,data);
								} 
								boolean response=dbOpenHelper.insertRowByColumn(Constants.TABLE_DEPARTMENT_BANGLA, column, value);
								if(response==false) success=0;	
							}
							else success=0;
							
						}
						catch(Exception e)
						{
							success=0;
						}
					}
					
					
			}
			catch(Exception e)
			{}
			
			return null;
		}
		
		
	}
	
	
	
	
	

}
