package com.w3dreamers.hospitalsofchittagong;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class LocationActivityBangla extends FragmentActivity{
	
	TextView txtTitle;
	
	double lat,lon;
	GoogleMap map;
	LatLng loc;
	Marker marker;
	
	//String id;
      String name,latitude,longitude;
	
	boolean supported;
	Typeface banglaFont;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.location_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		
		name=getIntent().getExtras().getString("name");
		//id=getIntent().getExtras().getString("id");
		latitude=getIntent().getExtras().getString("lat");
		longitude=getIntent().getExtras().getString("lon");
		
		lat=Double.valueOf(latitude);
		lon=Double.valueOf(longitude);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedName=AndroidCustomFontSupport.getCorrectedBengaliFormat(name, banglaFont, (float)1);
			txtTitle.setText(convertedName);
		}
		else
		{
			txtTitle.setText(name);
		}
    //  lat = 23.7289043;
	//	lon =90.40868929999999;
		
		
		try
		{
			map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		}
		catch(Exception e)
		{
			//Toast.makeText(getApplicationContext(), "Map can not be shown", Toast.LENGTH_LONG).show();
			showToast("দুঃখিত, ম্যাপ দেখানো সম্ভব হছে না");
			finish();
		}
		
		if(map!=null)
		{
			map.setMyLocationEnabled(true); 
			loc=new LatLng(lat, lon);
			marker=map.addMarker(new MarkerOptions().position(loc).title(name).snippet("বিস্তারিত দেখতে ক্লিক করুন"));
			marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
			marker.showInfoWindow();
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 12.0f));
			
			
			map.setInfoWindowAdapter(new InfoWindowAdapter() {
				
				@Override
				public View getInfoWindow(Marker marker) {
					// TODO Auto-generated method stub
					 View v=getLayoutInflater().inflate(R.layout.infowindow, null);
					 TextView txtWindowHeader=(TextView)v.findViewById(R.id.txtHeader);
					 TextView txtWindowBody=(TextView)v.findViewById(R.id.txtBody);
					 txtWindowHeader.setTypeface(banglaFont);
					 txtWindowBody.setTypeface(banglaFont);
					 String markerTitle=marker.getTitle();
					 String markerSnippet=marker.getSnippet();
					 if(supported)
					 {
						 SpannableString convertedMarkerTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(markerTitle, banglaFont, (float)1);
					     txtWindowHeader.setText(convertedMarkerTitle);
					     SpannableString convertedMarkerSnippet=AndroidCustomFontSupport.getCorrectedBengaliFormat(markerSnippet, banglaFont, (float)1);
					     txtWindowBody.setText(convertedMarkerSnippet);
					 }
					 else
					 {
						 txtWindowHeader.setText(markerTitle);
						 txtWindowBody.setText(markerSnippet);
					 }
					 return v;
				}
				
				@Override
				public View getInfoContents(Marker marker) {
					// TODO Auto-generated method stub
					 return null;
				}
			});
			
			
			
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(Marker arg0) {
					// TODO Auto-generated method stub
					HospitalInfoRetreiverBangla retreiver=new HospitalInfoRetreiverBangla(LocationActivityBangla.this);
					retreiver.DetailActivityCaller(name);
					//map.clear();
					//finish();
				}
			});
			
			
			
		}	
	
		
		
		
		
	}
	
	
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		map.clear();
		finish();
	} 
	
	
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	

}
